#ifndef COMPASS_SM_H
#define COMPASS_SM_H

//Includes
#include <stdint.h>
#include <stdbool.h>

// Event Definitions
#include "ES_Configure.h" /* gets us event definitions */
#include "ES_Types.h"     /* gets bool type for returns */
#include "ES_Events.h"

//Public Function Prototypes
bool       InitCompassSM(uint8_t Priority);
bool       PostCompassSM(ES_Event_t ThisEvent);
ES_Event_t RunCompassSM(ES_Event_t ThisEvent);

//Functions to query parameters set by COMPASS
uint8_t QueryBallColor(void);
//etc.


//Enum for SM states
typedef enum CompassState{
  CompassInitPState,
  Registering,
  WaitingToStart,
  GameStarted,
  GameEnded
}CompassState_t;

#endif //COMPASS_SM_H



