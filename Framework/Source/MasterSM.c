/****************************************************************************
 Module
   MasterSM.c

 Revision
   1.0.1

 Description
   This is a template file for implementing flat state machines under the
   Gen2 Events and Services Framework.

 Notes

 History
 When           Who     What/Why
 -------------- ---     --------
 01/15/12 11:12 jec      revisions for Gen2 framework
 11/07/11 11:26 jec      made the queue static
 10/30/11 17:59 jec      fixed references to CurrentEvent in RunMasterSM()
 10/23/11 18:20 jec      began conversion from SMMaster.c (02/20/07 rev)
****************************************************************************/
/*----------------------------- Include Files -----------------------------*/
/* include header files for this state machine as well as any machines at the
   next lower level in the hierarchy that are sub-machines to this machine
*/
#include "ES_Configure.h"
#include "ES_Framework.h"
#include "MasterSM.h"

#include "Ports.h"
#include "Parameters.h"
#include "PWM.h"
#include "Timers.h"
#include "HardwareInit.h"
#include "SortingModule.h"
#include "Recycling.h"
#include "Move.h"

#include "CompassSM.h"
#include "PositioningAgainstGW.h"
#include "CollisionDetector.h"
#include "BeaconService.h"

/*----------------------------- Module Defines ----------------------------*/

#define LIMIT 5
#define BACKUP_TIME 1000
#define DEPOSIT_WASTE_TIME 4000
#define BOT_HIT_MOVE_TIME 1000
#define CHECK_GARBAGE_TIME 5000
#define COLLECTING_POSITION_STRAIGHT_TIME 1000
#define COLLECTING_POSITION_TURN_TIME 1500
//#define BOT_HIT_TURN_TIME 2000
#define NO_BALLS_COLLECTED_TIME 20000

/*---------------------------- Module Functions ---------------------------*/
/* prototypes for private functions for this machine.They should be functions
   relevant to the behavior of this state machine
*/

/*---------------------------- Module Variables ---------------------------*/
// everybody needs a state variable, you may need others as well.
// type of state variable should match htat of enum in header file
static MasterSMState_t  CurrentState;
static MasterSMState_t  NextState;
static MasterSMState_t  CollisionReturnState;

// with the introduction of Gen2, we need a module level Priority var as well
static uint8_t  MyPriority;
static uint16_t GarbagePeriod;
static uint16_t RecyclingPeriod;
bool            FirstTime = true;

/*------------------------------ Module Code ------------------------------*/
/****************************************************************************
 Function
     InitMasterSM

 Parameters
     uint8_t : the priorty of this service

 Returns
     bool, false if error in initialization, true otherwise

 Description
     Saves away the priority, sets up the initial transition and does any
     other required initialization for this state machine
 Notes

 Author
     J. Edward Carryer, 10/23/11, 18:55
****************************************************************************/
bool InitMasterSM(uint8_t Priority)
{
  ES_Event_t ThisEvent;

  MyPriority = Priority;
  // put us into the Initial PseudoState
  CurrentState = MasterWaitingForStart;
  // post the initial transition event
  ThisEvent.EventType = ES_INIT;

  //XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX need to initialize N/S variables

  if (ES_PostToService(MyPriority, ThisEvent) == true)
  {
    return true;
  }
  else
  {
    return false;
  }
}

/****************************************************************************
 Function
     PostMasterSM

 Parameters
     EF_Event_t ThisEvent , the event to post to the queue

 Returns
     boolean False if the Enqueue operation failed, True otherwise

 Description
     Posts an event to this state machine's queue
 Notes

 Author
     J. Edward Carryer, 10/23/11, 19:25
****************************************************************************/
bool PostMasterSM(ES_Event_t ThisEvent)
{
  return ES_PostToService(MyPriority, ThisEvent);
}

/****************************************************************************
 Function
    RunMasterSM

 Parameters
   ES_Event_t : the event to process

 Returns
   ES_Event_t, ES_NO_EVENT if no error ES_ERROR otherwise

 Description
   add your description here
 Notes
   uses nested switch/case to implement the machine.
 Author
   J. Edward Carryer, 01/15/12, 15:23
****************************************************************************/
ES_Event_t RunMasterSM(ES_Event_t ThisEvent)
{
  ES_Event_t ReturnEvent;
  ReturnEvent.EventType = ES_NO_EVENT; // assume no errors

  //If we have a game over event, switch to waiting to game over state
  if (ThisEvent.EventType == GAME_OVER)
  {
    NextState = MasterGameOver;
    Move(FORWARD, 0);
  }
  else
  {
    switch (CurrentState)
    {
      case MasterWaitingForStart:
      {
        //printf("\n\r in master - waitingforstart \n\r");
        switch (ThisEvent.EventType)
        {
          case CLEANING_UP:
          {
            if (FirstTime)
            {
              ES_Event_t NewEvent;
              NewEvent.EventType = START;
              NewEvent.EventParam = NO_COLLISION;
              printf("\n\r in master - going into positioningAgainstGW \n\r");
              NextState = Recycling;
              PostRecycling(NewEvent);
              PostCollisionDetector(NewEvent);

              FirstTime = false;
            }
            else
            {
              ES_Event_t NewEvent;
              NewEvent.EventType = START;
              printf("\n\r in master - going into positioningAgainstGW \n\r");
              NextState = PositioningAgainstGW;
              PostPositioningAgainstGW(NewEvent);
              PostCollisionDetector(NewEvent);
            }
          }
          break;
        }
      }
      break;

      case PositioningAgainstGW:        // If current state is state one
      {
        //printf("\n\r masterstate = positioningAgainstGW \n\r");
        switch (ThisEvent.EventType)
        {
          /*
          case RECYCLING_BALL:
          {
            ES_Event_t NewEvent;
            NewEvent.EventType = START;
            PostRecycling(NewEvent);

            ES_Event_t NewEvent2;
            NewEvent2.EventType = EXIT;
            PostPositioningAgainstGW(NewEvent2);
            NextState = Recycling;
          }
          break;
          */

          case GW_DETECTED:
          {
            //if (QueryGarbageCount() > 0) // or >= LIMIT)?
            if (QueryGeneralCount() >= 1)
            {
              int GeneralCount = QueryGeneralCount();
              printf( "\n\rthis is general count: %d\n\r", GeneralCount);
              printf( "gw detected and, and you have garbage \n\r");
              Move(FORWARD, Slow_DC);
              ES_Timer_InitTimer(BACKUP_DEPOSIT_TIMER, BACKUP_TIME);
              NextState = DumpingGarbage;
            }
            else
            {  //go to collection position - move forward for X sec
              printf("gw detected and no garbage \n\r");
              ES_Timer_InitTimer(MOVE_TO_COLLECTING_POSITION_TIMER, COLLECTING_POSITION_STRAIGHT_TIME);
              Move(FORWARD, Slow_DC);
              //ES_Timer_InitTimer(CHECK_GARBAGE_TIMER, CHECK_GARBAGE_TIME + 10000);
              NextState = CollectingPositionStraight;
            }
          }
          break;

          case ES_TIMEOUT:
          {
            if (ThisEvent.EventParam == SERVO_TIMER)
            {
              //close servo, re-initiate homing on garbage wall
              SetServo(GARBAGE_SERVO, SERVO_CLOSED_ANGLE);
              ResetGarbageCount();
              ResetGeneralCount();
              ES_Event_t NewEvent;
              NewEvent.EventType = START;
              PostPositioningAgainstGW(NewEvent);
            }
          }
          break;

          /*
          case HIT_BOT_BEHIND:
          {
            printf("hit bot in behind\n\r");
            ES_Event_t NewEvent;
            NewEvent.EventType = EXIT;
            PostPositioningAgainstGW(NewEvent);
            CollisionReturnState = CurrentState;
            ES_Timer_InitTimer(BOT_HIT_TIMER, BOT_HIT_MOVE_TIME);
            Move(FORWARD, Slow_DC);
            NextState = CollisionGettingOutOfWay;
          }
          break;
          */

          case HIT_BOT_IN_FRONT:
          {
            printf("hit bot in front\n\r");
            ES_Event_t NewEvent;
            NewEvent.EventType = EXIT;
            PostPositioningAgainstGW(NewEvent);
            CollisionReturnState = CurrentState;
            ES_Timer_InitTimer(BOT_HIT_TIMER, BOT_HIT_MOVE_TIME);
            Move( R_WHEEL_BACKWARD, Slow_DC);
            Move( L_WHEEL_BACKWARD, Slow_DC + 20);
            NextState = CollisionGettingOutOfWay;
          }
          break;
        }
      }
      break;

      case CollectingPositionStraight:
      {
        switch (ThisEvent.EventType)
        {
          case ES_TIMEOUT:
          {
            if (ThisEvent.EventParam == MOVE_TO_COLLECTING_POSITION_TIMER)
            {
              Move(CLOCKWISE, Slow_DC);
              NextState = MasterWaitingInCollecting;
              ES_Timer_InitTimer(MOVE_TO_COLLECTING_POSITION_TIMER, COLLECTING_POSITION_TURN_TIME);
              printf("straight, sending command to turn into collecting pos'n\n\r");
            }
          }
          break;
        }
      }
      break;

      case DumpingGarbage:
      {
        //printf("\n\r masterstate = dumpinggarbage \n\r");
        switch (ThisEvent.EventType)
        {
          case ES_TIMEOUT:
          {
            if (ThisEvent.EventParam == BACKUP_DEPOSIT_TIMER)
            {
              printf("stop, open servo, wait for timer\n\r");
              //stop, open servo, wait for timer
              Move(FORWARD, 0);
              SetServo(GARBAGE_SERVO, SERVO_OPEN_ANGLE);
              ES_Timer_InitTimer(SERVO_TIMER, DEPOSIT_WASTE_TIME);
              NextState = PositioningAgainstGW;
              ResetGeneralCount();
            }
          }
          break;

          /*
          case HIT_BOT_BEHIND:
          {
            printf("hit bot behind\n\r");
            CollisionReturnState = CurrentState;
            ES_Timer_InitTimer(BOT_HIT_TIMER, BOT_HIT_MOVE_TIME);
            Move(FORWARD, Slow_DC);
            NextState = CollisionGettingOutOfWay;
          }
          break;
          */

          case HIT_BOT_IN_FRONT:
          {
            printf("hit bot in front\n\r");
            CollisionReturnState = CurrentState;
            ES_Timer_InitTimer(BOT_HIT_TIMER, BOT_HIT_MOVE_TIME);
            Move( R_WHEEL_BACKWARD, Slow_DC);
            Move( L_WHEEL_BACKWARD, Slow_DC + 20);
            NextState = CollisionGettingOutOfWay;
          }
          break;
        }
      }
      break;

      case MasterWaitingInCollecting:
      {
        //printf("\n\r masterstate = waitingincollecting \n\r");
        //no collision case b/c we're not moving
        switch (ThisEvent.EventType)
        {
          case ES_TIMEOUT:
          {
            switch (ThisEvent.EventParam)
            {
              case MOVE_TO_COLLECTING_POSITION_TIMER:
              {
                printf("finished driving forward to collecting position\n\r");
                Move(FORWARD, 0);
                NextState = MasterWaitingInCollecting;
                ES_Timer_InitTimer( NO_BALLS_COLLECTED_TIMER, NO_BALLS_COLLECTED_TIME);
                ES_Timer_InitTimer( CHECK_GARBAGE_TIMER,      CHECK_GARBAGE_TIME);
                printf("turned 90 deg clockwise and is in collection position\n\r");
              }
              break;

              case CHECK_GARBAGE_TIMER:
              {
                /*
                if (QueryRecyclingCount() > 0)
                {
                  ES_Event_t NewEvent;
                  NewEvent.EventType = START;
                  PostRecycling(NewEvent);
                  NextState = Recycling;
                  ClearBeaconDetectors();
                }
                else if (QueryGarbageCount() > LIMIT)*/
                if (QueryGeneralCount() >= LIMIT)
                {
                  //when we align with garabge wall, we'll have garbage count > 0 so we'll deposit
                  ES_Event_t NewEvent;
                  NewEvent.EventType = START;
                  PostPositioningAgainstGW(NewEvent);
                  NextState = PositioningAgainstGW;
                }
                ES_Timer_InitTimer(CHECK_GARBAGE_TIMER, CHECK_GARBAGE_TIME);
              }
              break;

              case NO_BALLS_COLLECTED_TIMER:
              {
                ES_Event_t NewEvent;
                NewEvent.EventType = START;
                PostPositioningAgainstGW(NewEvent);
                NextState = PositioningAgainstGW;
              }
              break;
            }
          }
          break;

          /*
          case HIT_BOT_BEHIND:
        {
          printf("hit bot in behind\n\r");
          ES_Event_t NewEvent;
          NewEvent.EventType = EXIT;
          PostPositioningAgainstGW(NewEvent);
          CollisionReturnState = CurrentState;
          ES_Timer_InitTimer(BOT_HIT_TIMER, BOT_HIT_MOVE_TIME);
          Move(FORWARD, Slow_DC);
          NextState = CollisionGettingOutOfWay;
        }
        break;
          */

          case HIT_BOT_IN_FRONT:
          {
            printf("hit bot in front\n\r");
            ES_Event_t NewEvent;
            NewEvent.EventType = EXIT;
            PostPositioningAgainstGW(NewEvent);
            CollisionReturnState = CurrentState;
            ES_Timer_InitTimer(BOT_HIT_TIMER, BOT_HIT_MOVE_TIME);
            Move( R_WHEEL_BACKWARD, Slow_DC);
            Move( L_WHEEL_BACKWARD, Slow_DC + 20);
            NextState = CollisionGettingOutOfWay;
          }
          break;
        }
      }
      break;

      case Recycling:
      {
        //printf("\n\r masterstate = recycling \n\r");
        switch (ThisEvent.EventType)
        {
          /*
          case HIT_BOT_BEHIND:
          {
            printf("hit bot in behind\n\r");
            ES_Event_t NewEvent;
            NewEvent.EventType = EXIT;
            PostRecycling(NewEvent);
            CollisionReturnState = CurrentState;
            ES_Timer_InitTimer(BOT_HIT_TIMER, BOT_HIT_MOVE_TIME);
            Move(FORWARD, Slow_DC);
            NextState = CollisionGettingOutOfWay;
          }
          break;
          */

          case HIT_BOT_IN_FRONT:
          {
            printf("hit bot in front\n\r");
            ES_Event_t NewEvent;
            NewEvent.EventType = EXIT;
            PostRecycling(NewEvent);
            CollisionReturnState = CurrentState;
            ES_Timer_InitTimer(BOT_HIT_TIMER, BOT_HIT_MOVE_TIME);
            Move( R_WHEEL_BACKWARD, Slow_DC);
            Move( L_WHEEL_BACKWARD, Slow_DC + 20);
            NextState = CollisionGettingOutOfWay;
          }
          break;

          case EXIT:
          {
            ES_Event_t NewEvent;
            NewEvent.EventType = START;
            PostPositioningAgainstGW(NewEvent);
            NextState = PositioningAgainstGW;
          }
          break;
        }
        default:
          ;
      }
      break;

      case CollisionGettingOutOfWay:
      {
        printf("\n\r masterstate = collisiongettingoutofway \n\r");
        switch (ThisEvent.EventType)
        {
          case ES_TIMEOUT:
          {
            switch (ThisEvent.EventParam)
            {
              case BOT_HIT_TIMER:
              {
                ES_Event_t NewEvent;
                NewEvent.EventType = START;
                if (CollisionReturnState == Recycling)
                {
                  NewEvent.EventParam = COLLISION;
                  PostRecycling(NewEvent);
                  NextState = Recycling;
                }
                else
                {
                  PostPositioningAgainstGW(NewEvent);
                  NextState = PositioningAgainstGW;
                }
              }
              break;
            }
          }
          break;

          case BUMP:
          {
            switch (ThisEvent.EventParam)
            {
              case BL_HIT:
              {
                printf("hit something while backing out of collision");
                Move(FORWARD, Slow_DC);
                ES_Timer_InitTimer(BOT_HIT_TIMER, BOT_HIT_MOVE_TIME);
              }
              break;

              case BR_HIT:
              {
                printf("hit something while backing out of collision");
                Move(FORWARD, Slow_DC);
                ES_Timer_InitTimer(BOT_HIT_TIMER, BOT_HIT_MOVE_TIME);
              }
              break;
            }
          }
          break;
        }
      }
      break;
    }
  }
  CurrentState = NextState;
  return ReturnEvent;
}

/****************************************************************************
 Function
     QueryMasterSM

 Parameters
     None

 Returns
     MasterState_t The current state of the Master state machine

 Description
     returns the current state of the Master state machine
 Notes

 Author
     J. Edward Carryer, 10/23/11, 19:21
****************************************************************************/
MasterSMState_t QueryMasterSM(void)
{
  return CurrentState;
}

/***************************************************************************
 private functions
 ***************************************************************************/
