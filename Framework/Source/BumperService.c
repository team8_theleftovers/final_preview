/****************************************************************************
 Module
   BumperService.c

 Revision
   1.0.1

 Description
   This is a template file for implementing a simple service under the
   Gen2 Events and Services Framework.

 Notes

 History
 When           Who     What/Why
 -------------- ---     --------
 01/16/12 09:58 jec      began conversion from TemplateFSM.c
****************************************************************************/
/*----------------------------- Include Files -----------------------------*/
/* include header files for this state machine as well as any machines at the
   next lower level in the hierarchy that are sub-machines to this machine
*/
#include "ES_Configure.h"
#include "ES_Framework.h"
#include "BumperService.h"
#include "Ports.h"
#include "Parameters.h"
//#include "FinalReportFirstCheckoff.h"

/*----------------------------- Module Defines ----------------------------*/

/*---------------------------- Module Functions ---------------------------*/
/* prototypes for private functions for this service.They should be functions
   relevant to the behavior of this service
*/

/*---------------------------- Module Variables ---------------------------*/
// with the introduction of Gen2, we need a module level Priority variable
static uint8_t  MyPriority;
static uint8_t  LastFrontLeftCornerState;
static uint8_t  LastFrontRightCornerState;
static uint8_t  LastBackLeftCornerState;
static uint8_t  LastBackRightCornerState;

/*------------------------------ Module Code ------------------------------*/
/****************************************************************************
 Function
     InitTemplateService

 Parameters
     uint8_t : the priorty of this service

 Returns
     bool, false if error in initialization, true otherwise

 Description
     Saves away the priority, and does any
     other required initialization for this service
 Notes

 Author
     J. Edward Carryer, 01/16/12, 10:00
****************************************************************************/
bool InitBumperService(uint8_t Priority)
{
  ES_Event_t ThisEvent;

  MyPriority = Priority;
  /********************************************
   in here you write your initialization code
   *******************************************/
  // post the initial transition event

  //Limit Switches
  //SetPin2Digital(PD, LIMIT_FL | LIMIT_FR | LIMIT_RL |LIMIT_RR);
  //SetPin2Input(PD, LIMIT_FL | LIMIT_FR | LIMIT_RL |LIMIT_RR);

  printf("pins initialuzed");

  ThisEvent.EventType = ES_INIT;
  if (ES_PostToService(MyPriority, ThisEvent) == true)
  {
    return true;
  }
  else
  {
    return false;
  }
}

/****************************************************************************
 Function
     PostBumperService

 Parameters
     EF_Event_t ThisEvent ,the event to post to the queue

 Returns
     bool false if the Enqueue operation failed, true otherwise

 Description
     Posts an event to this state machine's queue
 Notes

 Author
     J. Edward Carryer, 10/23/11, 19:25
****************************************************************************/
bool PostBumperService(ES_Event_t ThisEvent)
{
  return ES_PostToService(MyPriority, ThisEvent);
}

/****************************************************************************
 Function
    RunBumperService

 Parameters
   ES_Event_t : the event to process

 Returns
   ES_Event, ES_NO_EVENT if no error ES_ERROR otherwise

 Description
   add your description here
 Notes

 Author
   J. Edward Carryer, 01/15/12, 15:23
****************************************************************************/
ES_Event_t RunBumperService(ES_Event_t ThisEvent)
{
  ES_Event_t ReturnEvent;
  ReturnEvent.EventType = ES_NO_EVENT; // assume no errors
  /********************************************
   in here you write your service code
   *******************************************/

  //this code is kind of obsolete - at this point we just need the event checker

  //fix that - move event checker to FinalReportFirstCheckoff

  switch (ThisEvent.EventType)
  {
    case BUMP:
    {
      switch (ThisEvent.EventParam)
      {
        case 10:
        {
          printf("front left bumper hit");
        }
        break;

        case 11:
        {
          printf("front left bumper left");
        }
        break;
      }
    }
    break;
  }

  return ReturnEvent;
}

bool BumperEventChecker(void)
{
  bool    ReturnVal = false;

  uint8_t FL = HWREG(GPIO_PORTD_BASE + (GPIO_O_DATA + ALL_BITS)) & BIT2HI;
  uint8_t FR = HWREG(GPIO_PORTD_BASE + (GPIO_O_DATA + ALL_BITS)) & BIT3HI;
  uint8_t BL = HWREG(GPIO_PORTD_BASE + (GPIO_O_DATA + ALL_BITS)) & BIT6HI;
  uint8_t BR = HWREG(GPIO_PORTD_BASE + (GPIO_O_DATA + ALL_BITS)) & BIT7HI;

  if (FL != LastFrontLeftCornerState)
  {
    ES_Event_t NewEvent;
    NewEvent.EventType = BUMP;
    if (FL > 0)
    {
      //printf("\n\r front left corner hit");
      NewEvent.EventParam = 10;
      //PostFinalReportFirstCheckoff(NewEvent);
    }
    else
    {
      //printf("\n\r front left corner came off");
      NewEvent.EventParam = 11;
      //PostFinalReportFirstCheckoff(NewEvent);
    }
    LastFrontLeftCornerState = FL;
    ReturnVal = true;
  }

  if (FR != LastFrontRightCornerState)
  {
    ES_Event_t NewEvent;
    NewEvent.EventType = BUMP;
    if (FR > 0)
    {
      //printf("\n\r front right corner hit");
      NewEvent.EventParam = 20;
      //PostFinalReportFirstCheckoff(NewEvent);
    }
    else
    {
      //printf("\n\r front right corner came off");
      NewEvent.EventParam = 21;
      //PostFinalReportFirstCheckoff(NewEvent);
    }
    LastFrontRightCornerState = FR;
    ReturnVal = true;
  }

  if (BL != LastBackLeftCornerState)
  {
    ES_Event_t NewEvent;
    NewEvent.EventType = BUMP;
    if (BL > 0)
    {
      //printf("\n\r back left corner hit");
      NewEvent.EventParam = 30;
      //PostFinalReportFirstCheckoff(NewEvent);
    }
    else
    {
      //printf("\n\r back left corner came off");
      NewEvent.EventParam = 31;
      //PostFinalReportFirstCheckoff(NewEvent);
    }
    LastBackLeftCornerState = BL;
    ReturnVal = true;
  }

  if (BR != LastBackRightCornerState)
  {
    ES_Event_t NewEvent;
    NewEvent.EventType = BUMP;
    if (BR > 0)
    {
      //printf("\n\r back right corner hit");
      NewEvent.EventParam = 40;
      //PostFinalReportFirstCheckoff(NewEvent);
    }
    else
    {
      //printf("\n\r back right  corner came off");
      NewEvent.EventParam = 41;
      //PostFinalReportFirstCheckoff(NewEvent);
    }
    LastBackRightCornerState = BR;
    ReturnVal = true;
  }

  return ReturnVal;
}

/***************************************************************************
 private functions
 ***************************************************************************/

/*------------------------------- Footnotes -------------------------------*/
/*------------------------------ End of file ------------------------------*/
