/****************************************************************************
 Module
   PositioningAgainstGW.c

 Revision
   1.0.1

 Description
   This is a PositioningAgainstGW file for implementing flat state machines under the
   Gen2 Events and Services Framework.

 Notes

 History
 When           Who     What/Why
 -------------- ---     --------
 01/15/12 11:12 jec      revisions for Gen2 framework
 11/07/11 11:26 jec      made the queue static
 10/30/11 17:59 jec      fixed references to CurrentEvent in RunPositioningAgainstGW()
 10/23/11 18:20 jec      began conversion from SMPositioningAgainstGW.c (02/20/07 rev)
****************************************************************************/
/*----------------------------- Include Files -----------------------------*/
/* include header files for this state machine as well as any machines at the
   next lower level in the hierarchy that are sub-machines to this machine
*/
#include "ES_Configure.h"
#include "ES_Framework.h"
#include "PositioningAgainstGW.h"
#include "CompassSM.h"
#include "Ports.h"
#include <math.h>
#include "CollisionDetector.h"

#include "Parameters.h"
#include "PWM.h"
#include "Move.h"

#include "MasterSM.h"

#include "Recycling.h"

/*----------------------------- Module Defines ----------------------------*/

#define turn_DC 50
#define num_pulses 10
#define FreqToTicks 50

#define BEACON_SPIN_TIMEOUT 7000
#define XL_BEACON_SPIN_TIMEOUT 5000

#define BACK_AWAY_TIME 1500
#define COLLISION_BACK_AWAY_TIME 1500
#define SPIN_TIME_180 3200

/*---------------------------- Module Functions ---------------------------*/
/* prototypes for private functions for this machine.They should be functions
   relevant to the behavior of this state machine
*/

/*---------------------------- Module Variables ---------------------------*/
// everybody needs a state variable, you may need others as well.
// type of state variable should match htat of enum in header file
static PositioningAgainstGWState_t  CurrentState;
static PositioningAgainstGWState_t  NextState;

//For bumper event checker
static uint8_t  LastFrontLeftCornerState = false;
static uint8_t  LastFrontRightCornerState = false;
static uint8_t  LastBackLeftCornerState = false;
static uint8_t  LastBackRightCornerState = false;

// with the introduction of Gen2, we need a module level Priority var as well
static uint8_t MyPriority;

/*------------------------------ Module Code ------------------------------*/

/****************************************************************************
 Function       InitPositioningAgainstGWFSM
 Parameters     uint8_t : the priorty of this service
 Returns        bool, false if error in initialization, true otherwise
 Description    Saves away the priority, sets up the initial transition and does any
                other required initialization for this state machine
 Notes
****************************************************************************/
bool InitPositioningAgainstGW(uint8_t Priority)
{
  ES_Event_t ThisEvent;

  MyPriority = Priority;
  // put us into the Initial PseudoState
  CurrentState = GWWaitingForStart;
  // post the initial transition event
  ThisEvent.EventType = ES_INIT;

  if (ES_PostToService(MyPriority, ThisEvent) == true)
  {
    return true;
  }
  else
  {
    return false;
  }
}

/****************************************************************************
 Function       PostPositioningAgainstGWFSM
 Parameters     EF_Event_t ThisEvent , the event to post to the queue
 Returns        boolean False if the Enqueue operation failed, True otherwise
 Description    Posts an event to this state machine's queue
 Notes
****************************************************************************/
bool PostPositioningAgainstGW(ES_Event_t ThisEvent)
{
  return ES_PostToService(MyPriority, ThisEvent);
}

/****************************************************************************
 Function       RunPositioningAgainstGWFSM
 Parameters     ES_Event_t : the event to process
 Returns        ES_Event_t, ES_NO_EVENT if no error ES_ERROR otherwise
 Description    add your description here
 Notes          uses nested switch/case to implement the machine.
****************************************************************************/
ES_Event_t RunPositioningAgainstGW(ES_Event_t ThisEvent)
{
  ES_Event_t ReturnEvent;
  ReturnEvent.EventType = ES_NO_EVENT; // assume no errors

  //XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX handle straightening when beacon sensed/not sensed

  //If we have a game over event, switch to waiting to start state
  if (ThisEvent.EventType == GAME_OVER)
  {
    NextState = GWGameOver;
    Move(FORWARD, 0);
  }
  //Or if we have an EXIT event from Master, changes state to waiting
  else if (ThisEvent.EventType == EXIT)
  {
    NextState = GWWaitingForStart;
  }
  //otherwise implement our normal state machine
  else
  {
    switch (CurrentState)
    {
      case GWWaitingForStart:
      {
        if (ThisEvent.EventType == START)
        {
          printf("\n\r in GWSM - start event received \n\r");
          NextState = GWBeaconSensing360;
          Move(COUNTER_CLOCKWISE, Slow_DC);
        }
      }
      break;

      case GWBeaconSensing360:
      {
        //printf("\n\r GW - beacon sensing 360 \n\r");
        switch (ThisEvent.EventType)
        {
          case BEACON_SENSED:
          {
            Move( FORWARD,  0);
            Move( FORWARD,  Fast_DC);
            ES_Timer_InitTimer(BEACON_SPIN_TIMER, BEACON_SPIN_TIMEOUT);
            NextState = DrivingTowardsBeacon;
            printf("\n\r beacon sensed - moving forward \n\r");
          }
          break;

          //any bumper hits during spin - back up in the opposite direction

          case BUMP:
          {
            printf("\n\r in hit a limit switch during 360 \n\r");
            switch (ThisEvent.EventParam)
            {
              case FL_HIT:
              {
                NextState = GWBackingAway;
                Move(BACKWARD, Slow_DC);
                ES_Timer_InitTimer(GARBAGE_COLLISION_BACK_AWAY_TIMER, COLLISION_BACK_AWAY_TIME);
              }
              break;

              case FR_HIT:
              {
                NextState = GWBackingAway;
                Move(BACKWARD, Slow_DC);
                ES_Timer_InitTimer(GARBAGE_COLLISION_BACK_AWAY_TIMER, COLLISION_BACK_AWAY_TIME);
              }
              break;

              case BL_HIT:
              {
                NextState = GWBackingAway;
                Move(FORWARD, Slow_DC);
                ES_Timer_InitTimer(GARBAGE_COLLISION_BACK_AWAY_TIMER, COLLISION_BACK_AWAY_TIME);
              }
              break;

              case BR_HIT:
              {
                NextState = GWBackingAway;
                Move(FORWARD, Slow_DC);
                ES_Timer_InitTimer(GARBAGE_COLLISION_BACK_AWAY_TIMER, COLLISION_BACK_AWAY_TIME);
              }
              break;
            }
          }
          break;
        }
      }
      break;

      case DrivingTowardsBeacon:
      {
        switch (ThisEvent.EventType)
        {
          case ES_TIMEOUT:
          {
            if (ThisEvent.EventParam == BEACON_SPIN_TIMER)
            {
              Move(COUNTER_CLOCKWISE, Slow_DC);
              NextState = GWBeaconSensing360;
              //ES_Timer_InitTimer(BEACON_SPIN_TIMER, BEACON_SPIN_TIMEOUT);
              printf("\n\r general beacon spin timer went off \n\r");
            }
          }
          break;

          case BUMP:  //this section is for getting flush - after this it goes into beacon sensing 180
          {
            switch (ThisEvent.EventParam)
            {
              //if front left bumper hits
              case FL_HIT:
              {
                //printf("\n\r front left bumper hit \n\r");
                LastFrontLeftCornerState = true;
                // if back right is already hit
                if (LastFrontRightCornerState)
                {
                  //move backwardsto initialize 180
                  Move(BACKWARD, Slow_DC);
                  NextState = GW180_backup;
                  //USING THIS TIMERTO SAVETIMERS(ANDNO OVERLAP)
                  ES_Timer_InitTimer(BEACON_SPIN_TIMER, BACK_AWAY_TIME);
                }
                else
                {
                  //stop left motor, up PWM on right motor
                  Move( L_WHEEL_FORWARD,  0);
                  Move( R_WHEEL_FORWARD,  Slow_DC + 25);
                  ES_Timer_InitTimer(BEACON_SPIN_TIMER, XL_BEACON_SPIN_TIMEOUT);
                  //printf("\n\r left motor stop \n\r");
                }
              }
              break;

              //front left bumper disengages
              case FL_OFF:
              {
                printf("\n\r front left bumper disengaged poo \n\r");
                LastFrontLeftCornerState = false;
                //start left motor
                Move(L_WHEEL_FORWARD, Slow_DC);
                printf("\n\r left motor start backward poo\n\r");
              }
              break;

              //if front right bumper hits
              case FR_HIT:
              {
                //printf("\n\r front right bumper hit \n\r");
                LastFrontRightCornerState = true;
                // if left is already hit
                if (LastFrontLeftCornerState)
                {
                  //move backwardsto initialize 180
                  Move(BACKWARD, Slow_DC);
                  NextState = GW180_backup;
                  //USING THIS TIMERTO SAVETIMERS(ANDNO OVERLAP)
                  ES_Timer_InitTimer(BEACON_SPIN_TIMER, BACK_AWAY_TIME);
                }
                else
                {
                  //stop right motor, up PWM on left motor
                  Move( R_WHEEL_FORWARD,  0);
                  Move( L_WHEEL_FORWARD,  Slow_DC + 25);
                  ES_Timer_InitTimer(BEACON_SPIN_TIMER, XL_BEACON_SPIN_TIMEOUT);
                  //printf("\n\r right motor stop \n\r");
                }
              }
              break;

              //front right bumper disengages
              case FR_OFF:
              {
                //printf("\n\r front right bumper disengaged \n\r");
                LastFrontRightCornerState = false;
                //start right motor forward
                Move(R_WHEEL_FORWARD, Slow_DC);
                //printf("\n\r right motor restart \n\r");
              }
              break;
            }
          }
          break;

          case BEACON_CAME_OFF_L:
          {
            Move(L_WHEEL_FORWARD, Fast_DC + 10);
            //printf("\n\r GW - beacon sense came off left side \n\r");
          }
          break;

          case BEACON_CAME_OFF_R:
          {
            Move(R_WHEEL_FORWARD, Fast_DC + 10);
            //printf("\n\r GW - beacon sense came off right side \n\r");
          }
          break;

          case BEACON_LOST:
          {
            Move(COUNTER_CLOCKWISE, Slow_DC);
            NextState = GWBeaconSensing360;
            printf("\n\r GW - beacon sense came off completely \n\r");
          }
          break;

          case BEACON_SENSED:
          {
            Move(FORWARD, Fast_DC);
            printf("\n\r GW - beacon found \n\r");
            ES_Timer_InitTimer(BEACON_SPIN_TIMER, BEACON_SPIN_TIMEOUT);
          }
          break;
        }
      }
      break;

      case GWBackingAway:
      {
        //printf("\n\r GW - hit something during a spin state \n\r");
        switch (ThisEvent.EventType)
        {
          case ES_TIMEOUT:
          {
            if (ThisEvent.EventParam == GARBAGE_COLLISION_BACK_AWAY_TIMER)
            {
              printf("\n\r GW - finished backing up, going into 360 \n\r");
              NextState = GWBeaconSensing360;
              Move(COUNTER_CLOCKWISE, Slow_DC);
            }
          }
          break;
        }
      }
      break;

      case GW180_backup:
      {
        //printf("\n\r GW - backup before 180 to get flush (on correct side) \n\r");
        switch (ThisEvent.EventType)
        {
          case ES_TIMEOUT:
          {
            if (ThisEvent.EventParam == BEACON_SPIN_TIMER)
            {
              Move(COUNTER_CLOCKWISE, Slow_DC);
              ES_Timer_InitTimer(BEACON_SPIN_TIMER, SPIN_TIME_180);
              NextState = GW180;
            }
          }
          break;
        }
      }
      break;

      case GW180:
      {
        printf("\n\r GW - 180 to get flush (on correct side) \n\r");
        switch (ThisEvent.EventType)
        {
          case ES_TIMEOUT:
          {
            //timer used to save timers - didn't want to devote a whole new one to this
            if (ThisEvent.EventParam == BEACON_SPIN_TIMER)
            {
              Move(BACKWARD, Slow_DC);
              NextState = GW180_GettingFlush;
            }
          }
          break;
        }
      }
      break;

      //need to add caseforif it gets hit by another bot here
      case GW180_GettingFlush:
      {
        printf("\n\r GW - getting flush on back side using bumpers\n\r");
        switch (ThisEvent.EventType)
        {
          case BUMP:
          {
            switch (ThisEvent.EventParam)
            {
              //if back left bumper hits
              case BL_HIT:
              {
                printf("\n\r GW - back left bumper hit\n\r");
                LastBackLeftCornerState = true;
                // if back right is already hit
                if (LastBackRightCornerState)
                {
                  //stop both motors
                  Move(FORWARD, 0);

                  ES_Event_t NewEvent;
                  NewEvent.EventType = GW_DETECTED;
                  PostMasterSM(NewEvent);
                  NextState = GWWaitingForStart;
                  //printf("\n\r flush \n\r");
                }
                else
                {
                  //stop left motor
                  Move( L_WHEEL_BACKWARD, 0);
                  Move( R_WHEEL_BACKWARD, Slow_DC + 25);
                  //ES_Timer_InitTimer(BEACON_SPIN_TIMER, XL_BEACON_SPIN_TIMEOUT);
                  //printf("\n\r left motor stop \n\r");
                }
              }
              break;

              //back left bumper disengages
              case BL_OFF:
              {
                LastBackLeftCornerState = false;
                Move(L_WHEEL_BACKWARD, Slow_DC);
                //printf("\n\r left motor start backward \n\r");
              }
              break;

              //if back right bumper hits
              case BR_HIT:
              {
                printf("\n\r GW - back right bumper hit\n\r");
                LastBackRightCornerState = true;
                // if left is already hit
                if (LastBackLeftCornerState)
                {
                  //stop both motors
                  Move(FORWARD, 0);
                  //this is the end of the demo
                  ES_Event_t NewEvent;
                  NewEvent.EventType = GW_DETECTED;
                  PostMasterSM(NewEvent);
                  NextState = GWWaitingForStart;
                  //printf("\n\r flush \n\r");
                }
                else
                {
                  //stop right motor
                  Move( R_WHEEL_BACKWARD, 0);
                  Move( L_WHEEL_BACKWARD, Slow_DC + 25);
                  //printf("\n\r right motor stop \n\r");
                }
              }
              break;

              //back right bumper disengages
              case BR_OFF:
              {
                LastBackRightCornerState = false;
                Move(R_WHEEL_BACKWARD, Slow_DC);
                printf("\n\r right motor start backward \n\r");
              }
              break;
            }
          }
          break;
        }
      }
      break;

      default:
        ;
    }
  }
  CurrentState = NextState;
  return ReturnEvent;
}

/****************************************************************************
 Function       QueryPositioningAgainstGW
 Parameters     None
 Returns        PositioningAgainstGWState_t The current state of the
                PositioningAgainstGW state machine
 Description    returns the current state of the PositioningAgainstGW state machine
 Notes
****************************************************************************/
PositioningAgainstGWState_t QueryPositioningAgainstGW(void)
{
  return CurrentState;
}

/***************************************************************************
 private functions
 ***************************************************************************/

/****************************************************************************
 Function       BumperEventChecker
 Parameters     None
 Returns        None
 Description    Sends bumper events to the proper state machines
 Notes
****************************************************************************/
bool BumperEventChecker(void)
{
  bool    ReturnVal = false;

  uint8_t FL = PORTD & LIMIT_FL;
  uint8_t FR = PORTD & LIMIT_FR;
  uint8_t BL = PORTD & LIMIT_RL;
  uint8_t BR = PORTD & LIMIT_RR;

  if (FL != LastFrontLeftCornerState)
  {
    ES_Event_t NewEvent;
    NewEvent.EventType = BUMP;
    if (FL > 0)
    {
      //printf("\n\r front left corner hit");
      NewEvent.EventParam = FL_HIT;
      PostPositioningAgainstGW(NewEvent);
      PostRecycling(NewEvent);
      PostMasterSM(NewEvent);
    }
    else
    {
      //printf("\n\r front left corner came off");
      NewEvent.EventParam = FL_OFF;
      PostPositioningAgainstGW(NewEvent);
      PostRecycling(NewEvent);
      PostMasterSM(NewEvent);
    }
    LastFrontLeftCornerState = FL;
    ReturnVal = true;
  }

  if (FR != LastFrontRightCornerState)
  {
    ES_Event_t NewEvent;
    NewEvent.EventType = BUMP;
    if (FR > 0)
    {
      //printf("\n\r front right corner hit");
      NewEvent.EventParam = FR_HIT;
      PostPositioningAgainstGW(NewEvent);
      PostRecycling(NewEvent);
      //PostCollisionDetector(NewEvent);
      PostMasterSM(NewEvent);
    }
    else
    {
      //printf("\n\r front right corner came off");
      NewEvent.EventParam = FR_OFF;
      PostPositioningAgainstGW(NewEvent);
      PostRecycling(NewEvent);
      PostMasterSM(NewEvent);
    }
    LastFrontRightCornerState = FR;
    ReturnVal = true;
  }

  if (BL != LastBackLeftCornerState)
  {
    ES_Event_t NewEvent;
    NewEvent.EventType = BUMP;
    if (BL > 0)
    {
      //printf("\n\r back left corner hit");
      NewEvent.EventParam = BL_HIT;
      printf("\n\r GW - back left bumper hit\n\r");
      PostPositioningAgainstGW(NewEvent);
      PostRecycling(NewEvent);
      //PostCollisionDetector(NewEvent);
      PostMasterSM(NewEvent);
    }
    else
    {
      //printf("\n\r back left corner came off");
      NewEvent.EventParam = BL_OFF;
      PostPositioningAgainstGW(NewEvent);
      PostRecycling(NewEvent);
      PostMasterSM(NewEvent);
    }
    LastBackLeftCornerState = BL;
    ReturnVal = true;
  }

  if (BR != LastBackRightCornerState)
  {
    ES_Event_t NewEvent;
    NewEvent.EventType = BUMP;
    if (BR > 0)
    {
      //printf("\n\r back right corner hit");
      NewEvent.EventParam = BR_HIT;
      printf("\n\r GW - back right bumper hit\n\r");
      PostPositioningAgainstGW(NewEvent);
      PostRecycling(NewEvent);
      //PostCollisionDetector(NewEvent);
      PostMasterSM(NewEvent);
    }
    else
    {
      //printf("\n\r back right  corner came off");
      NewEvent.EventParam = BR_OFF;
      PostPositioningAgainstGW(NewEvent);
      PostRecycling(NewEvent);
      PostMasterSM(NewEvent);
    }
    LastBackRightCornerState = BR;
    ReturnVal = true;
  }

  return ReturnVal;
}

/*------------------------------ End of file ------------------------------*/
