/****************************************************************************
 Module: PortB.c
 Revision 1.0.1
 Description: This module includes basic functions for manipulating pins on all ports
 on Port B
 Notes:
 History
 When           Who     What/Why
 -------------- ---     --------
 10/11/15 19:55 jec     First Creation

 ****************************************************************************/

/*----------------------------- Include Files -----------------------------*/
//include header files for this c file
#include "Ports.h"

//For debugging
//#include "termio.h"

/*----------------------------- Module Functions -----------------------------*/
/*Initialize a particular Port*/
void EnablePort(PortName_t Port)
{
  //Construct the proper bit mask based on the desired port
  uint8_t   shift = (int)Port;          //Assumes the first enum PA has value 0
  uint16_t  PORT_BIT = BIT0HI << shift; //So if we have Port = PB = 1, PORT_BIT effectively becomes BIT1HI

  //Enable Port
  HWREG(SYSCTL_RCGCGPIO) |= PORT_BIT;

  //Stall for a few clock cycle
  while ((HWREG(SYSCTL_PRGPIO) & PORT_BIT) != PORT_BIT)
  {
    //Wait until port initializes
  }
}

/*Sets pins corresponding to high bits in "PINS" to
Digital.*/
void SetPin2Digital(PortName_t Port, uint8_t PINS)
{
  switch (Port)
  {
    case PA:
    {
      HWREG(GPIO_PORTA_BASE + GPIO_O_DEN) |= PINS;
    }
    break;
    case PB:
    {
      HWREG(GPIO_PORTB_BASE + GPIO_O_DEN) |= PINS;
    }
    break;
    case PC:
    {
      HWREG(GPIO_PORTC_BASE + GPIO_O_DEN) |= PINS;
    }
    break;
    case PD:
    {
      HWREG(GPIO_PORTD_BASE + GPIO_O_DEN) |= PINS;
    }
    break;
    case PE:
    {
      HWREG(GPIO_PORTE_BASE + GPIO_O_DEN) |= PINS;
    }
    break;
    case PF:
    {
      HWREG(GPIO_PORTF_BASE + GPIO_O_DEN) |= PINS;
    }
    break;
  }
}

/*Sets pins corresponding to high bits in "PINS" to
be output pins. A pin is an output if the corresponding
bit in the register is HIGH*/
void SetPin2Output(PortName_t Port, uint8_t PINS)
{
  switch (Port)
  {
    case PA:
    {
      HWREG(GPIO_PORTA_BASE + GPIO_O_DIR) |= PINS;
    }
    break;
    case PB:
    {
      HWREG(GPIO_PORTB_BASE + GPIO_O_DIR) |= PINS;
    }
    break;
    case PC:
    {
      HWREG(GPIO_PORTC_BASE + GPIO_O_DIR) |= PINS;
    }
    break;
    case PD:
    {
      HWREG(GPIO_PORTD_BASE + GPIO_O_DIR) |= PINS;
    }
    break;
    case PE:
    {
      HWREG(GPIO_PORTE_BASE + GPIO_O_DIR) |= PINS;
    }
    break;
    case PF:
    {
      HWREG(GPIO_PORTF_BASE + GPIO_O_DIR) |= PINS;
    }
    break;
  }
}

/*Sets pins corresponding to high bits in "PINS" to
be input pins. A pin is an input if the corresponding
bit in the register is LOW. */
void SetPin2Input(PortName_t Port, uint8_t PINS)
{
  switch (Port)
  {
    case PA:
    {
      HWREG(GPIO_PORTA_BASE + GPIO_O_DIR) &= ~PINS;
    }
    break;
    case PB:
    {
      HWREG(GPIO_PORTB_BASE + GPIO_O_DIR) &= ~PINS;
    }
    break;
    case PC:
    {
      HWREG(GPIO_PORTC_BASE + GPIO_O_DIR) &= ~PINS;
    }
    break;
    case PD:
    {
      HWREG(GPIO_PORTD_BASE + GPIO_O_DIR) &= ~PINS;
    }
    break;
    case PE:
    {
      HWREG(GPIO_PORTE_BASE + GPIO_O_DIR) &= ~PINS;
    }
    break;
    case PF:
    {
      HWREG(GPIO_PORTF_BASE + GPIO_O_DIR) &= ~PINS;
    }
    break;
  }
}

/*----------------------------- Test Harness -----------------------------*/
//#define PORTS_TEST
#ifdef PORTS_TEST

//Debug
#include "termio.h"
#include "Parameters.h"

#include <stdio.h>
#include "ES_Configure.h"
#include "ES_Framework.h"
#include "ES_Port.h"
#include "termio.h"
#include "EnablePA25_PB23_PD7_PF0.h"

#define clrScrn() printf("\x1b[2J")
#define goHome() printf("\x1b[1,1H")
#define clrLine() printf("\x1b[K")

static void InitGPIO(void)
{
  //Enable Ports A-F
  EnablePort( PA);
  EnablePort( PB);
  EnablePort( PC);
  EnablePort( PD);
  EnablePort( PE);
  EnablePort( PF);

  //---Enable GPIO Pins as digital inputs/outputs---

  //Limit Switches
  SetPin2Digital(PD, LIMIT_FL | LIMIT_FR | LIMIT_RL | LIMIT_RR);
  SetPin2Input(PD, LIMIT_FL | LIMIT_FR | LIMIT_RL | LIMIT_RR);

  //North/South Team status pin
  SetPin2Digital(PF, TEAM_STATUS_GPIO);
  SetPin2Input(PF, TEAM_STATUS_GPIO);

  //Wheel motor pins
  SetPin2Digital(PC, WHEEL_L_GPIO | WHEEL_R_GPIO);
  SetPin2Output(PC, WHEEL_L_GPIO | WHEEL_R_GPIO);

  //Conveyor Motor Pin
  SetPin2Digital(PF, CONVEYOR_MOTOR_GPIO);
  SetPin2Output(PF, CONVEYOR_MOTOR_GPIO);
}

int main(void)
{
  // Set the clock to run at 40MhZ using the PLL and 16MHz external crystal
  SysCtlClockSet(SYSCTL_SYSDIV_5 | SYSCTL_USE_PLL | SYSCTL_OSC_MAIN
      | SYSCTL_XTAL_16MHZ);
  TERMIO_Init();
  clrScrn();

  // When doing testing, it is useful to announce just which program
  // is running.
  puts("\rGPIO Test Harnessfor \r");
  printf( "GPIO_test\r\n");
  printf( "%s %s\n", __TIME__, __DATE__);
  printf( "\n\r\n");
  printf( "Press any key to post key-stroke events to Service 0\n\r");
  printf( "Press 'd' to test event deferral \n\r");
  printf( "Press 'r' to test event recall \n\r");

  //Init GPIO Lines
  InitGPIO();
  printf("\n\rGPIO Init Success");

  //Set all GPIO output pins to HIGH
  PORTC |= PC6;   //3.3
  PORTC |= PC7;   //3.3
  PORTF |= PF0;   //3.0
  printf("\n\rGPIOs Set");

  //Read the input pin
  uint32_t read;
  read = PORTD & LIMIT_FL;
  printf( "\n\rRead is %d", read);

  read = PORTD & LIMIT_FR;
  printf( "\n\rRead is %d", read);

  read = PORTD & LIMIT_RL;
  printf( "\n\rRead is %d", read);

  //read = PORTD & BIT7HI; //LIMIT_RR;  //BROKEN!!
  //printf("\n\rRead is %d",read); //Not reading input. PD7

  read = PORTF & TEAM_STATUS_GPIO;
  printf("\n\rRead is %d", read);

  //More Tests
  SetPin2Digital(PA, PA2 | PA3 | PA4);
  SetPin2Output(PA, PA2 | PA3 | PA4);
  PORTA |= (PA2 | PA3 | PA4);

  SetPin2Digital(PB, PB2 | PB3 | PB4);
  SetPin2Output(PB, PB2 | PB3 | PB4);
  PORTB |= (PB2 | PB3 | PB4);

  SetPin2Digital(PE, PE2 | PE3 | PE4);
  SetPin2Output(PE, PE2 | PE3 | PE4);
  PORTE |= (PE2 | PE3 | PE4);
  //All good except PA2...

  //Test to see if PD7 can output
  SetPin2Output(PD, PD6 | PD7);
  PORTD |= (PD6 | PC7);
  printf("\n\rOutpute on PD7");
  //--> output on PD6 but not PD7

  while (1)
  {
    ;
  }
  return 0;
}

#endif
