/****************************************************************************
 Module
   BeaconService.c

 Revision
   1.0.1

 Description
   This is a template file for implementing a simple service under the
   Gen2 Events and Services Framework.

 Notes
****************************************************************************/

/*----------------------------- Include Files -----------------------------*/
//include header files for this state machine as well as any machines at the
//next lower level in the hierarchy that are sub-machines to this machine
#include "ES_Configure.h"
#include "ES_Framework.h"
#include "BeaconService.h"

//For Beacon IC interrupts
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_timer.h"

#include "PositioningAgainstGW.h"
#include "CompassSM.h"
#include "Ports.h"
#include <math.h>
#include "CollisionDetector.h"
#include "Parameters.h"

#include "MasterSM.h"
#include "Recycling.h"
#include "termio.h"

/*----------------------------- Module Defines ----------------------------*/
//Beacon Stuff
#define TimerTicsPerUS 40
#define PERIOD_TIMEOUT_TIME 500

#define TickThreshold 25

/*---------------------------- Module Functions ---------------------------*/
// prototypes for private functions for this service.They should be functions
//  relevant to the behavior of this service

static bool SensePeriods(int microseconds, bool L);

static void UpdateLeftPeriod(uint32_t NullPeriod);
static void UpdateRightPeriod(uint32_t NullPeriod);

/*---------------------------- Module Variables ---------------------------*/
// with the introduction of Gen2, we need a module level Priority variable
static uint8_t MyPriority;

//For IR Beacon Sensors
static uint32_t ThisCaptureRight;
static uint32_t LastCaptureRight;
static uint32_t ThisCaptureLeft;
static uint32_t LastCaptureLeft;
static uint32_t PeriodRight;
static uint32_t PeriodLeft;

//For beacon period filtering
static uint16_t NumTruL;
static uint16_t NumTruR;

//For beacon event checker
static bool LastBeaconStateL = 0;
static bool LastBeaconStateR = 0;

//Recycling center and landfill periods
static uint16_t LandFillPeriodMicroseconds;
static uint16_t RecyclingPeriodMicroseconds = 500;

/*------------------------------ Module Code ------------------------------*/

/****************************************************************************
 Function       InitBeaconService
 Parameters     uint8_t : the priorty of this service
 Returns        bool, false if error in initialization, true otherwise
 Description    Saves away the priority, sets up the initial transition and does any
                other required initialization for this state machine
****************************************************************************/
bool InitBeaconService(uint8_t Priority)
{
  printf("\n\rInit BeaconService");
  ES_Event_t ThisEvent;

  MyPriority = Priority;

  // post the initial transition event
  ThisEvent.EventType = ES_INIT;
  if (ES_PostToService(MyPriority, ThisEvent) == true)
  {
    return true;
  }
  else
  {
    return false;
  }
}

/****************************************************************************
 Function       PostBeaconService
 Parameters     EF_Event_t ThisEvent , the event to post to the queue
 Returns        boolean False if the Enqueue operation failed, True otherwise
 Description    Posts an event to this state machine's queue
 Notes
****************************************************************************/
bool PostBeaconService(ES_Event_t ThisEvent)
{
  return ES_PostToService(MyPriority, ThisEvent);
}

/****************************************************************************
 Function       RunBeaconService
 Parameters     ES_Event_t : the event to process
 Returns        ES_Event_t, ES_NO_EVENT if no error ES_ERROR otherwise
 Description    Utilities for the beacon sensors
 Notes          uses nested switch/case to implement the machine.
****************************************************************************/
ES_Event_t RunBeaconService(ES_Event_t ThisEvent)
{
  ES_Event_t ReturnEvent;
  ReturnEvent.EventType = ES_NO_EVENT; // assume no errors

  if (ThisEvent.EventType == ES_TIMEOUT)
  {
    switch (ThisEvent.EventParam)
    {
      case LEFT_BEACON_PERIOD_TIMEOUT:
      {
        UpdateLeftPeriod(0);
      }
      break;

      case RIGHT_BEACON_PERIOD_TIMEOUT:
      {
        UpdateRightPeriod(0);
      }
      break;
    }
  }
  return ReturnEvent;
}

//-------------BEACON Interrupts---------------------------------------------------

/****************************************************************************
Function:     RightICTestResponse
Parameters:   None
Returns:      None
Description:  ISR functionality for input capture - Beacon right
Author:       S. Diehl
****************************************************************************/
void RightICTestResponse(void)
{
  //Clear interrupt flag
  HWREG(WTIMER2_BASE + TIMER_O_ICR) = TIMER_ICR_CAECINT;
  ThisCaptureRight = HWREG(WTIMER2_BASE + TIMER_O_TAR);
  PeriodRight = (ThisCaptureRight - LastCaptureRight) / TimerTicsPerUS;

  LastCaptureRight = ThisCaptureRight;

  ES_Timer_InitTimer(RIGHT_BEACON_PERIOD_TIMEOUT, PERIOD_TIMEOUT_TIME);

  //printf("\n\rBeacon 1");
}

/****************************************************************************
Function:     LeftICTestResponse
Parameters:   None
Returns:      None
Description:  ISR functionality for input capture - Beacon left
Author:       S. Diehl
****************************************************************************/
void LeftICTestResponse(void)
{
  //Clear interrupt flag
  HWREG(WTIMER2_BASE + TIMER_O_ICR) = TIMER_ICR_CBECINT;
  ThisCaptureLeft = HWREG(WTIMER2_BASE + TIMER_O_TBR);
  PeriodLeft = (ThisCaptureLeft - LastCaptureLeft) / TimerTicsPerUS;

  LastCaptureLeft = ThisCaptureLeft;

  ES_Timer_InitTimer(LEFT_BEACON_PERIOD_TIMEOUT, PERIOD_TIMEOUT_TIME);

  //printf("\n\rBeacon 2");
}

//-------------BEACON Helper function---------------------------------------------------

/****************************************************************************
Function:     SetRecyclingPeriod
Parameters:   uint16_t NewPeriod
Returns:      None
Description:  Sets recycling center period. Called from CompassSM.c
Author:       S. Diehl
****************************************************************************/
void SetRecyclingPeriod(uint16_t NewPeriod)
{
  RecyclingPeriodMicroseconds = NewPeriod;
}

/****************************************************************************
Function:     SetLandfillPeriod
Parameters:   uint16_t NewPeriod
Returns:      None
Description:  Sets landfill period. Called from CompassSM.c
Author:       S. Diehl
****************************************************************************/
void SetLandfillPeriod(uint16_t NewPeriod)
{
  LandFillPeriodMicroseconds = NewPeriod;
}

/****************************************************************************
Function:     LeftICTestResponse
Parameters:   None
Returns:      None
Description:  Used set period to zero after a timeout
Author:       S. Diehl
****************************************************************************/
static void UpdateRightPeriod(uint32_t NullPeriod)
{
  PeriodRight = NullPeriod;
}

/****************************************************************************
Function:     LeftICTestResponse
Parameters:   None
Returns:      None
Description:  Used to set period to zero after a timeout
Author:       S. Diehl
****************************************************************************/
static void UpdateLeftPeriod(uint32_t NullPeriod)
{
  PeriodLeft = NullPeriod;
}

//-------------BEACON Event Checker functions---------------------------------------------------

/****************************************************************************
 Function       BeaconEventChecker
 Parameters     None
 Returns        None
 Description    Sends beacon events to the proper state machines
 Notes
****************************************************************************/
bool BeaconEventChecker(void)
{
  bool  ReturnVal = false;

  bool  OnBeaconL = SensePeriods(PeriodLeft, true);
  bool  OnBeaconR = SensePeriods(PeriodRight, false);
  //printf("\n\rOnBeaconL %d", OnBeaconL);
  //printf("\n\rOnBeaconR %d\n\r", OnBeaconR);

  if ((OnBeaconL != LastBeaconStateL) || (OnBeaconR != LastBeaconStateR))
  {
    ES_Event_t NewEvent;
    if (!OnBeaconL && OnBeaconR)
    {
      NewEvent.EventType = BEACON_CAME_OFF_L;
      //maybe make a distribution list instead...
      PostPositioningAgainstGW(NewEvent);
      PostRecycling(NewEvent);
    }
    else if (!OnBeaconR && OnBeaconL)
    {
      NewEvent.EventType = BEACON_CAME_OFF_R;
      PostPositioningAgainstGW(NewEvent);
      PostRecycling(NewEvent);
    }
    else if (OnBeaconL && OnBeaconR)
    {
      NewEvent.EventType = BEACON_SENSED;
      PostPositioningAgainstGW(NewEvent);
      PostRecycling(NewEvent);
    }
    else if (!OnBeaconL && !OnBeaconR)
    {
      NewEvent.EventType = BEACON_LOST;
      //printf("\n\r BEACON LOST AHHHHHHH \n\r");
      PostPositioningAgainstGW(NewEvent);
      PostRecycling(NewEvent);
    }
    ReturnVal = true;
    LastBeaconStateL = OnBeaconL;
    LastBeaconStateR = OnBeaconR;
  }
  return ReturnVal;
}

/****************************************************************************
 Function       SensePeriods
 Parameters     None
 Returns        None
 Description    A filter for detecting the beacon periods. Returns true
                if 70/100 of the last periods fall within the desired range
 Notes
****************************************************************************/
static bool SensePeriods(int microseconds, bool L)
{
  if (QueryMasterSM() == Recycling)
  {
    if (L)
    {
      if ((abs(microseconds - RecyclingPeriodMicroseconds) < TickThreshold) && (NumTruL < 10))
      {
        NumTruL++;
      }
      else if ((abs(microseconds - RecyclingPeriodMicroseconds) > TickThreshold) && (NumTruL > 0))
      {
        NumTruL--;
      }
      //printf("\n\r NumTruL = %d\n\r", NumTruL);
      //printf("\n\r masterstate = recycling: = %d\n\r", QueryMasterSM() == Recycling);
      //printf("\n\r RecyclingPeriodMicroseconds = %d\n\r", RecyclingPeriodMicroseconds);
      return NumTruL >= 7;
    }
    else
    {
      if ((abs(microseconds - RecyclingPeriodMicroseconds) < TickThreshold) && (NumTruR < 10))
      {
        NumTruR++;
      }
      else if ((abs(microseconds - RecyclingPeriodMicroseconds) > TickThreshold) && (NumTruR > 0))
      {
        NumTruR--;
      }
      //printf("\n\r NumTruR = %d\n\r", NumTruR);

      return NumTruR >= 7;
    }
  }
  else
  {
    if (L)
    {
      if ((abs(microseconds - LandFillPeriodMicroseconds) < TickThreshold) && (NumTruL < 10))
      {
        NumTruL++;
      }
      else if ((abs(microseconds - LandFillPeriodMicroseconds) > TickThreshold) && (NumTruL > 0))
      {
        NumTruL--;
      }
      //printf("\n\r NumTruL = %d\n\r", NumTruL);

      return NumTruL >= 7;
    }
    else
    {
      if ((abs(microseconds - LandFillPeriodMicroseconds) < TickThreshold) && (NumTruR < 10))
      {
        NumTruR++;
      }
      else if ((abs(microseconds - LandFillPeriodMicroseconds) > TickThreshold) && (NumTruR > 0))
      {
        NumTruR--;
      }
      //printf("\n\r NumTruR = %d\n\r", NumTruR);

      return NumTruR >= 7;
    }
  }
}

void ClearBeaconDetectors(void)
{
  LastBeaconStateL = false;
  LastBeaconStateR = false;
}

/*------------------------------ End of file ------------------------------*/
