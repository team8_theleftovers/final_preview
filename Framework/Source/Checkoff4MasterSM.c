/****************************************************************************
 Module
   Checkoff4MasterSM.c

 Revision
   1.0.1

 Description
   This is a template file for implementing flat state machines under the
   Gen2 Events and Services Framework.

 Notes

 History
 When           Who     What/Why
 -------------- ---     --------
 01/15/12 11:12 jec      revisions for Gen2 framework
 11/07/11 11:26 jec      made the queue static
 10/30/11 17:59 jec      fixed references to CurrentEvent in RunCheckoff4MasterSM()
 10/23/11 18:20 jec      began conversion from SMCheckoff4Master.c (02/20/07 rev)
****************************************************************************/
/*----------------------------- Include Files -----------------------------*/
/* include header files for this state machine as well as any machines at the
   next lower level in the hierarchy that are sub-machines to this machine
*/
#include "ES_Configure.h"
#include "ES_Framework.h"
#include "Checkoff4MasterSM.h"

#include "Ports.h"
#include "Parameters.h"
#include "PWM.h"
#include "Timers.h"
//#include "SSIModule.h"
#include "HardwareInit.h"
#include "SortingModule.h"
#include "Recycling.h"

#include "Move.h"

#include "CompassModule.h"
#include "PositioningAgainstGW.h"

/*----------------------------- Module Defines ----------------------------*/

#define LIMIT 3
#define BACKUP_TIME 2000
#define DEPOSIT_WASTE_TIME 2000
#define SERVO_OPEN_DC 60
#define SERVO_CLOSED_DC 0
#define FullSpeed_DC 100
#define Slow_DC 30

#define COLLECTING_POLL_TIME 100
/*---------------------------- Module Functions ---------------------------*/
/* prototypes for private functions for this machine.They should be functions
   relevant to the behavior of this state machine
*/

/*---------------------------- Module Variables ---------------------------*/
// everybody needs a state variable, you may need others as well.
// type of state variable should match htat of enum in header file
static Checkoff4MasterSMState_t CurrentState;
static Checkoff4MasterSMState_t NextState;

// with the introduction of Gen2, we need a module level Priority var as well
static uint8_t  MyPriority;
static uint16_t GarbageFreq;
static uint16_t RecyclingFreq;

/*------------------------------ Module Code ------------------------------*/
/****************************************************************************
 Function
     InitCheckoff4MasterSM

 Parameters
     uint8_t : the priorty of this service

 Returns
     bool, false if error in initialization, true otherwise

 Description
     Saves away the priority, sets up the initial transition and does any
     other required initialization for this state machine
 Notes

 Author
     J. Edward Carryer, 10/23/11, 18:55
****************************************************************************/
bool InitCheckoff4MasterSM(uint8_t Priority)
{
  ES_Event_t ThisEvent;

  MyPriority = Priority;
  // put us into the Initial PseudoState
  CurrentState = Checkoff4MasterWaitingForStart;
  // post the initial transition event
  ThisEvent.EventType = ES_INIT;

  //XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX need to initialize N/S variables

  if (ES_PostToService(MyPriority, ThisEvent) == true)
  {
    return true;
  }
  else
  {
    return false;
  }
}

/****************************************************************************
 Function
     PostCheckoff4MasterSM

 Parameters
     EF_Event_t ThisEvent , the event to post to the queue

 Returns
     boolean False if the Enqueue operation failed, True otherwise

 Description
     Posts an event to this state machine's queue
 Notes

 Author
     J. Edward Carryer, 10/23/11, 19:25
****************************************************************************/
bool PostCheckoff4MasterSM(ES_Event_t ThisEvent)
{
  return ES_PostToService(MyPriority, ThisEvent);
}

/****************************************************************************
 Function
    RunCheckoff4MasterSM

 Parameters
   ES_Event_t : the event to process

 Returns
   ES_Event_t, ES_NO_EVENT if no error ES_ERROR otherwise

 Description
   add your description here
 Notes
   uses nested switch/case to implement the machine.
 Author
   J. Edward Carryer, 01/15/12, 15:23
****************************************************************************/
ES_Event_t RunCheckoff4MasterSM(ES_Event_t ThisEvent)
{
  ES_Event_t ReturnEvent;
  ReturnEvent.EventType = ES_NO_EVENT; // assume no errors

  switch (CurrentState)
  {
    case Checkoff4MasterWaitingForStart:
    {
      if (ThisEvent.EventType == CLEANING_UP)
      {
        ES_Event_t NewEvent;
        NewEvent.EventType = START;
        GarbageFreq = GetGarbageFreq();
        RecyclingFreq = GetRecyclingFreq();
        PostRecycling(NewEvent);
        NextState = Recycling;
        printf("\n\r in master - going into recycling \n\r");

        //should send everything to positioningagainstGW in the real thing
      }
    }
    break;

    case PositioningAgainstGW:        // If current state is state one
    {
      //CONCERN - THIS WILL QUERY VERY QUICKLY WHILE IN POSITIONING AGAINST GW - come back in and set a
      //timeout for this
      //maybe have the parallel ball checker state machine post to here instead and have
      //a case for each state that sends you into recycling
      //dont need to deal with case where u get another recycling ball
      if (QueryRecycling() >= 1)
      {
        ES_Event_t NewEvent;
        NewEvent.EventType = START;
        PostRecycling(NewEvent);
        NextState = Recycling;
      }
      else
      {
        switch (ThisEvent.EventType)
        {
          case GW_DETECTED:
          {
            if (QueryGarbageCount() >= LIMIT)
            {
              NextState = DumpingGarbage;
              Move(FORWARD, Slow_DC);
              ES_Timer_InitTimer(BACKUP_DEPOSIT_TIMER, BACKUP_TIME);
            }
            else
            {
              NextState = Checkoff4MasterWaitingInCollecting;
              ES_Timer_InitTimer(COLLECTING_POLL_TIMER, COLLECTING_POLL_TIME);

              //don't actually want to change state of positioningAgainstGW
              //at this point - already in "doing nothing"
              //just want to tell it to do timed sequence to get to collection
              //position, but that can be done in here... at some point)

              //ES_Event_t NewEvent;
              //NewEvent.EventType = START;
              //PostPositioningAgainstGW(NewEvent);
            }
          }
          break;

          case ES_TIMEOUT:
          {
            SetDC(SERVO_1, SERVO_CLOSED_DC);
            NextState = Checkoff4MasterWaitingInCollecting;
            ES_Timer_InitTimer(COLLECTING_POLL_TIMER, COLLECTING_POLL_TIME);
          }
          break;

          case GAME_OVER:
          {
            NextState = Checkoff4MasterWaitingForStart;
          }
          break;
        }

        break;
      }
    }
    break;

    case DumpingGarbage:
    {
      //nothing to send it into recycling here - will nly register once it finishes dumping garbage
      //and goes back into positioning against GW
      switch (ThisEvent.EventType)
      {
        case ES_TIMEOUT:
        {
          Move(FORWARD, 0);
          NextState = PositioningAgainstGW;
          SetDC(SERVO_1, SERVO_OPEN_DC);
          ES_Timer_InitTimer(SERVO_TIMER, DEPOSIT_WASTE_TIME);
        }
        break;

        case GAME_OVER:
        {
          NextState = Checkoff4MasterWaitingForStart;
        }
        break;
      }
    }
    break;

    case Checkoff4MasterWaitingInCollecting:
    {
      if (ThisEvent.EventParam == ES_TIMEOUT)
      {
        if (QueryRecycling() >= 1)
        {
          NextState = Recycling;
          ES_Event_t NewEvent;
          NewEvent.EventType = START;
          PostRecycling(NewEvent);
        }
        else if (QueryGarbageCount() >= LIMIT)
        {
          NextState = PositioningAgainstGW;
          ES_Event_t NewEvent;
          NewEvent.EventType = START;
          PostPositioningAgainstGW(NewEvent);
        }
        ES_Timer_InitTimer(COLLECTING_POLL_TIMER, COLLECTING_POLL_TIME);
      }
      if (ThisEvent.EventType == GAME_OVER)
      {
        NextState = Checkoff4MasterWaitingForStart;
      }
    }
    break;

    case Recycling:
    {
      switch (ThisEvent.EventType)
      {
        case EXIT:
        {
          NextState = PositioningAgainstGW;
          ES_Event_t NewEvent;
          NewEvent.EventType = START;
          PostCheckoff4MasterSM(NewEvent);
        }
        break;

        case GAME_OVER:
        {
          NextState = Checkoff4MasterWaitingForStart;
        }
      }
      default:
        ;
    }
    break;
  }    // end switch on Current State

  CurrentState = NextState;
  return ReturnEvent;
}

/****************************************************************************
 Function
     QueryCheckoff4MasterSM

 Parameters
     None

 Returns
     Checkoff4MasterState_t The current state of the Checkoff4Master state machine

 Description
     returns the current state of the Checkoff4Master state machine
 Notes

 Author
     J. Edward Carryer, 10/23/11, 19:21
****************************************************************************/
Checkoff4MasterSMState_t QueryCheckoff4MasterSM(void)
{
  return CurrentState;
}

/***************************************************************************
 private functions
 ***************************************************************************/