/****************************************************************************
 Module         SSIModule.c

 Revision       1.0.1

 Description    Functions for SSI module

 Notes

 History

****************************************************************************/
/*----------------------------- Include Files -----------------------------*/

// the common headers for C99 types
#include <stdint.h>
#include <stdbool.h>

//Header Files needed to make HWREG work, and BITDEFS
//include header files for hardware access
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_gpio.h"
#include "inc/hw_sysctl.h"
#include "inc/hw_ssi.h"
#include "inc/hw_nvic.h"

// the headers to access the TivaWare Library
#include "driverlib/sysctl.h"
#include "driverlib/pin_map.h"
#include "driverlib/gpio.h"
#include "driverlib/timer.h"
#include "driverlib/interrupt.h"

#include "BITDEFS.H"

//For debugging
#include "termio.h"

/*----------------------------- Defines -----------------------------*/

//Pin descriptions
/*SSI Module 0
SSI0CLK --> PA2
SSI0Fss --> PA3
SSI0Rx  --> PA4
SSI0Tx  --> PA5
Alternate functions on page 650
*/
#define BitsPerNibble 4
#define PA2 BIT2HI
#define PA3 BIT3HI
#define PA4 BIT4HI
#define PA5 BIT5HI

/*----------------------------- Private function declarations -----------------------------*/

/*----------------------------- Module code -----------------------------*/

void InitSSI(void)
{
  printf("\n\rInitSSI\r\n");
  //1.Enable the clock to the GPIO port A
  HWREG(SYSCTL_RCGCGPIO) |= BIT0HI;

  //2.Enable the clock to the SSI Module 0
  HWREG(SYSCTL_RCGCSSI) |= BIT0HI;

  //3. Stall for a few clock cycles until GPIO port is ready
  while ((HWREG(SYSCTL_PRGPIO) & BIT0HI) != BIT0HI)
  {
    ; //Wait until port initializes
  }

  //4. Program the GPIO to use the alternate functions on the SSI Pins
  //Select the alternate function for PA2-5
  HWREG(GPIO_PORTA_BASE + GPIO_O_AFSEL) |= (PA2 | PA3 | PA4 | PA5);

  //5. NOTE - PCTL bits already mapped to SSIO function on RESET.

  //6.Enable all SSI 0 pins as digital pins
  HWREG(GPIO_PORTA_BASE + GPIO_O_DEN) |= (PA2 | PA3 | PA4 | PA5);

  //7. make pins 2,3,5 on Port A into outputs
  HWREG(GPIO_PORTA_BASE + GPIO_O_DIR) |= (PA2 | PA3 | PA5);

  // make pin 4 on Port A into an input
  HWREG(GPIO_PORTA_BASE + GPIO_O_DIR) &= ~PA4;

  //8. If using SPI mode 3, program the pull-up on the clock line (PA2)
  HWREG(GPIO_PORTA_BASE + GPIO_O_PUR) |= (PA2 | PA3 | PA4 | PA5);

  //9. Make sure SSIO is initialized before continuing
  while ((HWREG(SYSCTL_PRSSI) & BIT0HI) != BIT0HI)
  {
    ; //Wait until SSI0 initializes
  }

  //10-11.Make sure that the SSI is disabled before programming mode bits,
  //and select master mode (all bits zeroed).
  HWREG(SSI0_BASE + SSI_O_CR1) = 0x00000000;

  //Select interrupt to occur when FIFO is empty (EOT)
  HWREG(SSI0_BASE + SSI_O_CR1) |= BIT4HI;
  ;

  //12.Configure the SSI clock source to the system clock
  HWREG(SSI0_BASE + SSI_O_CC) |= 0x0;

  //13. Configure the clock pre-scaler
  // BR=SysClk/(CPSDVSR * (1 + SCR))
  HWREG(SSI0_BASE + SSI_O_CPSR) |= 0xFE; //100

  //14.Configure Clock rate(SCR), phase and polarity (SPH, SPO),
  //mode(FRF),and datasize(DSS)
  //SCR = 0
  //SPH = 1
  //SPO = 1
  //FRF = 0
  //DSS =
  uint32_t Divisor = 0xB << 8;//0x32 << 8; //Divisor value of 11, shifted right 8 to get in right position
  HWREG(SSI0_BASE + SSI_O_CR0) |= (SSI_CR0_SPH | SSI_CR0_SPO | SSI_CR0_DSS_8 | Divisor);

  //15. Locally enable interrups (TXIM in SSIIM)
  //HWREG(SSI0_BASE+SSI_O_IM) |=  SSI_IM_TXIM; //TXIM bit = 1 (not masked)

  //15(alt). OR disable the interrupt if you don't want to use it
  HWREG(SSI0_BASE + SSI_O_IM) &= ~SSI_IM_TXIM; //TXIM bit = 0 (masked)

  //16. Make sure the SSI is enabled for operation
  HWREG(SSI0_BASE + SSI_O_CR1) |= BIT1HI;

  //17. Enable the NVIC interrupt for the SSI when starting to transmit
  //It is interrupt number 7 so appears in EN2 at bit 30
  HWREG(NVIC_EN0) |= BIT7HI;
}

/*------------------------------------------------------------------------*/
/*----------------------------- Test Harness -----------------------------*/
/*------------------------------------------------------------------------*/

//#define SSI_TEST
#ifdef SSI_TEST

#include <stdio.h>
#include "ES_Configure.h"
#include "ES_Framework.h"
#include "ES_Port.h"
#include "termio.h"
#include "EnablePA25_PB23_PD7_PF0.h"

#define clrScrn() printf("\x1b[2J")
#define goHome() printf("\x1b[1,1H")
#define clrLine() printf("\x1b[K")

void SSIResponse(void)
{
  //Read from data register
  //uint8_t message = HWREG(SSI0_BASE+SSI_O_DR);

  //Print message
  //printf("\n\rMessage is %d",message);

  //write data to register
  //HWREG(SSI0_BASE+SSI_O_DR) = 0xAA;

  //Read from data register
  //message = HWREG(SSI_O_DR);

  //Print message
  //printf("\n\rMessage is %d",message);

  HWREG(SSI0_BASE + SSI_O_IM) &= ~SSI_IM_TXIM; //TXIM bit = 0 (masked)
}

int main(void)
{
  // Set the clock to run at 40MhZ using the PLL and 16MHz external crystal
  SysCtlClockSet(SYSCTL_SYSDIV_5 | SYSCTL_USE_PLL | SYSCTL_OSC_MAIN
      | SYSCTL_XTAL_16MHZ);
  TERMIO_Init();
  clrScrn();

  // When doing testing, it is useful to announce just which program
  // is running.
  puts("\rStarting Test Harness for \r");
  printf( "Morse Decoding Lab\r\n");
  printf( "%s %s\n", __TIME__, __DATE__);
  printf( "\n\r\n");
  printf( "Press any key to post key-stroke events to Service 0\n\r");
  printf( "Press 'd' to test event deferral \n\r");
  printf( "Press 'r' to test event recall \n\r");

  //Initialize SSI System
  InitSSI();

  //Write 0XAA to the SSI system twice
  HWREG(SSI0_BASE + SSI_O_DR) = 0xAA;
  HWREG(SSI0_BASE + SSI_O_DR) = 0xAA;
  //HWREG(SSI_O_DR) = 0xAA;
  //HWREG(SSI_O_DR) = 0xAA;

  while (1)
  {
    //Read from data register
    uint8_t message = HWREG(SSI0_BASE + SSI_O_DR);

    //Print message
    printf("\n\rMessage is %d", message);

    //write data to register
    HWREG(SSI0_BASE + SSI_O_DR) = 0xAA;
  }

  return 0;
}

#endif

/*----------------------------- Notes -----------------------------
1. SSI Raw interrupt status (SSIRIS) register
    - If TXRIS == 1 and EOT bit is set, FIFO is empty
2. SSI Masked interrupt status (SSIMIS). READ ONLY.
    - 1 if interrupt is fired
    - Either need to mask the interrupt or put something in
    the FIFO to clear bit
*/
//Write data to the SSI Data register (SSIDR)
//Read data from the SSIDR register

//Other
//now choose to map SSI functions to those pins, this is a mux value of 2 that we
//want to use for specifying the function on bits 2-5
//HWREG(GPIO_PORTA_BASE+GPIO_O_PCTL) =
//(HWREG(GPIO_PORTA_BASE+GPIO_O_PCTL) &0x00ffffff) + (2<<(2*BitsPerNibble)) +
//(2<<(3*BitsPerNibble)) + (2<<(4*BitsPerNibble)) + (2<<(5*BitsPerNibble));
