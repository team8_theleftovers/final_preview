#include "Move.h"
#include "PWM.h"

// the common headers for C99 types
#include <stdint.h>
#include <stdbool.h>

//Hardware headers
#include "inc/hw_memmap.h"
#include "inc/hw_gpio.h"
#include "inc/hw_pwm.h"
#include "inc/hw_sysctl.h"
#include "inc/hw_types.h"
#include "bitdefs.h"
#include "inc/tm4c123gh6pm.h"

//Header for Port functions
#include "Ports.h"

//Header for parameter and pin defines
#include "Parameters.h"

//For debugging
#include "termio.h"

#define FORWARD_DIRECTION true //For interfacing with Set__WheelDirection(bool forward) functions
#define BACKWARD_DIRECTION false

/****************************************************************************
Function:       SetServo
Parameters:     Actuator (see enum in PWM.h) and DesiredAngle (0-90 or 0-180 depending on servo)
Returns:        None
Description:    Function for moving the servos
                NOTE: Assumes we have a 50Hz (20ms period) PWM signal to the servo.
                --> 1ms (5% DC) pulses command the lowest angle
                --> 2ms (10% DC) pulses command the highest angle
                --> 1.5 (7.5% DC) ms pulses command the neutral angle (90 deg for 180 deg servo)
*****************************************************************************/
void SetServo(Actuator_t Actuator, uint8_t DesiredAngle)
{
  //Our Servo frequency is assumed to be 50 Hz.

  //NEED TO CHECK THE MATH ON THIS FUNCTION! IE Figure out integer division. May
  //need to modify SetDC function to accomodate higher resolution PWM DC.

    #define SERVO_1_MAX_ANGLE 90
    #define SERVO_2_MAX_ANGLE 90
    #define SERVO_3_MAX_ANGLE 90
    #define MAX_SERVO_DC 5
    #define MIN_SERVO_DC 5
    #define DEL_SERVO_DC 5 //del = MaxDC - MinDC. Servo DC goes from 5-10% at 50 Hz

  //(DesiredAngle/SERVO_1_MAX_ANGLE)*(MAX_SERVO_DC-MIN_SERVO_DC);

  //SetDC expects a value of 0-1000 from the servos (to enable higher precision)
  //So we must multiply the our equations by 10.
  switch (Actuator)
  {
    case SERVO_1:
    {
      //Interpolate the Dutcy Cycle from the angle
      uint8_t DutyCycle = MIN_SERVO_DC * 10 +
          (10 * DesiredAngle * DEL_SERVO_DC) / (SERVO_1_MAX_ANGLE);
      SetDC(SERVO_1, DutyCycle);
    }
    break;

    case SERVO_2:
    {
      //Interpolate the Dutcy Cycle from the angle
      uint8_t DutyCycle = MIN_SERVO_DC * 10 +
          (10 * DesiredAngle * DEL_SERVO_DC) / (SERVO_2_MAX_ANGLE);
      SetDC(SERVO_2, DutyCycle);
    }
    break;

    case SERVO_3:
    {
      //Interpolate the Dutcy Cycle from the angle
      uint8_t DutyCycle = MIN_SERVO_DC * 10 +
          (10 * DesiredAngle * DEL_SERVO_DC) / (SERVO_2_MAX_ANGLE);
      SetDC(SERVO_3, DutyCycle);
    }
    break;
  }
}

/****************************************************************************
Function:       SetWheelPWMPolarity
Parameters:     Actuator (see enum in PWM.h) and direction (bool)
Returns:        None
Description:    Set Wheel PWM polarity to normal if direction is true, inverted if false
*****************************************************************************/
void SetWheelPWMPolarity(Actuator_t Actuator, bool direction)
{
  //Both wheels are on the same PWM block (Left wheel is GenA, Right is GenB on PWM1)
  // disable the PWM while initializing
  HWREG(PWM1_BASE + PWM_O_1_CTL) = 0;

  //Set polarity of left or right wheel's PWM signal
  //Wheel PWM's come from Module 1, PWM2 (Left wheel) and PWM3 (right wheel)
  if (Actuator == WHEEL_L)
  {
    if (direction == false)
    {
      //Since the left wheel is on PWM2, we set bit 2 low for a non-inverted signal
      HWREG(PWM1_BASE + PWM_O_INVERT) &= BIT2LO;
    }
    else
    {
      //Since the left wheel is on PWM2, we set bit 2 high for an inverted signal
      HWREG(PWM1_BASE + PWM_O_INVERT) |= BIT2HI;
    }
  }
  else if (Actuator == WHEEL_R)
  {
    if (direction == false)
    {
      //Since the right wheel is on PWM3, we set bit 3 low for a non-inverted signal
      HWREG(PWM1_BASE + PWM_O_INVERT) &= BIT3LO;
    }
    else
    {
      //Since the right wheel is on PWM3, we set bit 3 high for an inverted signal
      HWREG(PWM1_BASE + PWM_O_INVERT) |= BIT3HI;
    }
  }
  // set the up/down count mode, enable the PWM generator and make
  // both generator updates locally synchronized to zero count
  HWREG(PWM1_BASE + PWM_O_1_CTL) =
      (PWM_1_CTL_MODE | PWM_1_CTL_ENABLE | PWM_1_CTL_GENAUPD_LS | PWM_1_CTL_GENBUPD_LS);
}

/****************************************************************************
Function:       SetLeftWheelDirection
Parameters:     forward (bool)
Returns:        None
Description:    Set Left wheel to go forward if input is true, backwards if false
*****************************************************************************/
void SetLeftWheelDirection(bool forward)
{
  if (forward)
  {
    //Set Motor direction pin low
    PORTC |= WHEEL_L_GPIO;
    //Set polarity of PWM
    SetWheelPWMPolarity(WHEEL_L, forward);
  }
  else
  {
    //set motor directions pin high
    PORTC &= ~WHEEL_L_GPIO;
    //Set polarity of PWM
    SetWheelPWMPolarity(WHEEL_L, forward);
  }
}

/****************************************************************************
Function:       SetRightWheelDirection
Parameters:     forward (bool)
Returns:        None
Description:    Set Right wheel to go forward if input is true, backwards if false
*****************************************************************************/
void SetRightWheelDirection(bool forward)
{
  if (forward)
  {
    //Set Motor direction pin low
    PORTC |= WHEEL_R_GPIO;
    //Set polarity of PWM
    SetWheelPWMPolarity(WHEEL_R, forward);
  }
  else
  {
    //set motor directions pin high
    PORTC &= ~WHEEL_R_GPIO;
    //Set polarity of PWM
    SetWheelPWMPolarity(WHEEL_R, forward);
  }
}

/****************************************************************************
Function:       Move
Parameters:     MoveType_t (enum on Move.h), DutyCycle (0-100)
Returns:        None
Description:    Function for moving the wheels. Specify the move type and the
                duty cycle to move the wheels in tandem or individually.
*****************************************************************************/
void Move(MoveType_t DesiredMove, uint32_t DutyCycle)
{
  switch (DesiredMove)
  {
    case FORWARD:
    {
      //Set wheel direction
      SetLeftWheelDirection(FORWARD_DIRECTION);
      SetRightWheelDirection(FORWARD_DIRECTION);
      //Set Duty Cycles
      SetDC(WHEEL_L,  DutyCycle);
      SetDC(WHEEL_R,  DutyCycle);
    }
    break;

    case BACKWARD:
    {
      //Set wheel direction
      SetLeftWheelDirection(BACKWARD_DIRECTION);
      SetRightWheelDirection(BACKWARD_DIRECTION);
      //Set Duty Cycles
      SetDC(WHEEL_L,  DutyCycle);
      SetDC(WHEEL_R,  DutyCycle);
    }
    break;

    case COUNTER_CLOCKWISE:
    {
      //Set wheel direction
      SetLeftWheelDirection(BACKWARD_DIRECTION);
      SetRightWheelDirection(FORWARD_DIRECTION);

      //Set Duty Cycles
      SetDC(WHEEL_L,  DutyCycle);
      SetDC(WHEEL_R,  DutyCycle);
    }
    break;

    case CLOCKWISE:
    {
      //Set wheel direction
      SetLeftWheelDirection(FORWARD_DIRECTION);
      SetRightWheelDirection(BACKWARD_DIRECTION);
      //Set Duty Cycles
      SetDC(WHEEL_L,  DutyCycle);
      SetDC(WHEEL_R,  DutyCycle);
    }
    break;

    case L_WHEEL_FORWARD:
    {
      //Set wheel direction
      SetLeftWheelDirection(FORWARD_DIRECTION);
      //Set Duty Cycle
      SetDC(WHEEL_L, DutyCycle);
    }
    break;

    case L_WHEEL_BACKWARD:
    {
      //Set wheel direction
      SetLeftWheelDirection(BACKWARD_DIRECTION);
      //Set Duty Cycle
      SetDC(WHEEL_L, DutyCycle);
    }
    break;

    case R_WHEEL_FORWARD:
    {
      //Set wheel direction
      SetRightWheelDirection(FORWARD_DIRECTION);
      //Set Duty Cycle
      SetDC(WHEEL_R, DutyCycle);
    }
    break;

    case R_WHEEL_BACKWARD:
    {
      //Set wheel direction
      SetRightWheelDirection(BACKWARD_DIRECTION);
      //Set Duty Cycle
      SetDC(WHEEL_R, DutyCycle);
    }
    break;

    case STOP:
    {
      //Set wheel direction to FORWARD
      SetLeftWheelDirection(FORWARD_DIRECTION);
      SetRightWheelDirection(FORWARD_DIRECTION);
      //Set Duty Cycles to zero
      SetDC(WHEEL_L,  0);
      SetDC(WHEEL_R,  0);
    }
    break;
  }
}

//***********************************************************************************
//-----------MODULE TEST HARNESS-----------------------------------------------------
//***********************************************************************************
//#define MOVE_TEST
#ifdef MOVE_TEST

#include "termio.h"
#include <stdio.h>
#include "ES_Configure.h"
#include "ES_Framework.h"
#include "ES_Port.h"
#include "termio.h"
#include "EnablePA25_PB23_PD7_PF0.h"

#define clrScrn() printf("\x1b[2J")
#define goHome() printf("\x1b[1,1H")
#define clrLine() printf("\x1b[K")

#include "HardwareInit.h"

//Test the Timer functions to confirm that they are producing the desired output
int main(void)
{
  // Set the clock to run at 40MhZ using the PLL and 16MHz external crystal
  SysCtlClockSet(SYSCTL_SYSDIV_5 | SYSCTL_USE_PLL | SYSCTL_OSC_MAIN
      | SYSCTL_XTAL_16MHZ);
  TERMIO_Init();
  clrScrn();

  // When doing testing, it is useful to announce just which program
  // is running.
  puts("\rMove Test Harness=\r");
  printf( "Move.c\r\n");
  printf( "%s %s\n", __TIME__, __DATE__);
  printf( "\n\r\n");
  printf( "Press any key to post key-stroke events to Service 0\n\r");
  printf( "Press 'd' to test event deferral \n\r");
  printf( "Press 'r' to test event recall \n\r");

  //***************************************
  InitHW();

  printf("\n\n\rTesting Move.c functions now");

  if (0)
  {
    getchar();
    Move(R_WHEEL_FORWARD, 50);
    getchar();

    Move(R_WHEEL_BACKWARD, 50);
    getchar();

    Move(STOP, 0);
    getchar();

    Move(FORWARD, 30);
    getchar();

    Move(BACKWARD, 30);
    getchar();

    Move(CLOCKWISE, 30);
    getchar();

    Move(COUNTER_CLOCKWISE, 30);
    getchar();

    Move(STOP, 0);
    getchar();

    Move(L_WHEEL_FORWARD, 50);
    getchar();

    Move(L_WHEEL_BACKWARD, 50);
    getchar();

    Move(STOP, 30);
    getchar();
  }

  //Test Servos
  printf("\n\rSetting Servos");
  SetServo( SERVO_1,  20);
  SetServo( SERVO_2,  0);
  SetServo( SERVO_3,  89); //Can't do 90?
  getchar();
  printf("\n\rGarbage servo 40 deg");
  SetServo(GARBAGE_SERVO, 40);
  getchar();
  printf("\n\rGarbage servo 0 deg");
  SetServo(GARBAGE_SERVO, 0);
  getchar();
  printf("\n\rGarbage servo 10 deg");
  SetServo(GARBAGE_SERVO, 10);
  getchar();
  printf("\n\rGarbage servo 20 deg");
  SetServo(GARBAGE_SERVO, 20);
  getchar();
  printf("\n\rGarbage servo 30 deg");
  SetServo(GARBAGE_SERVO, 30);
  getchar();
  printf("\n\rGarbage servo 40 deg");
  SetServo(GARBAGE_SERVO, 40);
  getchar();
  printf("\n\rGarbage servo 50 deg");
  SetServo(GARBAGE_SERVO, 50);
  getchar();
  printf("\n\rGarbage servo 60 deg");
  SetServo(GARBAGE_SERVO, 60);
  getchar();
  printf("\n\rGarbage servo 70 deg");
  SetServo(GARBAGE_SERVO, 70);
  getchar();
  printf("\n\rGarbage servo 80 deg");
  SetServo(GARBAGE_SERVO, 80);

  //***************************************
  //Enter the eternal loop
  for ( ; ;)
  {
    ;
  }
  return 0;
}

#endif
