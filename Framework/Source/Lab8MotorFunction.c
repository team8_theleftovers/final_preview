/****************************************************************************
 Module
   Lab8MotorFunction.c

 Revision
   1.0.1

 Description
   This is a Template file for implementing flat state machines under the
   Gen2 Events and Services Framework.

 Notes

 History
 When           Who     What/Why
 -------------- ---     --------
 01/15/12 11:12 jec      revisions for Gen2 framework
 11/07/11 11:26 jec      made the queue static
 10/30/11 17:59 jec      fixed references to CurrentEvent in RunLab8MotorFunction()
 10/23/11 18:20 jec      began conversion from SMLab8MotorFunction.c (02/20/07 rev)
****************************************************************************/
/*----------------------------- Include Files -----------------------------*/
/* include header files for this state machine as well as any machines at the
   next lower level in the hierarchy that are sub-machines to this machine
*/
#include "ES_Configure.h"
#include "ES_Framework.h"
#include "Lab8MotorFunction.h"
//#include "MotorSpeedMeasure.h"
#include <math.h>

#include "ADMulti.h"

#include "ES_Port.h"
#include "termio.h"
//#include "PWM16Tiva.h"
#include "driverlib/sysctl.h"

/* include header files for hardware access
*/
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_gpio.h"
#include "inc/hw_sysctl.h"
#include "inc/hw_pwm.h"
#include "inc/hw_timer.h"
#include "inc/hw_nvic.h"

// the headers to access the TivaWare Library
#include "driverlib/sysctl.h"
#include "driverlib/pin_map.h"
#include "driverlib/gpio.h"
#include "driverlib/timer.h"
#include "driverlib/interrupt.h"
#include "driverlib/pwm.h"

/*----------------------------- Module Defines ----------------------------*/

/*---------------------------- Module Functions ---------------------------*/
/* prototypes for private functions for this machine.They should be functions
   relevant to the behavior of this state machine
*/

/*---------------------------- Module Variables ---------------------------*/
// everybody needs a state variable, you may need others as well.
// type of state variable should match htat of enum in header file
static Lab8MotorFunctionState_t CurrentState;

// with the introduction of Gen2, we need a module level Priority var as well
static uint8_t  MyPriority;
static uint16_t SamplingTime = 100;
//static int      MotorPWM;
static uint32_t CurrentPotVal;
int             PeriodInMS = 5;
int             PeriodInMS31 = 4;
int             PeriodInMS32 = 2;
int             PeriodInMS33 = 1;
int             PWMTicksPerMS = 1250;
int             PWMTicksPerMS34 = 625;
int             PWMTicksPerMS35 = 500;
uint32_t        GenA_Normal;
uint32_t        GenB_Normal;
int             BitsPerNibble = 4;
//static int      period4light;
//static int      PulsesPerRev = 512;

static uint32_t SwitchDirex = 3000;
uint32_t        SysClockTicksPerMS = 40000;

static bool     fwd = true;

//static bool     stopped = false;

//shows up as 1 - (this value) on oscope because the second input is high
static int  DutyCycleA = 50;
static int  DutyCycleB = 50;

/*------------------------------ Module Code ------------------------------*/
/****************************************************************************
 Function
     InitLab8MotorFunction

 Parameters
     uint8_t : the priorty of this service

 Returns
     bool, false if error in initialization, true otherwise

 Description
     Saves away the priority, sets up the initial transition and does any
     other required initialization for this state machine
 Notes

 Author
     J. Edward Carryer, 10/23/11, 18:55
****************************************************************************/
bool InitLab8MotorFunction(uint8_t Priority)
{
  ES_Event_t ThisEvent;

  //initialize all pins for driving motors, plus pin for analog
  //reading frompot, and pin for switching direction
  /*
  HWREG(SYSCTL_RCGCGPIO) |= (BIT5HI);
  while ((HWREG(SYSCTL_PRGPIO) & SYSCTL_PRGPIO_R5) != SYSCTL_PRGPIO_R5)
  {}
  HWREG(GPIO_PORTF_BASE + GPIO_O_DEN) |= (BIT0HI | BIT3HI | BIT4HI);
  HWREG(GPIO_PORTF_BASE + GPIO_O_DIR) |= (BIT0HI | BIT3HI | BIT4HI);


  //initialize port E
  HWREG(SYSCTL_RCGCGPIO) |= (BIT4HI);
  while ((HWREG(SYSCTL_PRGPIO) & SYSCTL_PRGPIO_R4) != SYSCTL_PRGPIO_R4)
  {}
  ADC_MultiInit(1);
  //HWREG(GPIO_PORTE_BASE + GPIO_O_DEN) |= (BIT0HI | BIT1HI | BIT5HI);
  //HWREG(GPIO_PORTE_BASE + GPIO_O_DIR) |= (BIT0HI | BIT1HI | BIT5HI);

  //HWREG(GPIO_PORTF_BASE + (GPIO_O_DATA + ALL_BITS)) |= BIT4HI;
  //HWREG(GPIO_PORTE_BASE + (GPIO_O_DATA + ALL_BITS)) |= BIT5HI;
  //HWREG(GPIO_PORTE_BASE + (GPIO_O_DATA + ALL_BITS)) &= (BIT0LO & BIT1LO);
  */

  //Initialize PWM pin - B6 and B7

  //start by enabling the clock to the PWM module
  HWREG(SYSCTL_RCGCPWM) |= SYSCTL_RCGCPWM_R0;
  //ENABLE THE CLOCK TO PORT B
  HWREG(SYSCTL_RCGCGPIO) |= SYSCTL_RCGCGPIO_R1;

  //for everything but part 3.5
  //SELECT THE PWM CLOCK AS SYSTEM CLOCK/32
  HWREG(SYSCTL_RCC) = (HWREG(SYSCTL_RCC) & ~SYSCTL_RCC_PWMDIV_M) | (SYSCTL_RCC_USEPWMDIV | SYSCTL_RCC_PWMDIV_32);

  //for part 3.5 and 3.4??
  //SELECT THE PWM CLOCK AS SYSTEM CLOCK/32
  //HWREG(SYSCTL_RCC) = (HWREG(SYSCTL_RCC) & ~SYSCTL_RCC_PWMDIV_M) | (SYSCTL_RCC_USEPWMDIV | SYSCTL_RCC_PWMDIV_16);

  //MAKE SURE THE PWM MODULE CLOCK HAS GOTTEN GOING
  while ((HWREG(SYSCTL_PRPWM) & SYSCTL_PRPWM_R0) != SYSCTL_PRPWM_R0)
  {
    ;
  }
  //DISABLE THE PWM WHILE INITIALIZING
  HWREG(PWM0_BASE + PWM_O_0_CTL) = 0;

  //PROGRAM GENERATORS TO GO TO 1 AT RISING COMPARE A/B, 0 ON FALLING COMPARE A/B
  GenA_Normal = (PWM_0_GENA_ACTCMPAU_ONE | PWM_0_GENA_ACTCMPAD_ZERO);
  HWREG(PWM0_BASE + PWM_O_0_GENA) = GenA_Normal;
  GenB_Normal = (PWM_0_GENB_ACTCMPBU_ONE | PWM_0_GENB_ACTCMPBD_ZERO);
  HWREG(PWM0_BASE + PWM_O_0_GENB) = GenB_Normal;

  //Set the PWM period. Since we are counting both up and down, we initialize the load register to 1/2 the desired
  //total period. We well also program the match registers to 1/2 the desired high time
  HWREG(PWM0_BASE + PWM_O_0_LOAD) = ((PeriodInMS * PWMTicksPerMS)) >> 1;

  // Set the initial Duty cycle on A to 50% by programming the compare value
  // to 1/2 the period to count up (or down). Technically, the value to program
  // should be Period/2 - DesiredHighTime/2, but since the desired high time is 1/2
  // the period, we can skip the subtract
  HWREG(PWM0_BASE + PWM_O_0_CMPA) = (HWREG(PWM0_BASE + PWM_O_0_LOAD)) - (((PeriodInMS * PWMTicksPerMS)) >> 3);

  //sET THE INTIIAL dUTY CYCLE ON b TO 25% by programming the compare value to period/2-period/8 (75% of the period)
  HWREG(PWM0_BASE + PWM_O_0_CMPB) = (HWREG(PWM0_BASE + PWM_O_0_LOAD)) - (((PeriodInMS * PWMTicksPerMS)) >> 3);

  //enable the pwm outputs
  HWREG(PWM0_BASE + PWM_O_ENABLE) |= (PWM_ENABLE_PWM1EN | PWM_ENABLE_PWM0EN);

  //now configure the portB  pins to PWM outputs
  //startby selecting the alternate function for 7
  HWREG(GPIO_PORTB_BASE + GPIO_O_AFSEL) |= (BIT7HI | BIT6HI);

  //chhose to map PWM to those pins, this is a mux value of 4 that we want to use for specifying the function
  //on bit7
  HWREG(GPIO_PORTB_BASE + GPIO_O_PCTL) = (HWREG(GPIO_PORTB_BASE + GPIO_O_PCTL) & 0X00ffffff) + (4 << (7 * BitsPerNibble)) + (4 << (6 * BitsPerNibble));

  //enable pin 6 and 7 on port b for digital I/O
  HWREG(GPIO_PORTB_BASE + GPIO_O_DEN) |= (BIT3HI | BIT4HI | BIT5HI | BIT6HI | BIT7HI);

  //make pin 7 on port B into outputs
  HWREG(GPIO_PORTB_BASE + GPIO_O_DIR) |= (BIT4HI | BIT5HI | BIT6HI | BIT7HI);

  //make PB3 into line sensor input
  HWREG(GPIO_PORTB_BASE + GPIO_O_DIR) &= (BIT3LO);

  //SET THE UP/DOWN COUNT MODE, ENABLE THE PWM GENERATOR AND MAKE BOTH
  //GENERATOR UPDATES LOCALLY SYNCHRONIZED TO AZERO COUNT
  HWREG(PWM0_BASE + PWM_O_0_CTL) = (PWM_0_CTL_MODE | PWM_0_CTL_ENABLE | PWM_0_CTL_GENAUPD_LS | PWM_0_CTL_GENBUPD_LS);

  HWREG(GPIO_PORTB_BASE + (GPIO_O_DATA + ALL_BITS)) |= (BIT4HI | BIT5HI);

  Set0DC();

  MyPriority = Priority;
  // put us into the Initial PseudoState
  CurrentState = Lab8MotorFunctionInitPState;
  // post the initial transition event
  ThisEvent.EventType = ES_INIT;
  if (ES_PostToService(MyPriority, ThisEvent) == true)
  {
    return true;
  }
  else
  {
    return false;
  }
}

/****************************************************************************
 Function
     PostLab8MotorFunction

 Parameters
     EF_Event ThisEvent , the event to post to the queue

 Returns
     boolean False if the Enqueue operation failed, True otherwise

 Description
     Posts an event to this state machine's queue
 Notes

 Author
     J. Edward Carryer, 10/23/11, 19:25
****************************************************************************/
bool PostLab8MotorFunction(ES_Event_t ThisEvent)
{
  return ES_PostToService(MyPriority, ThisEvent);
}

/****************************************************************************
 Function
    RunLab8MotorFunction

 Parameters
   ES_Event : the event to process

 Returns
   ES_Event, ES_NO_EVENT if no error ES_ERROR otherwise

 Description
   add your description here
 Notes
   uses nested switch/case to implement the machine.
 Author
   J. Edward Carryer, 01/15/12, 15:23
****************************************************************************/
ES_Event_t RunLab8MotorFunction(ES_Event_t ThisEvent)
{
  ES_Event_t ReturnEvent;
  ReturnEvent.EventType = ES_NO_EVENT;

  switch (CurrentState)
  {
    //initialization code - set current state to driving, start sampling timer
    case Lab8MotorFunctionInitPState:
    {
      if (ThisEvent.EventType == ES_INIT)
      {
        CurrentState = Driving;
        ES_Timer_InitTimer(DC_SPEED_TIMER, SamplingTime);
        //ES_Timer_InitTimer(DIREX_TIMER, SwitchDirex);
      }
    }
    break;

    //driving code - periodically checks desired RPM value
    case Driving:
    {
      switch (ThisEvent.EventType)
      {
        case ES_TIMEOUT:
        {
          if (ThisEvent.EventParam == DC_SPEED_TIMER)
          {
            HWREG(GPIO_PORTF_BASE + (GPIO_O_DATA + ALL_BITS)) |= (BIT4HI);
            //after every 100 ms timer expires,
            //print desired RPM, RPM, and dutycycle, send
            //state to check speed again, and start another
            //100 ms timer
            CurrentState = CheckingSpeed;
            ES_Event_t NewEvent;
            NewEvent.EventType = SpeedCheck;
            PostLab8MotorFunction(NewEvent);

            //IF IN REVERSE, DUTY CYCLE VALUE IS 100 - WHATEVER IS PRINTED
            printf( "\r\nDutyCycleA: %d\n\r", 100 - DutyCycleA);
            printf( "\r\nDutyCycleB: %d\n\r", DutyCycleB);

            //set the speed
            HWREG(PWM0_BASE + PWM_O_0_CMPB) = ((PeriodInMS * (PWMTicksPerMS) * DutyCycleB) / 200);
            HWREG(PWM0_BASE + PWM_O_0_CMPA) = ((PeriodInMS * (PWMTicksPerMS) * DutyCycleA) / 200);

            ES_Timer_InitTimer(DC_SPEED_TIMER, SamplingTime);
            HWREG(GPIO_PORTF_BASE + (GPIO_O_DATA + ALL_BITS)) &= (BIT4LO);
          }
          if (ThisEvent.EventParam == DIREX_TIMER)
          {
            if (fwd)
            {
              SetA(false, 35);
              SetB(false, 35);
              fwd = false;
            }
            else
            {
              SetA(true, 60);
              SetB(true, 60);
              fwd = true;
            }
            //ES_Timer_InitTimer(DIREX_TIMER, SwitchDirex);
            ES_Timer_InitTimer(DC_SPEED_TIMER, SamplingTime);

            HWREG(PWM0_BASE + PWM_O_0_CMPB) = ((PeriodInMS * (PWMTicksPerMS) * DutyCycleB) / 200);
            HWREG(PWM0_BASE + PWM_O_0_CMPA) = ((PeriodInMS * (PWMTicksPerMS) * DutyCycleA) / 200);
          }
        }
        break;
      }
    }
    break;

    //case where motor isn't rotating
    /*
    case MotorStopped:
    {
      switch (ThisEvent.EventType)
      {
        case ES_TIMEOUT:
        {
          //keep checking speed while stopped
          if (ThisEvent.EventParam == DC_SPEED_TIMER)
          {
            printf( "\r\nDesiredRPM: %d\n\r", DesiredRPM);
            printf( "\r\nDutyCycle: %d\n\r",  DutyCycle);
            printf( "\r\nRPM: 0\n\r");
            ES_Event_t NewEvent;
            NewEvent.EventType = SpeedCheck;
            CurrentState = CheckingSpeed;
            PostLab8MotorFunction(NewEvent);
          }
        }
        break;
      }
    }
    break;
    */

    //no matter if motor is stopped or rotating, state is switched
    //to checkingspeed every 100 ms
    case CheckingSpeed:
    {
      switch (ThisEvent.EventType)
      {
        //speed check event - desiredRPM is read/converted from analog voltage
        //on potentiometer
        case SpeedCheck:
        {
          CurrentState = Driving;
          //always initiate 100 ms timer
          ES_Timer_InitTimer(DC_SPEED_TIMER, SamplingTime);
        }
        break;
      }
    }
    break;
  }
  return ReturnEvent;
}

/****************************************************************************
 Function
     QueryLab8MotorFunction

 Parameters
     None

 Returns
     Lab8MotorFunctionState_t The current state of the Lab8MotorFunction state machine

 Description
     returns the current state of the Lab8MotorFunction state machine
 Notes

 Author
     J. Edward Carryer, 10/23/11, 19:21
****************************************************************************/
Lab8MotorFunctionState_t QueryLab8MotorFunction(void)
{
  return CurrentState;
}

/***************************************************************************
 private functions
 ***************************************************************************/
bool SpeedEventChecker(void)
{
  bool        ReturnVal = false;
  static int  LastPotVal = 0;
  //analog read state of potentiometer
  //uint32_t    AnalogVal[1];
  //ADC_MultiRead(AnalogVal);
  //set module level variable CurrentPotVal, which will be used to dtermine desired speed
  //CurrentPotVal = AnalogVal[0];
  CurrentPotVal = 10000;
  if (CurrentPotVal != LastPotVal)
  {
    LastPotVal = CurrentPotVal;
    ReturnVal = true;
  }

  return ReturnVal;
}

void Set100DC(void)
{
  HWREG(PWM0_BASE + PWM_O_0_GENB) = PWM_0_GENB_ACTZERO_ZERO;
  HWREG(PWM0_BASE + PWM_O_0_GENA) = PWM_0_GENA_ACTZERO_ONE;
}

void Set0DC(void)
{
  HWREG(PWM0_BASE + PWM_O_0_GENB) = PWM_0_GENB_ACTZERO_ONE;
  HWREG(PWM0_BASE + PWM_O_0_GENA) = PWM_0_GENA_ACTZERO_ZERO;
  DutyCycleA = 100;
  DutyCycleB = 0;
}

void RestoreDC(void)
{
  HWREG(PWM0_BASE + PWM_O_0_GENA) = GenA_Normal;
  HWREG(PWM0_BASE + PWM_O_0_GENB) = GenB_Normal;
}

void SetA(bool fwdA, int DutyCycleA2)
{
  if (fwdA)
  {
    HWREG(GPIO_PORTB_BASE + (GPIO_O_DATA + ALL_BITS)) |= (BIT5HI);
    DutyCycleA = 100 - DutyCycleA2;
  }
  else
  {
    HWREG(GPIO_PORTB_BASE + (GPIO_O_DATA + ALL_BITS)) &= (BIT5LO);
    DutyCycleA = DutyCycleA2;
  }
}

void SetB(bool fwdB, int DutyCycleB2)
{
  if (fwdB)
  {
    HWREG(GPIO_PORTB_BASE + (GPIO_O_DATA + ALL_BITS)) &= (BIT4LO);
    DutyCycleB = DutyCycleB2;
  }
  else
  {
    HWREG(GPIO_PORTB_BASE + (GPIO_O_DATA + ALL_BITS)) |= (BIT4HI);
    DutyCycleB = 100 - DutyCycleB2;
  }
}