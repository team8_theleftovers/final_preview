/***************************************************************************
 Module:        PWM_utils.c
 Revision:      1.0.0
 Description:   Collection of functions to manipulate PWM hardware
 Notes:         Currently outputs PWM on pins PB6 and PB7
****************************************************************************/

//Module header file
#include "PWM.h"
#include "termio.h"

// the common headers for C99 types
#include <stdint.h>
#include <stdbool.h>

//Hardware headers
#include "inc/hw_memmap.h"
#include "inc/hw_gpio.h"
#include "inc/hw_pwm.h"
#include "inc/hw_sysctl.h"
#include "inc/hw_types.h"
#include "bitdefs.h"
#include "inc/tm4c123gh6pm.h"

//Header for Port functions
#include "Ports.h"

//Header for parameter and pin defines
#include "Parameters.h"

//For debugging
#include "termio.h"

//----------Module Defines-----------------------------------------------
// 40,000 ticks per mS assumes a 40Mhz clock, we will use SysClk/32 for PWM
#define PWMTicksPerMS 40000 / 32
#define PWMUSPerTick 0.8 //Based on a 40MHz clock rate divided by 32

//For selecting alternate port functions
#define BitsPerNibble 4

//Dummy period for Recycling Beacon PWM initialization
#define DUMMY_PERIOD 100

//-------------Module variables---------------------------------

//All arrays used in the SetDC function. Used with the enumerated
//type Actuator_t we can have a single SetDC function instead of 8

//Generator addresses
static uint32_t GEN_ADDRESS[] = { PWM0_BASE + PWM_O_1_GENA,   //Servo1
                                  PWM0_BASE + PWM_O_1_GENB,   //Servo2
                                  PWM0_BASE + PWM_O_0_GENA,   //Servo3
                                  PWM0_BASE + PWM_O_0_GENB,   //Servo4
                                  PWM1_BASE + PWM_O_1_GENA,   //WheelL
                                  PWM1_BASE + PWM_O_1_GENB,   //WheelR
                                  PWM1_BASE + PWM_O_2_GENB,   //Conveyor
                                  PWM1_BASE + PWM_O_3_GENA }; //Recycling Beacon

//Compare address
static uint32_t CMP_ADDRESS[] = { PWM0_BASE + PWM_O_1_CMPA,   //Servo1
                                  PWM0_BASE + PWM_O_1_CMPB,   //Servo2
                                  PWM0_BASE + PWM_O_0_CMPA,   //Servo3
                                  PWM0_BASE + PWM_O_0_CMPB,   //Servo4
                                  PWM1_BASE + PWM_O_1_CMPA,   //WheelL
                                  PWM1_BASE + PWM_O_1_CMPB,   //WheelR
                                  PWM1_BASE + PWM_O_2_CMPB,   //Conveyor
                                  PWM1_BASE + PWM_O_3_CMPA }; //Recycling Beacon

//Current period of PWM
static uint32_t LOAD_ADDRESS[] = { PWM0_BASE + PWM_O_1_LOAD, //Servos 12
                                   PWM0_BASE + PWM_O_1_LOAD,
                                   PWM0_BASE + PWM_O_0_LOAD, //Servos 34
                                   PWM0_BASE + PWM_O_0_LOAD,
                                   PWM1_BASE + PWM_O_1_LOAD, //Wheels
                                   PWM1_BASE + PWM_O_1_LOAD,
                                   PWM1_BASE + PWM_O_2_LOAD,    //Conveyor
                                   PWM1_BASE + PWM_O_3_LOAD };  //Recycling Beacon

//Used to set 0% DC
static uint32_t ACTZERO_ZERO[] = { PWM_1_GENA_ACTZERO_ZERO, //Servos 12
                                   PWM_1_GENB_ACTZERO_ZERO,
                                   PWM_0_GENA_ACTZERO_ZERO, //Servos 34
                                   PWM_0_GENB_ACTZERO_ZERO,
                                   PWM_1_GENA_ACTZERO_ZERO, //Wheels
                                   PWM_1_GENB_ACTZERO_ZERO,
                                   PWM_2_GENB_ACTZERO_ZERO,   //Conveyor
                                   PWM_3_GENA_ACTZERO_ZERO }; //Recycling Beacon

//Used to set 100% DC
static uint32_t ACTZERO_ONE[] = { PWM_1_GENA_ACTZERO_ONE, //Servos 12
                                  PWM_1_GENB_ACTZERO_ONE,
                                  PWM_0_GENA_ACTZERO_ONE, //Servos 34
                                  PWM_0_GENB_ACTZERO_ONE,
                                  PWM_1_GENA_ACTZERO_ONE, //Wheels
                                  PWM_1_GENB_ACTZERO_ONE,
                                  PWM_2_GENB_ACTZERO_ONE,   //Conveyor
                                  PWM_3_GENA_ACTZERO_ONE }; //Recycling Beacon

// Normal Operation. program generator to go to 1 at rising comare, 0 on falling compare
static uint32_t GEN_NORMAL[] = { PWM_1_GENA_ACTCMPAU_ONE | PWM_1_GENA_ACTCMPAD_ZERO,    //Servo1
                                 PWM_1_GENB_ACTCMPBU_ONE | PWM_1_GENB_ACTCMPBD_ZERO,    //Servo2
                                 PWM_0_GENA_ACTCMPAU_ONE | PWM_0_GENA_ACTCMPAD_ZERO,    //Servo3
                                 PWM_0_GENB_ACTCMPBU_ONE | PWM_0_GENB_ACTCMPBD_ZERO,    //Servo4
                                 PWM_1_GENA_ACTCMPAU_ONE | PWM_1_GENA_ACTCMPAD_ZERO,    //WheelL
                                 PWM_1_GENB_ACTCMPBU_ONE | PWM_1_GENB_ACTCMPBD_ZERO,    //WheelR
                                 PWM_2_GENB_ACTCMPBU_ONE | PWM_2_GENB_ACTCMPBD_ZERO,    //Conveyor
                                 PWM_3_GENA_ACTCMPAU_ONE | PWM_3_GENA_ACTCMPAD_ZERO };  //Recycling Beacon

//-------------Module specific functions---------------------------------

//----------Function Definitions------------------------------------------

/****************************************************************************
Function:       InitPWM_Modules
Parameters:     None
Returns:        None
Description:    Initialize PWM Module 0 and Module 1. Sets PWM clock to
                system clock/32
****************************************************************************/
void InitPWM_Modules(void)
{
  // start by enabling the clock to PWM Module 0 (PWM0)
  HWREG(SYSCTL_RCGCPWM) |= SYSCTL_RCGCPWM_R0;

  //enable the clock to PWM Module 1 (PWM1)
  HWREG(SYSCTL_RCGCPWM) |= SYSCTL_RCGCPWM_R1;

  // Select the PWM clock as System Clock/32
  HWREG(SYSCTL_RCC) = (HWREG(SYSCTL_RCC) & ~SYSCTL_RCC_PWMDIV_M) |
      (SYSCTL_RCC_USEPWMDIV | SYSCTL_RCC_PWMDIV_32);

  // make sure that the PWM module clock has gotten going to M0
  while ((HWREG(SYSCTL_PRPWM) & SYSCTL_PRPWM_R0) != SYSCTL_PRPWM_R0)
  {
    //Kill a few clock cycles if needed
  }

  // make sure that the PWM module clock has gotten going to M1
  while ((HWREG(SYSCTL_PRPWM) & SYSCTL_PRPWM_R1) != SYSCTL_PRPWM_R1)
  {
    //Kill a few clock cycles if needed
  }
}

/****************************************************************************
Function:       InitPWM_Servos12
Parameters:     None
Returns:        None
Description:    Initializes PWM1GENA and PWM1GENB on M0 for PB4 and PB5. Sets
                the duty cycle to zero and the period to the value set by
                SERVO_PERIOD_IN_MS.
*****************************************************************************/
void InitPWM_Servos12(void)
{
  //Just in case, enable GPIO port B
  EnablePort(PB);

  // disable the PWM while initializing
  HWREG(PWM0_BASE + PWM_O_1_CTL) = 0;

  // program generators to initially output a 0% DC
  HWREG(PWM0_BASE + PWM_O_1_GENA) = PWM_1_GENA_ACTZERO_ZERO;
  HWREG(PWM0_BASE + PWM_O_1_GENB) = PWM_1_GENB_ACTZERO_ZERO;

  // Set the PWM period. Since we are counting both up & down, we initialize
  // the load register to 1/2 the desired total period. We wll also program
  // the match compare registers to 1/2 the desired high time
  HWREG(PWM0_BASE + PWM_O_1_LOAD) = ((SERVO_PERIOD_IN_MS * PWMTicksPerMS) >> 1);

  // enable the PWM outputs
  HWREG(PWM0_BASE + PWM_O_ENABLE) |= (PWM_ENABLE_PWM2EN | PWM_ENABLE_PWM3EN);

  // now configure the pin to be a PWM output
  // start by selecting the alternate function for PB4 and PB5
  HWREG(GPIO_PORTB_BASE + GPIO_O_AFSEL) |= (PB4 | PB5);

  // now choose to map PWM to those pins, this is a mux value of 4 that we
  // want to use for specifying the function on bits 4 & 5
  HWREG(GPIO_PORTB_BASE + GPIO_O_PCTL) =
      (HWREG(GPIO_PORTB_BASE + GPIO_O_PCTL) & 0xff00ffff) +
      (4 << (5 * BitsPerNibble)) + (4 << (4 * BitsPerNibble));

  // Enable pins 4 and 5 on Port B for digital I/O
  SetPin2Digital(PB, PB4 | PB5);

  // make pins 4 and 5 on Port B into an output
  SetPin2Output(PB, PB4 | PB5);

  // set the up/down count mode, enable the PWM generator and make
  // both generator updates locally synchronized to zero count
  HWREG(PWM0_BASE + PWM_O_1_CTL) =
      (PWM_1_CTL_MODE | PWM_1_CTL_ENABLE | PWM_1_CTL_GENAUPD_LS | PWM_1_CTL_GENBUPD_LS);
}

/****************************************************************************
Function:       InitPWM_Servos34
Parameters:     None
Returns:        None
Description:    Initializes PWM0GENA and PWM0GENB on M0 for PB6 and PB7. Sets
                the duty cycle to zero and the period to the value set by
                SERVO_PERIOD_IN_MS.
*****************************************************************************/
void InitPWM_Servos34(void)
{
  //Just in case, enable GPIO port B
  EnablePort(PB);

  // disable the PWM while initializing
  HWREG(PWM0_BASE + PWM_O_0_CTL) = 0;

  // program generators to initially output a 0% DC
  HWREG(PWM0_BASE + PWM_O_0_GENA) = PWM_0_GENA_ACTZERO_ZERO;
  HWREG(PWM0_BASE + PWM_O_0_GENB) = PWM_0_GENB_ACTZERO_ZERO;

  // Set the PWM period. Since we are counting both up & down, we initialize
  // the load register to 1/2 the desired total period. We wll also program
  // the match compare registers to 1/2 the desired high time
  HWREG(PWM0_BASE + PWM_O_0_LOAD) = ((SERVO_PERIOD_IN_MS * PWMTicksPerMS) >> 1);

  // enable the PWM outputs
  HWREG(PWM0_BASE + PWM_O_ENABLE) |= (PWM_ENABLE_PWM0EN | PWM_ENABLE_PWM1EN);

  // now configure the pin to be a PWM output
  // start by selecting the alternate function for PB6
  HWREG(GPIO_PORTB_BASE + GPIO_O_AFSEL) |= (PB6 | PB7);

  // now choose to map PWM to those pins, this is a mux value of 4 that we
  // want to use for specifying the function on bits 6 & 7
  HWREG(GPIO_PORTB_BASE + GPIO_O_PCTL) =
      (HWREG(GPIO_PORTB_BASE + GPIO_O_PCTL) & 0x00ffffff) +
      (4 << (7 * BitsPerNibble)) + (4 << (6 * BitsPerNibble));

  // Enable pins 6 and 7 on Port B for digital I/O
  SetPin2Digital(PB, PB6 | PB7);

  // make pins 6 and 7 on Port B into an output
  SetPin2Output(PB, PB6 | PB7);

  // set the up/down count mode, enable the PWM generator and make
  // both generator updates locally synchronized to zero count
  HWREG(PWM0_BASE + PWM_O_0_CTL) =
      (PWM_0_CTL_MODE | PWM_0_CTL_ENABLE | PWM_0_CTL_GENAUPD_LS | PWM_0_CTL_GENBUPD_LS);
}

/****************************************************************************
Function:       InitPWM_Wheels
Parameters:     None
Returns:        None
Description:    Initializes PWM1GENA and PWM1GENB on M1 for PA6 and PA7. Sets
                the duty cycle to zero and the period to the value set by
                WHEEL_MOTOR_PERIOD_IN_US.
*****************************************************************************/
void InitPWM_Wheels(void)
{    //Just in case, enable GPIO port A
  EnablePort(PA);

  // disable the PWM while initializing
      HWREG(PWM1_BASE + PWM_O_1_CTL) = 0;

  // program generators to initially output a 0% DC
      HWREG(PWM1_BASE + PWM_O_1_GENA) = PWM_1_GENA_ACTZERO_ZERO;
      HWREG(PWM1_BASE + PWM_O_1_GENB) = PWM_1_GENB_ACTZERO_ZERO;

  // Set the PWM period. Since we are counting both up & down, we initialize
  // the load register to 1/2 the desired total period. We wll also program
  // the match compare registers to 1/2 the desired high time
  uint16_t DesiredPeriod =
      HWREG(PWM1_BASE + PWM_O_1_LOAD) = ((int)(WHEEL_MOTOR_PERIOD_IN_US / PWMUSPerTick) >> 1);

  // enable the PWM outputs
      HWREG(PWM1_BASE + PWM_O_ENABLE) |= (PWM_ENABLE_PWM2EN | PWM_ENABLE_PWM3EN);

  // now configure the pin to be a PWM output
  // start by selecting the alternate function for PB6
      HWREG(GPIO_PORTA_BASE + GPIO_O_AFSEL) |= (PA6 | PA7);

  // now choose to map PWM to those pins, this is a mux value of 5 that we
  // want to use for specifying the function on bits 6 & 7
      HWREG(GPIO_PORTA_BASE + GPIO_O_PCTL) =
      (HWREG(GPIO_PORTA_BASE + GPIO_O_PCTL) & 0x00ffffff) +
      (5 << (7 * BitsPerNibble)) + (5 << (6 * BitsPerNibble));

  // Enable pins 6 and 7 on Port A for digital I/O
  SetPin2Digital(PA, PA6 | PA7);

  // make pin 6 on Port A into an output
  SetPin2Output(PA, PA6 | PA7);

  // set the up/down count mode, enable the PWM generator and make
  // both generator updates locally synchronized to zero count
  HWREG(PWM1_BASE + PWM_O_1_CTL) =
      (PWM_1_CTL_MODE | PWM_1_CTL_ENABLE | PWM_1_CTL_GENAUPD_LS | PWM_1_CTL_GENBUPD_LS);
}

/****************************************************************************
Function:       InitPWM_Conveyor
Parameters:     None
Returns:        None
Description:    Initializes PWM2GENA on M1 for PFO. Sets
                the duty cycle to zero and the period to the value set by
                CONVEYOR_PERIOD_IN_US. Does not configure PWM2GENB.
*****************************************************************************/
void InitPWM_Conveyor(void)
{
  //Just in case, enable GPIO port F
  EnablePort(PF);

  // disable the PWM while initializing
  HWREG(PWM1_BASE + PWM_O_2_CTL) = 0;

  // program generators to initially output a 0% DC
  HWREG(PWM1_BASE + PWM_O_2_GENB) = PWM_2_GENB_ACTZERO_ZERO;

  // Set the PWM period. Since we are counting both up & down, we initialize
  // the load register to 1/2 the desired total period. We wll also program
  // the match compare registers to 1/2 the desired high time
  HWREG(PWM1_BASE + PWM_O_2_LOAD) = ((int)(CONVEYOR_PERIOD_IN_US / PWMUSPerTick) >> 1);

  // enable the PWM outputs
  HWREG(PWM1_BASE + PWM_O_ENABLE) |= (PWM_ENABLE_PWM5EN);

  // now configure the pin to be a PWM output
  // start by selecting the alternate function for PF0
  HWREG(GPIO_PORTF_BASE + GPIO_O_AFSEL) |= (PF1);

  // now choose to map PWM to those pins, this is a mux value of 5 that we
  // want to use for specifying the function on bit 0
  HWREG(GPIO_PORTF_BASE + GPIO_O_PCTL) =
      ((HWREG(GPIO_PORTF_BASE + GPIO_O_PCTL) & 0xffffff0f) + (5 << (1 * BitsPerNibble)));

  // Enable pin PF0 for digital I/O
  SetPin2Digital(PF, PF1);

  // make pin PF0 into an output
  SetPin2Output(PF, PF1);

  // set the up/down count mode, enable the PWM generator and make
  // both generator updates locally synchronized to zero count
  HWREG(PWM1_BASE + PWM_O_2_CTL) =
      (PWM_2_CTL_MODE | PWM_2_CTL_ENABLE | PWM_2_CTL_GENAUPD_LS | PWM_2_CTL_GENBUPD_LS);
}

/****************************************************************************
Function:       InitPWM_RecyclingBeacon
Parameters:     None
Returns:        None
Description:    Initializes PWM3GENA on M1 for PF2. Sets
                the duty cycle to zero and the period to a dummy value. Period
                will be set later in software. Does not configure PWM3GENB.
*****************************************************************************/
void InitPWM_RecyclingBeacon(void)
{
  //Just in case, enable GPIO port F
  EnablePort(PF);

  // disable the PWM while initializing
  HWREG(PWM1_BASE + PWM_O_3_CTL) = 0;

  // program generators to initially output a 0% DC
  HWREG(PWM1_BASE + PWM_O_3_GENA) = PWM_3_GENA_ACTZERO_ZERO;

  // Set the PWM period. Since we are counting both up & down, we initialize
  // the load register to 1/2 the desired total period. We wll also program
  // the match compare registers to 1/2 the desired high time
  HWREG(PWM1_BASE + PWM_O_3_LOAD) = (DUMMY_PERIOD);

  // enable the PWM outputs
  HWREG(PWM1_BASE + PWM_O_ENABLE) |= (PWM_ENABLE_PWM6EN);

  // now configure the pin to be a PWM output
  // start by selecting the alternate function for PB6
  HWREG(GPIO_PORTF_BASE + GPIO_O_AFSEL) |= (PF2);

  // now choose to map PWM to those pins, this is a mux value of 5 that we
  // want to use for specifying the function on bit 2
  HWREG(GPIO_PORTF_BASE + GPIO_O_PCTL) =
      ((HWREG(GPIO_PORTF_BASE + GPIO_O_PCTL) & 0xfffff0ff) + (5 << (2 * BitsPerNibble)));

  // Enable pin PF0 for digital I/O
  SetPin2Digital(PF, PF2);

  // make pin PF0 into an output
  SetPin2Output(PF, PF2);

  // set the up/down count mode, enable the PWM generator and make
  // both generator updates locally synchronized to zero count
  HWREG(PWM1_BASE + PWM_O_3_CTL) =
      (PWM_3_CTL_MODE | PWM_3_CTL_ENABLE | PWM_3_CTL_GENAUPD_LS | PWM_3_CTL_GENBUPD_LS);
}

/****************************************************************************
Function:       SetDC
Parameters:     Actuator (see enum in PWM.h) and DutyCycle (0-100)
Returns:        None
Description:    Assumes Actuator enum starts at zero. Sets the duty
                cycle on the appropriate PWM line.
*****************************************************************************/
void SetDC(Actuator_t Actuator, uint32_t DutyCycle)
{
  //Extract our index from the actuator enum variable
  uint8_t index = (int)Actuator;

  //Account for edge cases - 0% or 100% DC
  if (DutyCycle == 0)
  {
    // To program 0% DC, simply set the action on Zero to set the output to zero
    HWREG(GEN_ADDRESS[index]) = ACTZERO_ZERO[index];
  }
  else if (DutyCycle == 100)
  {
    // To program 100% DC, simply set the action on Zero to set the output to one
    HWREG(GEN_ADDRESS[index]) = ACTZERO_ONE[index];
  }
  else
  {
    //Restore the proper actions when the DC drops below 100% or rises above 0%
    // To restore the previos DC, simply set the action back to the normal actions
    HWREG(GEN_ADDRESS[index]) = GEN_NORMAL[index];

    //SPECIAL CASE - Servos need more precision in SetDC. Assumes they give
    //a value from 0 to 1000. Real DC is the given divided by 10 (ie a 75 input --> 7.5% DC )
    if ((Actuator == SERVO_1) || (Actuator == SERVO_2) || (Actuator == SERVO_3))
    {
      HWREG(CMP_ADDRESS[index]) = (((1000 - DutyCycle) * (HWREG(LOAD_ADDRESS[index]))) / 1000);
    }
    else
    {
      /*Set new DC value by setting compare value.
        For up-down counting, Compare = Load-1/2*Desired_Hi_Time
        In terms of number of ticks, that is (DutyCycle*Period)/100
        Thus, NumOfTicks = (DutyCycle*(2*Load)/100.
        Load is already expressed in number of ticks, so we're good. */
      HWREG(CMP_ADDRESS[index]) = (((100 - DutyCycle) * (HWREG(LOAD_ADDRESS[index]))) / 100);
    }
  }
}

/****************************************************************************
Function:       SetRecylingBeaconFrequency
Parameters:     uint16_t Desired period in us
Returns:        None
Description:    Sets the period on the recycling beacon PWM line. NEED TO
                RESET DUTY CYCLE AFTER CHANGING PERIOD!
*****************************************************************************/
void SetRecyclingBeaconPeriod(uint16_t PeriodInMicroSec)
{
  // disable the PWM while changing period
  HWREG(PWM1_BASE + PWM_O_3_CTL) = 0;

  // Set the PWM period. Since we are counting both up & down, we initialize
  // the load register to 1/2 the desired total period. We wll also program
  // the match compare registers to 1/2 the desired high time
  HWREG(PWM1_BASE + PWM_O_3_LOAD) = ((int)(PeriodInMicroSec / PWMUSPerTick) >> 1);

  // set the up/down count mode, enable the PWM generator and make
  // both generator updates locally synchronized to zero count
  HWREG(PWM1_BASE + PWM_O_3_CTL) =
      (PWM_3_CTL_MODE | PWM_3_CTL_ENABLE | PWM_3_CTL_GENAUPD_LS | PWM_3_CTL_GENBUPD_LS);
}

//***********************************************************************************
//-----------MODULE TEST HARNESS-----------------------------------------------------
//***********************************************************************************
/*
//#define PWM_TEST
#ifdef PWM_TEST

#include <stdio.h>
#include "ES_Configure.h"
#include "ES_Framework.h"
#include "ES_Port.h"
#include "termio.h"
#include "EnablePA25_PB23_PD7_PF0.h"

#define clrScrn() printf("\x1b[2J")
#define goHome() printf("\x1b[1,1H")
#define clrLine() printf("\x1b[K")


//Test the PWM functions to confirm that they are producing the desired output

int main(void)
{
     // Set the clock to run at 40MhZ using the PLL and 16MHz external crystal
    SysCtlClockSet(SYSCTL_SYSDIV_5 | SYSCTL_USE_PLL | SYSCTL_OSC_MAIN
        | SYSCTL_XTAL_16MHZ);
    TERMIO_Init();
    clrScrn();

    // When doing testing, it is useful to announce just which program
    // is running.
    puts("\rStarting Test Harness for \r");
    printf( "PWM_Test\r\n");
    printf( "%s %s\n", __TIME__, __DATE__);
    printf( "\n\r\n");
    printf( "Press any key to post key-stroke events to Service 0\n\r");
    printf( "Press 'd' to test event deferral \n\r");
    printf( "Press 'r' to test event recall \n\r");


    //Init PWM Lines
    InitPWM_Modules();
    printf("\n\rPWM Modules Init Success");
    InitPWM_Servos12();
    printf("\n\rPWM Servos 12 Init Success");
    InitPWM_Servos34();
    printf("\n\rPWM Servos 34 Init Success");
    InitPWM_Wheels();
    printf("\n\rPWM Wheels Init Success");
    InitPWM_Conveyor();
    printf("\n\rPWM Conveyor Init Success");
    InitPWM_RecyclingBeacon();
    printf("\n\rPWM Recycling Beacon Init Success");
    printf("\n\rPWM Init Success");

    //Put function tests here:

    //Set DC on all lines
    SetDC(SERVO_1, 10);
    SetDC(SERVO_2, 75);

    SetDC(SERVO_4, 75);
    SetDC(SERVO_3, 10);

    SetDC(WHEEL_L, 75);
    SetDC(WHEEL_R, 10);

    SetDC(CONVEYOR, 80);
    SetDC(RECYCLING_BEACON, 50);

    printf("\n\rDC's Set");

    //Set Period of Recycling beacon to 1.5kHz
    //NOTE: Need to reset DC after Setting a new period.
    SetRecyclingBeaconPeriod(800);
    SetDC(RECYCLING_BEACON, 50);
    printf("\n\rBeacon period set ");
    //***************************************


    //Enter the eternal loop
    for ( ; ;)
    {
    ;
    }

    return 0;
}

#endif
*/
//***********************************************************************************
//-----------END OF FILE-------------------------------------------------------------
//***********************************************************************************
