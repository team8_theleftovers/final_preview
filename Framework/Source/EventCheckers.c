/****************************************************************************
 Module
   EventCheckers.c

 Revision
   1.0.1

 Description
   This is the sample for writing event checkers along with the event
   checkers used in the basic framework test harness.

 Notes
   Note the use of static variables in sample event checker to detect
   ONLY transitions.

 History
 When           Who     What/Why
 -------------- ---     --------
 08/06/13 13:36 jec     initial version
****************************************************************************/

// this will pull in the symbolic definitions for events, which we will want
// to post in response to detecting events
#include "ES_Configure.h"
// this will get us the structure definition for events, which we will need
// in order to post events in response to detecting events
#include "ES_Events.h"
// if you want to use distribution lists then you need those function
// definitions too.
#include "ES_PostList.h"
// This include will pull in all of the headers from the service modules
// providing the prototypes for all of the post functions
#include "ES_ServiceHeaders.h"
// this test harness for the framework references the serial routines that
// are defined in ES_Port.c
#include "ES_Port.h"
// include our own prototypes to insure consistency between header &
// actual functionsdefinition
#include "EventCheckers.h"

#include "SortingModule.h"

#include "Move.h"
#include "parameters.h"

// This is the event checking function sample. It is not intended to be
// included in the module. It is only here as a sample to guide you in writing
// your own event checkers
#if 0
/****************************************************************************
 Function
   Check4Lock
 Parameters
   None
 Returns
   bool: true if a new event was detected
 Description
   Sample event checker grabbed from the simple lock state machine example
 Notes
   will not compile, sample only
 Author
   J. Edward Carryer, 08/06/13, 13:48
****************************************************************************/
bool Check4Lock(void)
{
  static uint8_t  LastPinState = 0;
  uint8_t         CurrentPinState;
  bool            ReturnVal = false;

  CurrentPinState = LOCK_PIN;
  // check for pin high AND different from last time
  // do the check for difference first so that you don't bother with a test
  // of a port/variable that is not going to matter, since it hasn't changed
  if ((CurrentPinState != LastPinState) &&
      (CurrentPinState == LOCK_PIN_HI)) // event detected, so post detected event
  {
    ES_Event ThisEvent;
    ThisEvent.EventType = ES_LOCK;
    ThisEvent.EventParam = 1;
    // this could be any of the service post function, ES_PostListx or
    // ES_PostAll functions
    ES_PostList01(ThisEvent);
    ReturnVal = true;
  }
  LastPinState = CurrentPinState; // update the state for next time

  return ReturnVal;
}

#endif

/****************************************************************************
 Function
   Check4Keystroke
 Parameters
   None
 Returns
   bool: true if a new key was detected & posted
 Description
   checks to see if a new key from the keyboard is detected and, if so,
   retrieves the key and posts an ES_NewKey event to TestHarnessService0
 Notes
   The functions that actually check the serial hardware for characters
   and retrieve them are assumed to be in ES_Port.c
   Since we always retrieve the keystroke when we detect it, thus clearing the
   hardware flag that indicates that a new key is ready this event checker
   will only generate events on the arrival of new characters, even though we
   do not internally keep track of the last keystroke that we retrieved.
 Author
   J. Edward Carryer, 08/06/13, 13:48
****************************************************************************/
bool Check4Keystroke(void)
{
  if (IsNewKeyReady())   // new key waiting?
  {
    ES_Event_t ThisEvent;
    ThisEvent.EventType = ES_NEW_KEY;
    ThisEvent.EventParam = GetNewKey();

    // test distribution list functionality by sending the 'L' key out via
    // a distribution list.
    if (ThisEvent.EventParam == 'a')
    {
      ES_Event_t NewEvent;
      NewEvent.EventType = BEACON_SENSED;
      PostPositioningAgainstGW(NewEvent);
      PostRecycling(NewEvent);
    }
    if (ThisEvent.EventParam == 'b')
    {
      ES_Event_t NewEvent;
      NewEvent.EventType = BEACON_CAME_OFF_L;
      PostPositioningAgainstGW(NewEvent);
      PostRecycling(NewEvent);
    }
    if (ThisEvent.EventParam == 'c')
    {
      ES_Event_t NewEvent;
      NewEvent.EventType = BEACON_CAME_OFF_R;
      PostPositioningAgainstGW(NewEvent);
      PostRecycling(NewEvent);
    }
    if (ThisEvent.EventParam == 'd')
    {
      ES_Event_t NewEvent;
      NewEvent.EventType = BEACON_LOST;
      PostPositioningAgainstGW(NewEvent);
      PostRecycling(NewEvent);
    }
    if (ThisEvent.EventParam == 'e')
    {
      ES_Event_t NewEvent;
      NewEvent.EventType = CLEANING_UP;
      PostMasterSM(NewEvent);
      PostSortingModule(NewEvent);
    }
    if (ThisEvent.EventParam == 'f')
    {
      ES_Event_t NewEvent;
      NewEvent.EventType = START;
      PostMasterSM(NewEvent);
    }
    if (ThisEvent.EventParam == 'g')
    {
      ES_Event_t NewEvent;
      NewEvent.EventType = GW_DETECTED;
      PostMasterSM(NewEvent);
    }
    if (ThisEvent.EventParam == 'h')
    {
      ES_Event_t NewEvent;
      NewEvent.EventType = GAME_OVER;
      PostPositioningAgainstGW(NewEvent);
      PostRecycling(NewEvent);
      PostMasterSM(NewEvent);
      PostSortingModule(NewEvent);
    }
    if (ThisEvent.EventParam == 'i')
    {
      ES_Event_t NewEvent;
      NewEvent.EventType = EXIT;
      PostPositioningAgainstGW(NewEvent);
      PostRecycling(NewEvent);
    }
    if (ThisEvent.EventParam == 'j')
    {
      ES_Event_t NewEvent;
      NewEvent.EventType = BUMP;
      NewEvent.EventParam = 1;
      PostPositioningAgainstGW(NewEvent);
      //PostMasterSM(NewEvent);
    }
    if (ThisEvent.EventParam == 'k')
    {
      ES_Event_t NewEvent;
      NewEvent.EventType = BUMP;
      NewEvent.EventParam = 2;
      PostPositioningAgainstGW(NewEvent);
    }
    if (ThisEvent.EventParam == 'l')
    {
      ES_Event_t NewEvent;
      NewEvent.EventType = BUMP;
      NewEvent.EventParam = 3;
      PostPositioningAgainstGW(NewEvent);
      //PostMasterSM(NewEvent);
    }
    if (ThisEvent.EventParam == 'm')
    {
      ES_Event_t NewEvent;
      NewEvent.EventType = BUMP;
      NewEvent.EventParam = 4;
      PostPositioningAgainstGW(NewEvent);
    }
    if (ThisEvent.EventParam == 'n')
    {
      ES_Event_t NewEvent;
      NewEvent.EventType = BUMP;
      NewEvent.EventParam = 5;
      PostPositioningAgainstGW(NewEvent);
      //PostMasterSM(NewEvent);
    }
    if (ThisEvent.EventParam == 'o')
    {
      ES_Event_t NewEvent;
      NewEvent.EventType = BUMP;
      NewEvent.EventParam = 6;
      PostPositioningAgainstGW(NewEvent);
    }
    if (ThisEvent.EventParam == 'p')
    {
      ES_Event_t NewEvent;
      NewEvent.EventType = BUMP;
      NewEvent.EventParam = 7;
      PostPositioningAgainstGW(NewEvent);
      //PostMasterSM(NewEvent);
    }
    if (ThisEvent.EventParam == 'q')
    {
      ES_Event_t NewEvent;
      NewEvent.EventType = BUMP;
      NewEvent.EventParam = 8;
      PostPositioningAgainstGW(NewEvent);
    }
    if (ThisEvent.EventParam == 'r')
    {
      ES_Event_t NewEvent;
      NewEvent.EventType = FLUSH_WITH_WALL;
      PostMasterSM(NewEvent);
    }
    if (ThisEvent.EventParam == 's')
    {
      ES_Event_t NewEvent;
      NewEvent.EventType = WRITE_FINISHED;
      PostPositioningAgainstGW(NewEvent);
    }
    if (ThisEvent.EventParam == 't')
    {
      ES_Event_t NewEvent;
      NewEvent.EventType = RECYCLING_BALL;
      PostMasterSM(NewEvent);
    }
    if (ThisEvent.EventParam == 'u')
    {
      ES_Event_t NewEvent;
      NewEvent.EventType = HIT_BOT_IN_FRONT;
      PostMasterSM(NewEvent);
    }
    if (ThisEvent.EventParam == 'v')
    {
      ES_Event_t NewEvent;
      NewEvent.EventType = HIT_BOT_BEHIND;
      PostMasterSM(NewEvent);
    }
    if (ThisEvent.EventParam == 'x')
    {
      ES_Event_t NewEvent;
      NewEvent.EventType = BUMP;
      NewEvent.EventParam = 1;
      PostCollisionDetector(NewEvent);
      //PostMasterSM(NewEvent);
    }
    /*
    if (ThisEvent.EventParam == 'v')
    {
      ES_Event_t NewEvent;
      NewEvent.EventType = HIT_BOT_BEHIND;
      PostPositioningAgainstGW(NewEvent);
    }
    if (ThisEvent.EventParam == 'v')
    {
      ES_Event_t NewEvent;
      NewEvent.EventType = HIT_BOT_BEHIND;
      PostPositioningAgainstGW(NewEvent);
    }*/
    if (ThisEvent.EventParam == 'y')
    {
      debugIncreaseGeneralCount();
    }

    if (ThisEvent.EventParam == '1')
    {
      SetServo(TRAPDOOR_SERVO, SORT_SERVO_REC_ANGLE);
    }

    if (ThisEvent.EventParam == '2')
    {
      SetServo(TRAPDOOR_SERVO, SORT_SERVO_GARB_ANGLE);
    }

    if (ThisEvent.EventParam == '3')
    {
      printf( "\n\r Garbage Count: %d\n\r",   QueryGarbageCount());
      printf( "\n\r Recycling Count: %d\n\r", QueryRecyclingCount());
      printf( "\n\r General Count: %d\n\r",   QueryGeneralCount());
    }

    return true;
  }
  return false;
}
