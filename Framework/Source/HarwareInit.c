/****************************************************************************
 Module: HardwareInit.c
 Revision 1.0.1
 Description: This module includes functions for initializing hardware.
 on Port B
 Notes:
 History
 When           Who     What/Why
 -------------- ---     --------
 10/11/15 19:55 jec     First Creation

 ****************************************************************************/

/*----------------------------- Include Files -----------------------------*/
#include "Parameters.h"

#include "Ports.h"
#include "PWM.h"
#include "Timers.h" //Include input capture functionality
#include "termio.h"

/*----------------------------- Module Private Functions -----------------------------*/
static void InitGPIO(void);

/*----------------------------- Functions Definitions -----------------------------*/

/****************************************************************************
Function:       InitHW
Parameters:     None
Returns:        None
Description:    Call in main function to init all hardware. Prints Success
                messages to indicate hardware init functions have completed.
*****************************************************************************/
void InitHW(void)
{
  //Init GPIO Lines
  InitGPIO();
  printf("\n\rGPIO Init Success");

  //Init PWM Lines
  InitPWM_Modules();
  InitPWM_Servos12();
  InitPWM_Servos34();
  InitPWM_Wheels();
  InitPWM_Conveyor();
  InitPWM_RecyclingBeacon();
  printf("\n\rPWM Init Success");

  //Init SSI 0
  //InitSSI();
  //printf("\n\rSSI Init Success");

  //Init Input Capture (2 for wheels, 2 for Beacon sensors)
  //InitEncoderInputCapture();
  //InitBeaconSensorInputCapture();
  //printf("\n\rInput Capture Init Success");

  //Init Periodic timer for Motor Control (wheels)
  //InitControllerPeriodicInt();
  //printf("\n\rPeriodic Timer Init Success");
}

/****************************************************************************
Function:       InitGPIO
Parameters:     None
Returns:        None
Description:    Using the functions defined in Ports.c and the parameters set
                in Parameters.h, initialize all Ports and set up all
                dedicated GPIO lines.
*****************************************************************************/
static void InitGPIO(void)
{
  //Enable Ports A-F
  EnablePortA();
  EnablePortB();
  EnablePortC();
  EnablePortD();
  EnablePortE();
  EnablePortF();

  //---Enable GPIO Pins as digital inputs/outputs---

  //Limit Switches
  SetDPin2Digital(LIMIT_FL | LIMIT_FR | LIMIT_RL | LIMIT_RR);
  SetDPin2Input(LIMIT_FL | LIMIT_FR | LIMIT_RL | LIMIT_RR);

  //North/South Team status pin
  SetFPin2Digital(TEAM_STATUS_GPIO);
  SetFPin2Input(TEAM_STATUS_GPIO);

  //Wheel motor pins
  SetCPin2Digital(WHEEL_L_GPIO | WHEEL_R_GPIO);
  SetCPin2Output(WHEEL_L_GPIO | WHEEL_R_GPIO);

  //Conveyor Motor Pin
  SetFPin2Digital(CONVEYOR_MOTOR_GPIO);
  SetFPin2Output(CONVEYOR_MOTOR_GPIO);
}

/*
//***********************************************************************************
//-----------MODULE TEST HARNESS-----------------------------------------------------
//***********************************************************************************
//#define HW_INIT_TEST
#ifdef HW_INIT_TEST

//Debug
#include "termio.h"

#include <stdio.h>
#include "ES_Configure.h"
#include "ES_Framework.h"
#include "ES_Port.h"
#include "termio.h"
#include "EnablePA25_PB23_PD7_PF0.h"

#define clrScrn() printf("\x1b[2J")
#define goHome() printf("\x1b[1,1H")
#define clrLine() printf("\x1b[K")

int main(void)
{

    // Set the clock to run at 40MhZ using the PLL and 16MHz external crystal
    SysCtlClockSet(SYSCTL_SYSDIV_5 | SYSCTL_USE_PLL | SYSCTL_OSC_MAIN
        | SYSCTL_XTAL_16MHZ);
    TERMIO_Init();
    clrScrn();

    // When doing testing, it is useful to announce just which program
    // is running.
    puts("\rTimers Test Harnessfor \r");
    printf( "Timer_test\r\n");
    printf( "%s %s\n", __TIME__, __DATE__);
    printf( "\n\r\n");
    printf( "Press any key to post key-stroke events to Service 0\n\r");
    printf( "Press 'd' to test event deferral \n\r");
    printf( "Press 'r' to test event recall \n\r");

    //Init all hardware
    InitHW();

    //Set all GPIO ouptput pins to HIGH
    PORTD |= PD2; //x
    PORTD |= PD3;
    PORTD |= PD6;
    PORTD |= PD7;
    PORTC |= PC6;
    PORTC |= PC7;
    PORTF |= PF4;
    PORTF |= PF0;
    printf("\n\rGPIOs Set");

    //Set DC on all lines
    SetDC(SERVO_1, 10);
    SetDC(SERVO_2, 75);

    SetDC(SERVO_4, 50);
    SetDC(SERVO_3, 10);

    SetDC(WHEEL_L, 75);
    SetDC(WHEEL_R, 10);

    SetDC(CONVEYOR, 80);
    SetDC(RECYCLING_BEACON, 50);

    printf("\n\rDC's Set");

    //Set Period of Recycling beacon to 1.5kHz
    //NOTE: Need to reset DC after Setting a new period.
    SetRecyclingBeaconPeriod(800);
    SetDC(RECYCLING_BEACON, 50);
    printf("\n\rBeacon period set ");
    //***************************************

    while(1)
        {;}
    return 0;
}


#endif*/