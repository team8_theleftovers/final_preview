/***************************************************************************
 Module:        Timer_utils.c
 Revision:      1.0.0
 Description:   Collection of functions to manipulate Timer hardware
 Notes:
****************************************************************************/

/*----------------------------- Include Files -----------------------------*/
//Module header file
#include "Timers.h"

// the common headers for C99 types
#include <stdint.h>
#include <stdbool.h>

//#include "inc/tm4c123gh6pm.h"
#include "inc/hw_ints.h"
#include "inc/hw_memmap.h"
#include "inc/hw_gpio.h"
#include "inc/hw_timer.h"
#include "inc/hw_sysctl.h"
#include "inc/hw_types.h"
#include "inc/hw_nvic.h"
#include "bitdefs.h"
#include "driverlib/pin_map.h"
#include "driverlib/sysctl.h"
#include "driverlib/gpio.h"
#include "driverlib/timer.h"
//#include "driverlib/interrupt.h"

//What headers do we need?
#include "driverlib/interrupt.h"
#include "driverlib/uart.h"
#include "driverlib/systick.h"
#include "driverlib/ssi.h"
#include "utils/uartstdio.h"

//include header files for the framework
#include "ES_Configure.h"
#include "ES_Framework.h"
#include "ES_DeferRecall.h"
#include "ES_ShortTimer.h"

//Include for pin definitions
#include "Parameters.h"

//Include for port GPIO manipulatin
#include "Ports.h"

/*----------------------------- Defines -----------------------------*/
#define TimerTicksPerMS 40000 //Using system clock at 40 MHz
#define TimerTicsPerUS 40     //Using system clock at 40MHz
#define BitsPerNibble 4

/*----------------------------- Functions Definitions -----------------------------*/

/****************************************************************************
Function:       InitControllerPeriodicInt
Parameters:     None
Returns:        None
Description:    Periodic timer for calling PID control function. Initialize wide
                timer 1 Timer A in periodic mode, Enable the timer interrupt,
                Set the time using PID_CONTROLLER_PERIOD_IN_MS define in Parameters.h
*****************************************************************************/
void InitControllerPeriodicInt(void)
{
  // start by enabling the clock to the timer (Wide Timer 0)
  HWREG(SYSCTL_RCGCWTIMER) |= SYSCTL_RCGCWTIMER_R1;

  // kill a few cycles to let the clock get going
  while ((HWREG(SYSCTL_PRWTIMER) & SYSCTL_PRWTIMER_R1) != SYSCTL_PRWTIMER_R1)
  {}
  // make sure that timer (Timer A) is disabled before configuring
  HWREG(WTIMER1_BASE + TIMER_O_CTL) &= ~TIMER_CTL_TAEN;

  // set it up in 32bit wide (individual, not concatenated) mode
  HWREG(WTIMER1_BASE + TIMER_O_CFG) = TIMER_CFG_16_BIT;

  // set up timer A in periodic mode so that it repeats the time-outs
  HWREG(WTIMER1_BASE + TIMER_O_TAMR) = (HWREG(WTIMER1_BASE + TIMER_O_TAMR) & ~TIMER_TAMR_TAMR_M) | TIMER_TAMR_TAMR_PERIOD;

  // set timeout period
  HWREG(WTIMER1_BASE + TIMER_O_TAILR) = PID_CONTROLLER_PERIOD_IN_MS * TimerTicksPerMS;

  // enable a local timeout interrupt
  HWREG(WTIMER1_BASE + TIMER_O_IMR) |= TIMER_IMR_TATOIM;

  // enable the Timer A in Wide Timer 1 interrupt in the NVIC
  // it is interrupt number 96 so appears in EN3 at bit 0
  HWREG(NVIC_EN3) |= BIT0HI;

  // make sure interrupts are enabled globally
  __enable_irq();

  // now kick the timer off by enabling it and enabling the timer to
  // stall while stopped by the debugger
  HWREG(WTIMER1_BASE + TIMER_O_CTL) |= (TIMER_CTL_TAEN | TIMER_CTL_TASTALL);
}

/****************************************************************************
Function:       InitEncoderInputCapture
Parameters:     None
Returns:        None
Description:    Input Capture Init on encoder pins. Interrupt is triggered on
                rising edge on PC4 (Timer A) and PC5 (Timer B) port inputs.
*****************************************************************************/
void InitEncoderInputCapture(void)
{
  //Enable Port C just in case
  EnablePort(PC);

  // start by enabling the clock to the timer (Wide Timer 0)
  HWREG(SYSCTL_RCGCWTIMER) |= SYSCTL_RCGCWTIMER_R0;

  // Make sure the timer is ready
  while ((HWREG(SYSCTL_PRWTIMER) & SYSCTL_PRWTIMER_R0) != SYSCTL_PRWTIMER_R0)
  {
    //Kill a few clock cycles if needed
  }

  // make sure that the timers (Timer A and B) are disabled before configuring
  HWREG(WTIMER0_BASE + TIMER_O_CTL) &= ~(TIMER_CTL_TAEN | TIMER_CTL_TBEN);

  // set it up in 32bit wide (individual, not concatenated) mode
  // the constant name derives from the 16/32 bit timer, but this is a 32/64
  // bit timer so we are setting the 32bit mode
  HWREG(WTIMER0_BASE + TIMER_O_CFG) = TIMER_CFG_16_BIT;

  // we want to use the full 32 bit count, so initialize the Interval Load
  // register to 0xffff.ffff (its default value :-)
  HWREG(WTIMER0_BASE + TIMER_O_TAILR) = 0xffffffff;
  HWREG(WTIMER0_BASE + TIMER_O_TBILR) = 0xffffffff;

  // set up timer A in capture mode (TAMR=3, TAAMS = 0),
  // for edge time (TACMR = 1) and up-counting (TACDIR = 1)
  HWREG(WTIMER0_BASE + TIMER_O_TAMR) = (HWREG(WTIMER0_BASE + TIMER_O_TAMR) &
      ~TIMER_TAMR_TAAMS) | (TIMER_TAMR_TACDIR | TIMER_TAMR_TACMR | TIMER_TAMR_TAMR_CAP);

  // set up timer B in capture mode (TBMR=3, TBAMS = 0),
  // for edge time (TBCMR = 1) and up-counting (TBCDIR = 1)
  HWREG(WTIMER0_BASE + TIMER_O_TBMR) = (HWREG(WTIMER0_BASE + TIMER_O_TBMR) &
      ~TIMER_TBMR_TBAMS) | (TIMER_TBMR_TBCDIR | TIMER_TBMR_TBCMR | TIMER_TBMR_TBMR_CAP);

  // To set the event to rising edge, we need to modify the TAEVENT and TBEVENT bits
  // in GPTMCTL. Rising edge = 00, so we clear the TAEVENT and TBEVENT bits
  HWREG(WTIMER0_BASE + TIMER_O_CTL) &= ~(TIMER_CTL_TAEVENT_M | TIMER_CTL_TBEVENT_M);

  // Now Set up the port to do the capture (clock was enabled earlier)
  // start by setting the alternate function for PC4 (WT0CCP0) and PC5 (WT0CCP1)
  HWREG(GPIO_PORTC_BASE + GPIO_O_AFSEL) |= (PC4 | PC5);

  // Then, map PC4 and PC5's alternate function to WT0CCP0 and WT0CCP1
  // 7 is the mux value to select WT0CCP0 and WT0CCP1.
  HWREG(GPIO_PORTC_BASE + GPIO_O_PCTL) =
      (HWREG(GPIO_PORTC_BASE + GPIO_O_PCTL) & 0xff00ffff) +
      (7 << (4 * BitsPerNibble)) + (7 << (5 * BitsPerNibble));

  // Enable pin on Port C for digital I/O
  SetPin2Digital(PC, PC4 | PC5);

  // make pin 4 on Port C into an input
  SetPin2Input(PC, PC4 | PC5);

  // back to the timer to enable a local capture interrupt
  HWREG(WTIMER0_BASE + TIMER_O_IMR) |= (TIMER_IMR_CAEIM | TIMER_IMR_CBEIM);

  // enable Timer A and B in Wide Timer 0 interrupt in the NVIC
  // A is interrupt number 94 so it appears in EN2 at bit 30.
  // B is interrupt number 95 so it appears in EN2 at bit 31. (see pg 105 and 134)
  HWREG(NVIC_EN2) |= (BIT30HI | BIT31HI);

  // make sure interrupts are enabled globally
  __enable_irq();

  // now kick the timer off by enabling it and enabling the timer to
  // stall while stopped by the debugger
  HWREG(WTIMER0_BASE + TIMER_O_CTL) |=
      (TIMER_CTL_TAEN | TIMER_CTL_TBEN | TIMER_CTL_TASTALL | TIMER_CTL_TBSTALL);
}

/****************************************************************************
Function:       InitBeaconSensorInputCapture
Parameters:     None
Returns:        None
Description:    Input Capture Init on beacon sensor pins. Interrupt is triggered on
                rising edge on PD0 (Timer A) and PD1 (Timer B) port inputs. Wide timer 2
*****************************************************************************/
void InitBeaconSensorInputCapture(void)
{
  //Enable Port D just in case
  EnablePort(PD);

  // start by enabling the clock to the timer (Wide Timer 2)
  HWREG(SYSCTL_RCGCWTIMER) |= SYSCTL_RCGCWTIMER_R2;

  // Make sure the timer is ready
  while ((HWREG(SYSCTL_PRWTIMER) & SYSCTL_PRWTIMER_R2) != SYSCTL_PRWTIMER_R2)
  {
    //Kill a few clock cycles if needed
  }

  // make sure that the timers (Timer A and B) are disabled before configuring
  HWREG(WTIMER2_BASE + TIMER_O_CTL) &= ~(TIMER_CTL_TAEN | TIMER_CTL_TBEN);

  // set it up in 32bit wide (individual, not concatenated) mode
  // the constant name derives from the 16/32 bit timer, but this is a 32/64
  // bit timer so we are setting the 32bit mode
  HWREG(WTIMER2_BASE + TIMER_O_CFG) = TIMER_CFG_16_BIT;

  // we want to use the full 32 bit count, so initialize the Interval Load
  // register to 0xffff.ffff (its default value :-)
  HWREG(WTIMER2_BASE + TIMER_O_TAILR) = 0xffffffff;
  HWREG(WTIMER2_BASE + TIMER_O_TBILR) = 0xffffffff;

  // set up timer A in capture mode (TAMR=3, TAAMS = 0),
  // for edge time (TACMR = 1) and up-counting (TACDIR = 1)
  HWREG(WTIMER2_BASE + TIMER_O_TAMR) = (HWREG(WTIMER2_BASE + TIMER_O_TAMR) &
      ~TIMER_TAMR_TAAMS) | (TIMER_TAMR_TACDIR | TIMER_TAMR_TACMR | TIMER_TAMR_TAMR_CAP);

  // set up timer B in capture mode (TBMR=3, TBAMS = 0),
  // for edge time (TBCMR = 1) and up-counting (TBCDIR = 1)
  HWREG(WTIMER2_BASE + TIMER_O_TBMR) = (HWREG(WTIMER2_BASE + TIMER_O_TBMR) &
      ~TIMER_TBMR_TBAMS) | (TIMER_TBMR_TBCDIR | TIMER_TBMR_TBCMR | TIMER_TBMR_TBMR_CAP);

  // To set the event to rising edge, we need to modify the TAEVENT and TBEVENT bits
  // in GPTMCTL. Rising edge = 00, so we clear the TAEVENT and TBEVENT bits
  HWREG(WTIMER2_BASE + TIMER_O_CTL) &= ~(TIMER_CTL_TAEVENT_M | TIMER_CTL_TBEVENT_M);

  // Now Set up the port to do the capture (clock was enabled earlier)
  // start by setting the alternate function for PC4 (WT0CCP0) and PC5 (WT0CCP1)
  HWREG(GPIO_PORTD_BASE + GPIO_O_AFSEL) |= (PD0 | PD1);

  // Then, map PD0 and PD1's alternate function to WT0CCP0 and WT0CCP1
  // 7 is the mux value to select WT0CCP0 and WT0CCP1.
  HWREG(GPIO_PORTD_BASE + GPIO_O_PCTL) =
      (HWREG(GPIO_PORTD_BASE + GPIO_O_PCTL) & 0xffffff00) +
      (7 << (1 * BitsPerNibble)) + (7 << (0 * BitsPerNibble));

  // Enable pins for digital I/O
  SetPin2Digital(PD, PD0 | PD1);

  // make pins into inputs
  SetPin2Input(PD, PD0 | PD1);

  // back to the timer to enable a local capture interrupt
  HWREG(WTIMER2_BASE + TIMER_O_IMR) |= (TIMER_IMR_CAEIM | TIMER_IMR_CBEIM);

  // enable Timer A and B in Wide Timer 2 interrupt in the NVIC
  // A is interrupt number 98 so it appears in EN3 at bit 2.
  // B is interrupt number 99 so it appears in EN3 at bit 3. (see pg 105 and 134)
  HWREG(NVIC_EN3) |= (BIT2HI | BIT3HI);

  // make sure interrupts are enabled globally
  __enable_irq();

  // now kick the timer off by enabling it and enabling the timer to
  // stall while stopped by the debugger
  HWREG(WTIMER2_BASE + TIMER_O_CTL) |=
      (TIMER_CTL_TAEN | TIMER_CTL_TBEN | TIMER_CTL_TASTALL | TIMER_CTL_TBSTALL);
}

//Make simple functions to clear flag interrupt flags?
void ClearControllerIntFlag(void)
{
  HWREG(WTIMER1_BASE + TIMER_O_ICR) = TIMER_ICR_TATOCINT;
}

//***********************************************************************************
//-----------MODULE TEST HARNESS-----------------------------------------------------
//***********************************************************************************
//#define TIMER_TEST
#define HW_INIT_TEST
#if defined(TIMER_TEST) || defined(HW_INIT_TEST)

//Debug
#include "termio.h"

//---------Interrupts Reponses-----------------
static uint8_t TimeoutCount = 0;

//ISR functionality for periodic init
void ControllerPeriodicTestResponse(void)
{
  //Clear interrupt flag
  HWREG(WTIMER1_BASE + TIMER_O_ICR) = TIMER_ICR_TATOCINT;

  //Slow printing. Only goes every 256 times
  static uint8_t  spacer = 0;
  static int8_t   i = 1;

  //Raise GPIO line
  if (i == 1)
  {
    PORTB |= PB2;
  }

  if (spacer == 0)
  {
    printf("\n\rController Periodic");
  }

  //Lower GPIO line
  if (i == -1)
  {
    PORTB &= ~PB2;
  }

  ++spacer;
  i *= -1;

  //GPIO lines only change every other time, so actual ISR period is measuredPeriod/2
}

//ISR functionality for input capture - encoder 1
void Encoder1ICTestResponse(void)
{
  //Clear interrupt flag
  HWREG(WTIMER0_BASE + TIMER_O_ICR) = TIMER_ICR_CAECINT;

  printf("\n\rEncoder 1");
}

//ISR functionality for input capture - encoder 1
void Encoder2ICTestResponse(void)
{
  //Clear interrupt flag
  HWREG(WTIMER0_BASE + TIMER_O_ICR) = TIMER_ICR_CBECINT;

  printf("\n\rEncoder 2");
}

//ISR functionality for input capture - encoder 1
void Beacon1ICTestResponse(void)
{
  //Clear interrupt flag
  HWREG(WTIMER2_BASE + TIMER_O_ICR) = TIMER_ICR_CAECINT;

  printf("\n\rBeacon 1");
}

//ISR functionality for input capture - encoder 1
void Beacon2ICTestResponse(void)
{
  //Clear interrupt flag
  HWREG(WTIMER2_BASE + TIMER_O_ICR) = TIMER_ICR_CBECINT;

  printf("\n\rBeacon 2");
}

#endif

#ifdef TIMER_TEST

#include <stdio.h>
#include "ES_Configure.h"
#include "ES_Framework.h"
#include "ES_Port.h"
#include "termio.h"
#include "EnablePA25_PB23_PD7_PF0.h"

#define clrScrn() printf("\x1b[2J")
#define goHome() printf("\x1b[1,1H")
#define clrLine() printf("\x1b[K")

//Test the Timer functions to confirm that they are producing the desired output
int main(void)
{
  // Set the clock to run at 40MhZ using the PLL and 16MHz external crystal
  SysCtlClockSet(SYSCTL_SYSDIV_5 | SYSCTL_USE_PLL | SYSCTL_OSC_MAIN
      | SYSCTL_XTAL_16MHZ);
  TERMIO_Init();
  clrScrn();

  // When doing testing, it is useful to announce just which program
  // is running.
  puts("\rTimers Test Harnessfor \r");
  printf( "Timer_test\r\n");
  printf( "%s %s\n", __TIME__, __DATE__);
  printf( "\n\r\n");
  printf( "Press any key to post key-stroke events to Service 0\n\r");
  printf( "Press 'd' to test event deferral \n\r");
  printf( "Press 'r' to test event recall \n\r");

  //***************************************
  //Put function tests here:
  //Enable GPIO for testing
  EnablePort(PB);
  SetPin2Digital(PB, PB2);
  SetPin2Output(PB, PB2);

  InitControllerPeriodicInt();
  printf( "\n\rController Periodic Init Success");

  //InitEncoderInputCapture();
  printf( "\n\rEncoder IC Init Success");

  //InitBeaconSensorInputCapture();
  printf( "\n\rBeacon Sensor IC Init Success");

  //***************************************
  //Enter the eternal loop
  for ( ; ;)
  {
    ;
  }
  return 0;
}

#endif
//***********************************************************************************
//-----------END OF FILE-------------------------------------------------------------
//***********************************************************************************
