/****************************************************************************
 Module: HardwareInit.c
 Revision 1.0.1
 Description: This module includes functions for initializing hardware.
 on Port B
 Notes:
 History
 When           Who     What/Why
 -------------- ---     --------
 10/11/15 19:55 jec     First Creation

 ****************************************************************************/

/*----------------------------- Include Files -----------------------------*/
#include "Parameters.h"
#include "Move.h"
#include "Ports.h"
#include "PWM.h"
#include "SPI.h"
#include "Timers.h" //Include input capture functionality
#include "termio.h"
#include "ADMulti.h"

/*----------------------------- Module Private Functions -----------------------------*/
static void InitGPIO(void);

/*----------------------------- Functions Definitions -----------------------------*/

/****************************************************************************
Function:       InitHW
Parameters:     None
Returns:        None
Description:    Call in main function to init all hardware. Prints Success
                messages to indicate hardware init functions have completed.
*****************************************************************************/
void InitHW(void)
{
  //Init GPIO Lines
  InitGPIO();
  printf("\n\rGPIO Init Success");

  //Init PWM Lines
  InitPWM_Modules();
  InitPWM_Servos12();// ??What the heck??
  InitPWM_Servos34();
  InitPWM_Wheels();
  InitPWM_Conveyor();

  InitPWM_RecyclingBeacon();
  printf("\n\rPWM Init Success");

  //Init SSI 0
  InitSSI();
  printf("\n\rSSI Init Success");

  //Init Input Capture (2 for wheels, 2 for Beacon sensors)
  //InitEncoderInputCapture();
  InitBeaconSensorInputCapture();
  //printf("\n\rInput Capture Init Success");

  //Init Periodic timer for Motor Control (wheels)
  //InitControllerPeriodicInt();
  //printf("\n\rPeriodic Timer Init Success");

  ADC_MultiInit(2);
  //Set wheels to be stopped
  Move(STOP, 0);
  SetServo( GARBAGE_SERVO,    SERVO_CLOSED_ANGLE);
  SetServo( RECYCLING_SERVO,  SERVO_CLOSED_ANGLE);
  SetServo( TRAPDOOR_SERVO,   SERVO_CLOSED_ANGLE);
}

/****************************************************************************
Function:       InitGPIO
Parameters:     None
Returns:        None
Description:    Using the functions defined in Ports.c and the parameters set
                in Parameters.h, initialize all Ports and set up all
                dedicated GPIO lines.
*****************************************************************************/
static void InitGPIO(void)
{
  //Enable Ports A-F
  EnablePort( PA);
  EnablePort( PB);
  EnablePort( PC);
  EnablePort( PD);
  EnablePort( PE);
  EnablePort( PF);

  //---Enable GPIO Pins as digital inputs/outputs---

  //Limit Switches
  SetPin2Digital(PD, LIMIT_FL | LIMIT_FR | LIMIT_RL | LIMIT_RR);
  SetPin2Input(PD, LIMIT_FL | LIMIT_FR | LIMIT_RL | LIMIT_RR);

  //North/South Team status pin
  SetPin2Digital(PF, TEAM_STATUS_GPIO);
  SetPin2Input(PF, TEAM_STATUS_GPIO);

  //Wheel motor pins
  SetPin2Digital(PC, WHEEL_L_GPIO | WHEEL_R_GPIO);
  SetPin2Output(PC, WHEEL_L_GPIO | WHEEL_R_GPIO);

  //Conveyor Motor Pin
  SetPin2Digital(PF, CONVEYOR_MOTOR_GPIO);
  SetPin2Output(PF, CONVEYOR_MOTOR_GPIO);
  PORTF &= ~CONVEYOR_MOTOR_GPIO;

  //Game Status pin
  SetPin2Digital(PC, GAME_STATUS_GPIO);
  SetPin2Output(PC, GAME_STATUS_GPIO);
  //Set game status low initially
  PORTC &= ~GAME_STATUS_GPIO;
}

//***********************************************************************************
//-----------MODULE TEST HARNESS-----------------------------------------------------
//***********************************************************************************
//#define HW_INIT_TEST
#ifdef HW_INIT_TEST

//Debug
#include "termio.h"

#include <stdio.h>
#include "ES_Configure.h"
#include "ES_Framework.h"
#include "ES_Port.h"
#include "termio.h"
#include "EnablePA25_PB23_PD7_PF0.h"

#define clrScrn() printf("\x1b[2J")
#define goHome() printf("\x1b[1,1H")
#define clrLine() printf("\x1b[K")

int main(void)
{
  // Set the clock to run at 40MhZ using the PLL and 16MHz external crystal
  SysCtlClockSet(SYSCTL_SYSDIV_5 | SYSCTL_USE_PLL | SYSCTL_OSC_MAIN
      | SYSCTL_XTAL_16MHZ);
  TERMIO_Init();
  clrScrn();

  // When doing testing, it is useful to announce just which program
  // is running.
  puts("\rTimers Test Harnessfor \r");
  printf( "Timer_test\r\n");
  printf( "%s %s\n", __TIME__, __DATE__);
  printf( "\n\r\n");
  printf( "Press any key to post key-stroke events to Service 0\n\r");
  printf( "Press 'd' to test event deferral \n\r");
  printf( "Press 'r' to test event recall \n\r");

  //Init all hardware
  InitHW();

  //Set all GPIO ouptput pins to HIGH
  PORTD |= PD2;   //x
  PORTD |= PD3;
  PORTD |= PD6;
  PORTD |= PD7;
  PORTC |= PC6;
  PORTC |= PC7;
  PORTF |= PF4;
  PORTF |= PF0;
  printf("\n\rGPIOs Set");

  //Set DC on all lines
  SetDC(SERVO_1,          10);
  SetDC(SERVO_2,          75);

  SetDC(SERVO_4,          50);
  SetDC(SERVO_3,          10);

  SetDC(WHEEL_L,          75);
  SetDC(WHEEL_R,          10);

  SetDC(CONVEYOR,         80);
  SetDC(RECYCLING_BEACON, 50);

  printf("\n\rDC's Set");

  //Set Period of Recycling beacon to 1.5kHz
  //NOTE: Need to reset DC after Setting a new period.
  SetRecyclingBeaconPeriod(800);
  SetDC(RECYCLING_BEACON, 50);
  printf("\n\rBeacon period set ");
  //***************************************

  while (1)
  {
    ;
  }
  return 0;
}

#endif
