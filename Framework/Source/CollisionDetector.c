
/****************************************************************************
 Module
   CollisionDetector.c

 Revision
   1.0.1

 Description
   This is a template file for implementing a simple service under the
   Gen2 Events and Services Framework.

 Notes

 History
 When           Who     What/Why
 -------------- ---     --------
 01/16/12 09:58 jec      began conversion from TemplateFSM.c
****************************************************************************/
/*----------------------------- Include Files -----------------------------*/
/* include header files for this state machine as well as any machines at the
   next lower level in the hierarchy that are sub-machines to this machine
*/
#include "ES_Configure.h"
#include "ES_Framework.h"
#include "CollisionDetector.h"
#include "MasterSM.h"
#include "Recycling.h"
#include "PositioningAgainstGW.h"
#include "ADMulti.h"

/*----------------------------- Module Defines ----------------------------*/
#define OTHER_BOT_THRESHOLD 250
#define Measurement_Set_Size 11

/*---------------------------- Module Functions ---------------------------*/
/* prototypes for private functions for this service.They should be functions
   relevant to the behavior of this service
*/

static uint16_t AverageADC(uint16_t CurrentBeacon);

/*---------------------------- Module Variables ---------------------------*/
// with the introduction of Gen2, we need a module level Priority variable
static uint8_t MyPriority;

//
static bool BotBehind;
static bool BotInFront;

//Values for reading ADC values
static uint32_t AnalogVal[2];
static uint16_t PastBeacons[10] = { 4095, 4095, 4095, 4095, 4095, 4095, 4095, 4095, 4095, 4095 };

//Values to hold our current average reading on the ADC pins
static uint16_t CurrentSensorValL;
static uint16_t CurrentSensorValR;

//Define our zero point from reading level at game start
static uint32_t LeftDatum = 4095; //Initially set to highest value
static uint32_t RightDatum = 4095;

/*------------------------------ Module Code ------------------------------*/
/****************************************************************************
 Function       InitCollisionDetector
 Parameters     uint8_t : the priorty of this service
 Returns        bool, false if error in initialization, true otherwise
 Description    Saves away the priority, and does any other required
                initialization for this service
****************************************************************************/
bool InitCollisionDetector(uint8_t Priority)
{
  ES_Event_t ThisEvent;

  MyPriority = Priority;

  // post the initial transition event
  ThisEvent.EventType = ES_INIT;
  if (ES_PostToService(MyPriority, ThisEvent) == true)
  {
    return true;
  }
  else
  {
    return false;
  }
}

/****************************************************************************
 Function       PostCollisionDetector
 Parameters     EF_Event_t ThisEvent ,the event to post to the queue
 Returns        bool false if the Enqueue operation failed, true otherwise
 Description    Posts an event to this state machine's queue
****************************************************************************/
bool PostCollisionDetector(ES_Event_t ThisEvent)
{
  return ES_PostToService(MyPriority, ThisEvent);
}

/****************************************************************************
 Function       RunCollisionDetector
 Parameters     ES_Event_t : the event to process
 Returns        ES_Event, ES_NO_EVENT if no error ES_ERROR otherwise
 Description    add your description here
****************************************************************************/
ES_Event_t RunCollisionDetector(ES_Event_t ThisEvent)
{
  ES_Event_t ReturnEvent;
  ReturnEvent.EventType = ES_NO_EVENT; // assume no errors
  /********************************************
   in here you write your service code
   *******************************************/

  switch (ThisEvent.EventType)
  {
    case START:
    {
      LeftDatum = CurrentSensorValL;
      RightDatum = CurrentSensorValR;
      printf( "\n\rRightDatum: %d", RightDatum);
      printf( "\n\rRightDatum: %d", RightDatum);
    }
    break;

      /*
      case BUMP:
      {
        if (BotBehind && ((ThisEvent.EventParam == BL_HIT) || (ThisEvent.EventParam == BR_HIT)))
        {
          ES_Event_t NewEvent;
          NewEvent.EventType = HIT_BOT_BEHIND;
          //PostMasterSM(NewEvent);
          //PostPositioningAgainstGW(NewEvent);
          //PostRecycling(NewEvent);
        }
        if (BotInFront && ((ThisEvent.EventParam == FL_HIT) || (ThisEvent.EventParam == FR_HIT)))
        {
          ES_Event_t NewEvent;
          NewEvent.EventType = HIT_BOT_IN_FRONT;
          //PostMasterSM(NewEvent);
          //PostPositioningAgainstGW(NewEvent);
          //PostRecycling(NewEvent);
        }
      }
      break;
      */
  }

  return ReturnEvent;
}

/***************************************************************************
 private functions
 ***************************************************************************/

/*------------------------------- Footnotes -------------------------------*/
/*------------------------------ End of file ------------------------------*/

/****************************************************************************
 Function       CollisionEventChecker
 Parameters     None
 Returns        true if new event, false otherwise
 Description    add your description here
****************************************************************************/
bool CollisionEventChecker(void)
{
  bool ReturnVal = false;
  //analog read state of pin E3
  ADC_MultiRead(AnalogVal);

  uint32_t  SensorValL = AnalogVal[0];
  uint32_t  SensorValR = AnalogVal[1];

  //Run our new data through the MA filter
  CurrentSensorValL = AverageADC(SensorValL);
  CurrentSensorValR = AverageADC(SensorValR);

  //printf("\n\r Left Sensor value: %d", CurrentSensorValL);
  //printf("\n\r Right Sensor value: %d", CurrentSensorValR);

  //Debug
  //printf("\n\rBeacon Checker: %d", CurrentBeaconVal);
  if ((CurrentSensorValL < LeftDatum) && ((LeftDatum - CurrentSensorValL) > OTHER_BOT_THRESHOLD) && (QueryMasterSM() != CollisionGettingOutOfWay))
  {
    //printf("\n\r Other Bot Detected - left");
    printf("\n\r Left Sensor value: %d", CurrentSensorValL);
    ES_Event_t NewEvent;
    NewEvent.EventType = HIT_BOT_IN_FRONT;
    PostMasterSM(NewEvent);
    ReturnVal = true;
  }
  else if ((CurrentSensorValR < RightDatum) && ((RightDatum - CurrentSensorValR) > OTHER_BOT_THRESHOLD) && (QueryMasterSM() != CollisionGettingOutOfWay))
  {
    //printf("\n\r Other Bot Detedcted - Right");
    printf("\n\r Right Sensor value: %d", CurrentSensorValR);
    ES_Event_t NewEvent;
    NewEvent.EventType = HIT_BOT_IN_FRONT;
    PostMasterSM(NewEvent);
    ReturnVal = true;
  }

  return ReturnVal;
}

/****************************************************************************
 Function       AverageBeacon
 Parameters     New data point
 Returns        None. Updates static variables
 Description    Moving average filter for our tape sensor readings
****************************************************************************/
static uint16_t AverageADC(uint16_t CurrentBeacon)
{
  uint16_t Average = CurrentBeacon;

  //Loop through PastPeriods array
  for (int i = Measurement_Set_Size - 2; i >= 0; i--)
  {
    //Add past values to average
    Average += PastBeacons[i];

    //Add new value to zero position
    if (i == 0)
    {
      PastBeacons[i] = CurrentBeacon;
    }
    //Move old values one over. Lose oldest value
    else
    {
      PastBeacons[i] = PastBeacons[i - 1];
    }
  }
  Average = Average / Measurement_Set_Size;
  return Average;
}

void debugSetBotInFront(void)
{
  BotInFront = true;
}

void debugSetBotBehind(void)
{
  BotBehind = true;
}