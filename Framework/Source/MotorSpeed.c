/****************************************************************************
 Module
   MotorSpeed.c

 Revision
   1.0.1

 Description
   This is a service to measure motor speed from a DC Motor.

 Notes

 History
 When           Who     What/Why
 -------------- ---     --------
 01/25/19 11:00 alaisha  converted for use in Lab 6
 01/16/12 09:58 jec      began conversion from TemplateFSM.c
****************************************************************************/

/*----------------------------- Include Files -----------------------------*/
/* include header files for this state machine as well as any machines at the
   next lower level in the hierarchy that are sub-machines to this machine
*/
// the headers to access the GPIO subsystem
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_gpio.h"
#include "inc/hw_sysctl.h"

#include <stdint.h>
#include <stdbool.h>
#include "inc/hw_pwm.h"
#include "driverlib/sysctl.h"
#include "driverlib/pin_map.h"
#include "driverlib/gpio.h"
#include "driverlib/pwm.h"
#include "inc/hw_timer.h"
#include "inc/hw_nvic.h"

#include "ES_Framework.h"

//#include "LEDBar.h"

#include "MotorSpeed.h"

/*----------------------------- Module Defines ----------------------------*/
#define MaxPeriod 0x00004650
#define TicksPerSec 40000000

/*---------------------------- Module Functions ---------------------------*/
/* prototypes for private functions for this service. They should be
   functions relevant to the behavior of this service.
*/

/*---------------------------- Module Variables ---------------------------*/

static uint32_t LastCapture;
static uint32_t Period, RPM;
//static uint32_t MaxPeriod = 0;
static bool     CanGetPeriod = false;

/*------------------------------ Module Code ------------------------------*/
/****************************************************************************
 Function
     InitMotorSpeed

 Parameters
     none

 Returns
     none

 Description
     Initializes input capture timer for measuring speed of DC Motor
 Notes

 Author
     Alaisha Alexander, 01/25/19
****************************************************************************/
void InitMotorSpeed(void)
{
  //printf("\rINIT - MS\r\n");
  // enable the clock to the timer (Wide Timer 0)
  HWREG(SYSCTL_RCGCWTIMER) |= SYSCTL_RCGCWTIMER_R0;
  while ((HWREG(SYSCTL_PRWTIMER) & SYSCTL_RCGCWTIMER_R0) != SYSCTL_PRWTIMER_R0)
  {}

  // enable the clock to Port C
  HWREG(SYSCTL_RCGCGPIO) |= SYSCTL_RCGCGPIO_R2;
  while ((HWREG(SYSCTL_PRGPIO) & BIT2HI) != BIT2HI)
  {}

  // disable timer A
  HWREG(WTIMER0_BASE + TIMER_O_CTL) &= ~TIMER_CTL_TAEN;

  // set up timer in 32-bit wide individual (not concatenated) mode
  HWREG(WTIMER0_BASE + TIMER_O_CFG) = TIMER_CFG_16_BIT;

  // initialize the Interval Load Register to 0xffff.ffff
  HWREG(WTIMER0_BASE + TIMER_O_TAILR) = 0xffffffff;

  // set up timer A
  HWREG(WTIMER0_BASE + TIMER_O_TAMR) =
      (HWREG(WTIMER0_BASE + TIMER_O_TAMR) & ~TIMER_TAMR_TAAMS) |
      (TIMER_TAMR_TACDIR | TIMER_TAMR_TACMR | TIMER_TAMR_TAMR_CAP);

  // set the event to rises only
  HWREG(WTIMER0_BASE + TIMER_O_CTL) &= ~TIMER_CTL_TAEVENT_M;

  // set the alternate function for PC4
  HWREG(GPIO_PORTC_BASE + GPIO_O_AFSEL) |= BIT4HI;

  HWREG(GPIO_PORTC_BASE + GPIO_O_PCTL) =
      (HWREG(GPIO_PORTC_BASE + GPIO_O_PCTL) & 0xfff0ffff) + (7 << 16);

  // make PC4 a digital input
  HWREG(GPIO_PORTC_BASE + GPIO_O_DEN) |= BIT4HI;
  HWREG(GPIO_PORTC_BASE + GPIO_O_DIR) &= BIT4LO;

  // enable a local capture interrupt for the timer
  HWREG(WTIMER0_BASE + TIMER_O_IMR) |= TIMER_IMR_CAEIM;

  // enable timer A
  HWREG(NVIC_EN2) |= BIT30HI;

  // make sure interrupts are enabled globally
  __enable_irq();

  // enable the input capture timer
  HWREG(WTIMER0_BASE + TIMER_O_CTL) |= (TIMER_CTL_TAEN | TIMER_CTL_TASTALL);
}

/****************************************************************************
 Function
     MotorSpeedISR

 Parameters
     none

 Returns
     none

 Description
     Interrupt response routine for motor speed

 Notes

 Author
     Alaisha Alexander, 01/25/19
****************************************************************************/
void MotorSpeedISR(void)
{
  uint32_t ThisCapture;

  // clear the source of the interrupt
  HWREG(WTIMER0_BASE + TIMER_O_ICR) = TIMER_ICR_CAECINT;

  // grab the captured value
  ThisCapture = HWREG(WTIMER0_BASE + TIMER_O_TAR);

  // check to see if we can get the period
  if (CanGetPeriod == false)
  {
    CanGetPeriod = true;
  }
  else
  {
    // calculate the period
    Period = ThisCapture - LastCapture;

    if (Period > MaxPeriod)
    {
      CanGetPeriod = false;
      Period = MaxPeriod;
    }
  }

  // update LastCapture to prepare for the next edge
  LastCapture = ThisCapture;
}

/****************************************************************************
 Function
    QueryPeriod

 Parameters
   none

 Returns
   uint32_t, Period

 Description
   Returns the period

 Notes

 Author
     Alaisha Alexander, 01/25/19
****************************************************************************/
uint32_t QueryPeriod(void)
{
  // return Period
  return Period;
}

/****************************************************************************
 Function
    QueryRPM

 Parameters
   none

 Returns
   uint32_t, RPM

 Description
   Returns the RPM

 Notes

 Author
     Alaisha Alexander, 01/25/19
****************************************************************************/
uint32_t QueryRPM(void)
{
  if (Period == MaxPeriod)
  {
    RPM = 0;
  }
  else
  {
    RPM = (TicksPerSec * 60 * 59) / (Period * 512 * 10);      // integer values from motor stand connections sheet for pulses and gearbox info
  }

  // return RPM
  return RPM;
}

/***************************************************************************
 private functions
 ***************************************************************************/

/*------------------------------- Footnotes -------------------------------*/
/*------------------------------ End of file ------------------------------*/
