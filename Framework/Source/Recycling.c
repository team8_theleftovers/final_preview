/****************************************************************************
 Module
   Recycling.c

 Revision
   1.0.1

 Description
   This is a Recycling file for implementing flat state machines under the
   Gen2 Events and Services Framework.

 Notes

 History
 When           Who     What/Why
 -------------- ---     --------
 01/15/12 11:12 jec      revisions for Gen2 framework
 11/07/11 11:26 jec      made the queue static
 10/30/11 17:59 jec      fixed references to CurrentEvent in RunRecycling()
 10/23/11 18:20 jec      began conversion from SMRecycling.c (02/20/07 rev)
****************************************************************************/
/*----------------------------- Include Files -----------------------------*/
/* include header files for this state machine as well as any machines at the
   next lower level in the hierarchy that are sub-machines to this machine
*/
#include "ES_Configure.h"
#include "ES_Framework.h"
#include "Recycling.h"
//#include "CompassModule.h"
#include "Ports.h"
#include <math.h>
#include "SortingModule.h"
#include "PositioningAgainstGW.h"

#include "Parameters.h"
#include "PWM.h"
#include "Move.h"

#include "mastersm.h"
#include "CompassSM.h"

/*----------------------------- Module Defines ----------------------------*/

#define turn_DC 50
#define num_pulses 10
#define FreqToTicks 50
#define TickThreshold 50

#define FullSpeed_DC 100
//#define Slow_DC 45

#define BEACON_SPIN_TIMEOUT 7000
#define XL_BEACON_SPIN_TIMEOUT 5000

#define COLLISION_BACK_AWAY_TIME 1500

#define RECYCLING_BACKUP_TIME 1000
#define RECYCLING_WAIT_FOR_PULSE_TIME 2000
#define RECYCLING_SERVO_DUMPING_TIME 3000
#define RECYCLING_INITIAL_MOVE_TIME 3000

/*---------------------------- Module Functions ---------------------------*/
/* prototypes for private functions for this machine.They should be functions
   relevant to the behavior of this state machine
*/
static bool SensePeriods(uint16_t Ticks, bool L);
void PulseRecycling(void);

/*---------------------------- Module Variables ---------------------------*/
// everybody needs a state variable, you may need others as well.
// type of state variable should match htat of enum in header file
static RecyclingState_t CurrentState;
static RecyclingState_t NextState;

static uint8_t          LastFrontLeftCornerState;
static uint8_t          LastFrontRightCornerState;
static uint8_t          LastBackLeftCornerState;
static uint8_t          LastBackRightCornerState;

static uint16_t         RecyclingPWM;

static uint16_t         RecyclingPeriod;

// with the introduction of Gen2, we need a module level Priority var as well
static uint8_t MyPriority;

/*------------------------------ Module Code ------------------------------*/
/****************************************************************************
 Function
     InitRecyclingFSM

 Parameters
     uint8_t : the priorty of this service

 Returns
     bool, false if error in initialization, true otherwise

 Description
     Saves away the priority, sets up the initial transition and does any
     other required initialization for this state machine
 Notes

 Author
     J. Edward Carryer, 10/23/11, 18:55
****************************************************************************/
bool InitRecycling(uint8_t Priority)
{
  ES_Event_t ThisEvent;

  MyPriority = Priority;
  // put us into the Initial PseudoState
  CurrentState = RecyclingWaitingForStart;
  // post the initial transition event
  ThisEvent.EventType = ES_INIT;

  if (ES_PostToService(MyPriority, ThisEvent) == true)
  {
    return true;
  }
  else
  {
    return false;
  }
}

/****************************************************************************
 Function
     PostRecyclingFSM

 Parameters
     EF_Event_t ThisEvent , the event to post to the queue

 Returns
     boolean False if the Enqueue operation failed, True otherwise

 Description
     Posts an event to this state machine's queue
 Notes

 Author
     J. Edward Carryer, 10/23/11, 19:25
****************************************************************************/
bool PostRecycling(ES_Event_t ThisEvent)
{
  return ES_PostToService(MyPriority, ThisEvent);
}

/****************************************************************************
 Function
    RunRecyclingFSM

 Parameters
   ES_Event_t : the event to process

 Returns
   ES_Event_t, ES_NO_EVENT if no error ES_ERROR otherwise

 Description
   add your description here
 Notes
   uses nested switch/case to implement the machine.
 Author
   J. Edward Carryer, 01/15/12, 15:23
****************************************************************************/
ES_Event_t RunRecycling(ES_Event_t ThisEvent)
{
  ES_Event_t ReturnEvent;
  ReturnEvent.EventType = ES_NO_EVENT; // assume no errors

  //XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX handle straightening when beacon sensed/not sensed
  //xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx ADD ISR'S TO STARTUP.RVMDK
  //XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX add a game clock that ends at 2:16 (unless compass tells us that)
  //what do we do if beaconsense 360 goes on indefinitely - if we never sense signal - stop turning and drive randomly for a sec?
  //this one doesn't ever get exit events - those should only be posted (to other SM's) when recycling ball is registered
  //make sure this is the case ^^^

  //If we have a game over event, switch to waiting to start state
  if (ThisEvent.EventType == GAME_OVER)
  {
    NextState = RecyclingGameOver;
    printf("\n\r game over \n\r");
    Move(FORWARD, 0);
  }
  //Or if we have an EXIT event from Master, changes state to waiting
  else if (ThisEvent.EventType == EXIT)
  {
    NextState = RecyclingWaitingForStart;
  }
  //otherwise implement our normal state machine
  else
  {
    switch (CurrentState)
    {
      case RecyclingWaitingForStart:
      {
        switch (ThisEvent.EventType)
        {
          case START:
          {
            switch (ThisEvent.EventParam)
            {
              case NO_COLLISION:
              {
                Move( R_WHEEL_FORWARD,  Fast_DC + 10);
                Move( L_WHEEL_FORWARD,  Fast_DC);
                ES_Timer_InitTimer(RECYCLING_INITIAL_MOVE_TIMER, RECYCLING_INITIAL_MOVE_TIME);
                NextState = RecyclingInitialMoveRight;
              }
              break;

              case COLLISION:
              {
                Move(COUNTER_CLOCKWISE, Slow_DC);
                NextState = RecyclingBeaconSensing360;
              }
              break;
            }
          }
          break;
        }
      }
      break;

      case RecyclingInitialMoveRight:
      {
        switch (ThisEvent.EventType)
        {
          case ES_TIMEOUT:
          {
            switch (ThisEvent.EventParam)
            {
              case RECYCLING_INITIAL_MOVE_TIMER:
              {
                NextState = RecyclingBeaconSensing360;
                Move(COUNTER_CLOCKWISE, Slow_DC);
                printf("\n\r commencing beaconsensing360, moving into recyclingbeaconsensing360 state \n\r");
              }
              break;
            }
          }
          break;
        }
      }
      break;

      case RecyclingBeaconSensing360:
      {
        //printf("\n\r in recyclingbeaconsensing360 state \n\r");
        switch (ThisEvent.EventType)
        {
          case BEACON_SENSED:
          {
            Move( FORWARD,  0);
            Move( FORWARD,  Fast_DC);
            ES_Timer_InitTimer(BEACON_SPIN_TIMER, BEACON_SPIN_TIMEOUT);
            NextState = RecyclingDrivingTowardsBeacon;
            //printf("\n\r sensed beacon - switching to driving straight, entering driving towards beacon state \n\r");
          }
          break;

          //any bumper hits during spin - back up in the opposite direction

          case BUMP:
          {
            switch (ThisEvent.EventParam)
            {
              case FL_HIT:
              {
                NextState = RecyclingBackingAway;
                Move(BACKWARD, Slow_DC);
                ES_Timer_InitTimer(RECYCLING_COLLISION_BACK_AWAY_TIMER, COLLISION_BACK_AWAY_TIME);
                printf("\n\r front left bumper hit, backing up \n\r");
              }
              break;

              case FR_HIT:
              {
                NextState = RecyclingBackingAway;
                Move(BACKWARD, Slow_DC);
                ES_Timer_InitTimer(RECYCLING_COLLISION_BACK_AWAY_TIMER, COLLISION_BACK_AWAY_TIME);
                printf("\n\r front right bumper hit, backing up \n\r");
              }
              break;

              case BL_HIT:
              {
                NextState = RecyclingBackingAway;
                Move(FORWARD, Slow_DC);
                ES_Timer_InitTimer(RECYCLING_COLLISION_BACK_AWAY_TIMER, COLLISION_BACK_AWAY_TIME);
                printf("\n\r back left bumper hit, backing up \n\r");
              }
              break;

              case BR_HIT:
              {
                NextState = RecyclingBackingAway;
                Move(FORWARD, Slow_DC);
                ES_Timer_InitTimer(RECYCLING_COLLISION_BACK_AWAY_TIMER, COLLISION_BACK_AWAY_TIME);
                printf("\n\r back right bumper hit, backing up \n\r");
              }
              break;
            }
          }
          break;

          default:
            ;
        }
      }
      break;

      case RecyclingDrivingTowardsBeacon:
      {
        switch (ThisEvent.EventType)
        {
          case ES_TIMEOUT:
          {
            if (ThisEvent.EventParam == BEACON_SPIN_TIMER)
            {
              Move(COUNTER_CLOCKWISE, Slow_DC);
              NextState = RecyclingBeaconSensing360;
              ES_Timer_InitTimer(BEACON_SPIN_TIMER, BEACON_SPIN_TIMEOUT);
              printf("\n\r in recycling - timeout while drivingtowards beacon - initiating spin and going back into beaconsensing360 \n\r");
            }
          }
          break;

          case BUMP:
          {
            switch (ThisEvent.EventParam)
            {
              //if front left bumper hits
              case FL_HIT:
              {
                printf("\n\r in recycling - front left bumper hit \n\r");
                {
                  LastFrontLeftCornerState = true;
                  // if back right is already hit
                  if (LastFrontRightCornerState)
                  {
                    //stop both motors
                    Move(STOP, 0);

                    ES_Event_t NewEvent;
                    NewEvent.EventType = FLUSH_WITH_WALL;
                    NextState = RecyclingDepositingBackup;
                    PostRecycling(NewEvent);
                    // printf("\n\r flush \n\r");
                  }
                  else
                  {
                    //stop left motor, up PWM on right motor
                    Move( L_WHEEL_FORWARD,  0);
                    Move( R_WHEEL_FORWARD,  Pivot_DC);
                    ES_Timer_InitTimer(BEACON_SPIN_TIMER, XL_BEACON_SPIN_TIMEOUT);
                    //printf("\n\r left motor stop \n\r");
                  }
                }
              }
              break;

              //front left bumper disengages
              case FL_OFF:
              {
                //printf("\n\r in recycling -front left bumper disengaged \n\r");
                LastFrontLeftCornerState = false;
                //start left motor
                Move(L_WHEEL_FORWARD, Slow_DC);
                //printf("\n\r left motor start backward \n\r");
              }
              break;

              //if front right bumper hits
              case FR_HIT:
              {
                //printf("\n\r in recycling - front right bumper hit \n\r");
                {
                  LastFrontRightCornerState = true;
                  // if left is already hit
                  if (LastFrontLeftCornerState)
                  {
                    //stop both motors
                    Move(STOP, 0);
                    //this is the end of the demo
                    ES_Event_t NewEvent;
                    NewEvent.EventType = FLUSH_WITH_WALL;
                    NextState = RecyclingDepositingBackup;
                    PostRecycling(NewEvent);
                    //printf("\n\r flush \n\r");
                  }
                  else
                  {
                    //stop right motor, up PWM on left motor
                    Move( R_WHEEL_FORWARD,  0);
                    Move( L_WHEEL_FORWARD,  Pivot_DC);
                    ES_Timer_InitTimer(BEACON_SPIN_TIMER, XL_BEACON_SPIN_TIMEOUT);
                    //printf("\n\r right motor stop \n\r");
                  }
                }
              }
              break;

              //front right bumper disengages
              case FR_OFF:
              {
                //printf("\n\r in recycling - front right bumper disengaged \n\r");
                LastFrontRightCornerState = false;
                //start right motor backward
                Move(R_WHEEL_FORWARD, Slow_DC);
                //printf("\n\r right motor start backward \n\r");
              }
              break;
            }
          }
          break;

          case BEACON_CAME_OFF_L:
          {
            //Increase left wheel duty cycle
            Move(L_WHEEL_FORWARD, Fast_DC + 10);
            //printf("\n\r in recycling - beacon came off on left side\n\r");
            //could implement proportional control here - increase DC based on value of NumTru
          }
          break;

          case BEACON_CAME_OFF_R:
          {
            //Increase Right wheel duty cycle
            Move(R_WHEEL_FORWARD, Fast_DC + 10);
            //printf("\n\r in recycling - beacon came off on right side\n\r");
          }
          break;

          case BEACON_LOST:
          {
            Move(COUNTER_CLOCKWISE, Slow_DC);
            NextState = RecyclingBeaconSensing360;
            //printf("\n\r in recycling - beacon came off completely \n\r");
          }
          break;

          case BEACON_SENSED:
          {
            Move(FORWARD, Fast_DC);
            //printf("\n\r in recycling - beacon acquired\n\r");
            ES_Timer_InitTimer(BEACON_SPIN_TIMER, BEACON_SPIN_TIMEOUT);
          }
          break;
        }
      }
      break;

      case RecyclingBackingAway:
      {
        switch (ThisEvent.EventType)
        {
          case ES_TIMEOUT:
          {
            if (ThisEvent.EventParam == RECYCLING_COLLISION_BACK_AWAY_TIMER)
            {
              NextState = RecyclingBeaconSensing360;
              Move(COUNTER_CLOCKWISE, Slow_DC);
              printf("\n\r in recycling - finished backing away from whatever u colided with while turning \n\r");
            }
          }
          break;
        }
      }
      break;

      case RecyclingDepositingBackup:
      {
        switch (ThisEvent.EventType)
        {
          case FLUSH_WITH_WALL:
          {
            Move(BACKWARD, Slow_DC);
            ES_Timer_InitTimer(RECYCLING_SCORE_MANEUVER_TIMER, RECYCLING_BACKUP_TIME);
            printf("\n\r in recycling - in depositing state - flush with wall \n\r");
          }
          break;

          case ES_TIMEOUT:
          {
            switch (ThisEvent.EventParam)
            {
              case RECYCLING_SCORE_MANEUVER_TIMER:
              {
                Move(STOP, 0);
                PulseRecycling();
                //make sure this works^^^^^^^^^^^
                ES_Timer_InitTimer(RECYCLING_SCORE_MANEUVER_TIMER, RECYCLING_WAIT_FOR_PULSE_TIME);
                printf("\n\r in recycling  depositing state - backed up from wall, just pulsed the recycling IR receiver \n\r");
                NextState = RecyclingDepositingWaitingForPulse;
              }
              break;
            }
          }
          break;
        }
      }
      break;

      case RecyclingDepositingWaitingForPulse:
      {
        switch (ThisEvent.EventType)
        {
          case ES_TIMEOUT:
          {
            switch (ThisEvent.EventParam)
            {
              case RECYCLING_SCORE_MANEUVER_TIMER:
              {
                SetServo(RECYCLING_SERVO, SERVO_OPEN_ANGLE);
                ES_Timer_InitTimer(RECYCLING_SCORE_MANEUVER_TIMER, RECYCLING_SERVO_DUMPING_TIME);
                printf("\n\r in recycling depositing state - just opened the recycling door \n\r");
                NextState = RecyclingBallsDumpedWaiting;
              }
              break;
            }
          }
          break;
        }
      }
      break;

      case RecyclingBallsDumpedWaiting:
      {
        switch (ThisEvent.EventType)
        {
          case ES_TIMEOUT:
          {
            switch (ThisEvent.EventParam)
            {
              case RECYCLING_SCORE_MANEUVER_TIMER:
              {
                StopPulsingRecycling();
                SetServo(RECYCLING_SERVO, SERVO_CLOSED_ANGLE);
                NextState = RecyclingWaitingForStart;
                ES_Event_t NewEvent;
                NewEvent.EventType = EXIT;
                PostMasterSM(NewEvent);
                ResetRecyclingCount();
                printf("\n\r finished depositing - close servo and post exit to master SM \n\r");
              }
              break;
            }
          }
          break;
        }
      }
      break;

      default:
        ;
    }
  }
  CurrentState = NextState;
  return ReturnEvent;
}

/****************************************************************************
 Function
     QueryRecycling

 Parameters
     None

 Returns
     RecyclingState_t The current state of the Recycling state machine

 Description
     returns the current state of the Recycling state machine
 Notes

 Author
     J. Edward Carryer, 10/23/11, 19:21
****************************************************************************/
RecyclingState_t QueryRecycling(void)
{
  return CurrentState;
}

/***************************************************************************
 private functions
 ***************************************************************************/

void PulseRecycling(void)
{
  uint16_t PeriodInMicroSec = 500;//QueryDoorPeriod(); //this gives frequency
  SetRecyclingBeaconPeriod(PeriodInMicroSec);
  SetDC(RECYCLING_BEACON, 50);
}

void StopPulsingRecycling()
{
  SetDC(RECYCLING_BEACON, 0);
}
