/****************************************************************************
 Module         SSIModule.c

 Revision       1.0.1

 Description    Functions for SSI module

 Notes

 History

****************************************************************************/
/*----------------------------- Include Files -----------------------------*/
//header for module
#include "CompassSM.h"

//To mask and unmask SSI interrupt
#include "inc/hw_ssi.h"
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_timer.h"

// the common headers for C99 types
#include <stdint.h>
#include <stdbool.h>

//Header for Timing Functions
#include "ES_Port.h"

//include header files for the framework
#include "ES_Configure.h"
#include "ES_Framework.h"
#include "ES_DeferRecall.h"
#include "ES_ShortTimer.h"

//For debugging
#include "termio.h"

//For parameters
#include "Parameters.h"
#include "Ports.h"
#include "PWM.h"

//For state machines
#include "MasterSM.h"
#include "PositioningAgainstGW.h"
#include "Recycling.h"
#include "SortingModule.h"

//For BeaconService
#include "BeaconService.h"

/*----------------------------- Defines -----------------------------*/
//#define COMPASS_DEBUG //Define if you want print statements enabled
#define USING_COMPASS //Define if you want the compass to send events to master and other SM's

#define START_WAIT_TIME 200       //ms
#define GAME_WAIT_TIME 100        //ms
#define CLEANING_UP_WAIT_TIME 100 //ms

#define NUM_COMPASS_COMMANDS 3
#define COM_COLOR_AND_FREQUENCY 0xD2  //Command to return assigned color and frequency info
#define COM_POINTS 0xB4               //Returns each team's points earned via recycling
#define COM_STATUS 0x78               //Returns game status
#define GAME_OVER_COMMAND 2

#define EAST_RECYCLING_CENTER_PERIOD 500  //microseconds
#define WEST_RECYCLING_CENTER_PERIOD 600  //microseconds
#define NORTH_LANDFILL_PERIOD 800         //microseconds
#define SOUTH_LANDFILL_PERIOD 700         //microseconds

#define WEST_RECYCLING_DOOR_PERIOD 1000
#define EAST_RECYCLING_DOOR_PERIOD 200

/*---------------------------- Module Variables ---------------------------*/
static uint8_t        MyPriority;
static CompassState_t CurrentState = CompassInitPState;
static uint8_t        WriteIndex = 0;   //Used to cycle through query commands
static uint8_t        NextCommand = 0;  //Used to store the command we are currently working on

static uint8_t        REG;    //Team's register byte. 0x01 for South, 0x10 for North
static bool           North;  //true if north. false if south
static uint8_t        GameState = 0;
static uint8_t        WestRecylingColor;
static uint8_t        EastRecylingColor;

static uint16_t       RecylingDoorPeriodList[] = { 1000, 947, 893, 840, 787, 733, 680, 627,
                                                   573, 520, 467, 413, 360, 307, 253, 200 };

static uint8_t        COMPASS_COMMANDS[] = { COM_COLOR_AND_FREQUENCY, COM_POINTS,
                                             COM_STATUS };

//Variables that can be queried by other modules
static uint8_t  AssignedColor;          //Color to be recycled. A number from 0-5
static uint16_t CurrentPoints;          //How many points our team currently has
static uint16_t RecyclingCenterPeriod;  //Period of beacon for the assigned recyling center
static uint16_t LandfillPeriod;         //Period of beacon for assigned landfill
static uint16_t RecyclingDoorPeriod;    //Beacon period needed to open recycling center

/*---------------------------- Module Functions ---------------------------*/
void CompassISR(void);
static void WriteBytes(uint8_t B1, uint8_t B2, uint8_t B3);
void WritePointsBytes(uint8_t B1);
void ProcessColorAndFrequency(void);
void ProcessGameStatus(void);
void ProcessPoints(void);

/*----------------------------- Module code -----------------------------*/
/****************************************************************************
Function:       InitCompassSM
Parameters:     uint8_t : the priorty of this service
Returns:        bool, false if error in initialization, true otherwise
Description:    Saves away the priority, and does any other required initialization
                for this service
Author:         S. Diehl
****************************************************************************/
bool InitCompassSM(uint8_t Priority)
{
  printf("\n\rInitCompassSM");
  //SPI system already initialized in InitHW() in main, so we don't have to do it here.

  //Set up initial transition event
  ES_Event_t ThisEvent;
  MyPriority = Priority;

  // post the initial transition event
  ThisEvent.EventType = ES_INIT;

  if (ES_PostToService(MyPriority, ThisEvent) == true)
  {
    return true;
  }
  else
  {
    return false;
  }
}

/****************************************************************************
Function:     PostCompassSM
Parameters:   ES_Event ThisEvent ,the event to post to the queue
Returns:      bool false if the Enqueue operation failed, true otherwise
Description:  Posts an event to this state machine's queue
Author:       J. Edward Carryer, 10/23/11, 19:25
****************************************************************************/
bool PostCompassSM(ES_Event_t ThisEvent)
{
  return ES_PostToService(MyPriority, ThisEvent);
}

/****************************************************************************
Function:     RunLCDService
Parameters:   ES_Event_t : the event to process
Returns:      ES_Event_t, ES_NO_EVENT if no error ES_ERROR otherwise
Description:  add your description here
Author:       J. Edward Carryer, 01/15/12, 15:23
****************************************************************************/
ES_Event_t RunCompassSM(ES_Event_t ThisEvent)
{
  ES_Event_t ReturnEvent;
  ReturnEvent.EventType = ES_NO_EVENT; // assume no errors

  switch (CurrentState)
  {
    case CompassInitPState:
    {
      if (ThisEvent.EventType == ES_INIT)
      {
        //Read Port pin to determing team status (HIGH = North, LOW = SOUTH)
        uint8_t team = PORTF & TEAM_STATUS_GPIO;
        printf("\n\rteam is %d", team);
        if (team > 0)
        {
          North = false;   //Set to true for now
        }
        else
        {
          North = true;
        }

        //Set registration byte according to team status
        if (North)
        {
          REG = 0x10;
          printf("\n\rNorth Team");
          LandfillPeriod = NORTH_LANDFILL_PERIOD;
        }
        else
        {
          REG = 0x01;
          printf("\n\rSouth Team");
          LandfillPeriod = SOUTH_LANDFILL_PERIOD;
        }
        //Set Landfill period in BeaconService Module
        SetLandfillPeriod(LandfillPeriod);

        //Write Registration bytes to Compass and unmask interrupt
        WriteBytes(REG, 0x00, 0x00);

        //Set state to Registering
        CurrentState = CompassRegistering;
      }
    }
    break;

    case CompassRegistering:
    {
      if (ThisEvent.EventType == WRITE_FINISHED)
      {
        //Read in bytes from recieve FIFO. We are only concerned about the last byte
        uint8_t CommandByte;
        for (int i = 0; i < 3; i++)
        {
          CommandByte = HWREG(SSI0_BASE + SSI_O_DR);
          //For debugging
          //printf("\n\rBYTE: %d", CommandByte);
        }

        //If we received FF, then the command was not accepted
        if (CommandByte == 0xFF)
        {
          //Write registration bytes again and unmask interrupt
          WriteBytes(REG, 0x00, 0x00);
        }
        //If the last bit is zero, then we did not successfully register
        else if ((CommandByte & BIT0HI) == false)
        {
          //Write warning message to TeraTerm
          //printf("\n\rNACK: Illegal byte used to register.");
          //Write registration bytes again and unmask interrupt
          WriteBytes(REG, 0x00, 0x00);
        }
        //Otherwise we are registerd
        else
        {
          //Set Timer
          ES_Timer_InitTimer(COMPASS_TIMER, START_WAIT_TIME);
          //Change state to Waiting to Start
          CurrentState = CompassWaitingToStart;
          //Debugging Message
          printf("\n\rSuccessfully registered");
        }
      }
    }
    break;

    case CompassWaitingToStart:
    {
      if (ThisEvent.EventType == ES_TIMEOUT)
      {
        //Write Bytes to query game status, and unmask interrupt
        WriteBytes(COM_STATUS, 0x00, 0x00);
      }
      else if (ThisEvent.EventType == WRITE_FINISHED)
      {
        //Read in bytes from recieve FIFO. We are only concerned about the last byte
        uint8_t CommandByte;
        for (int i = 0; i < 3; i++)
        {
          CommandByte = HWREG(SSI0_BASE + SSI_O_DR);
          //For debugging
          #ifdef COMPASS_DEBUG
          printf("\n\rBYTE: %d", CommandByte);
          #endif
        }

        //Extract relevant bits. Cleanup up (game start) is indicated by
        //the byte having the value 0bxxxxxx01. Bit shift CommandByte
        //left by six so that it will equal 0b01000000 when the game is started.
        CommandByte = CommandByte << 6;
        //If the game status is cleaning up
        if (CommandByte == BIT6HI)
        {
          //Change state to GameStarted
          CurrentState = CompassGameStarted;
          //Send GAME_STARTED event to master SM
          printf("\n\rGameStarted");
          ES_Event_t Event;
          Event.EventType = CLEANING_UP;
          #ifdef USING_COMPASS
          PostMasterSM(Event);
          PostSortingModule(Event);
          #endif
          //Set Timer
          ES_Timer_InitTimer(COMPASS_TIMER, GAME_WAIT_TIME);
          //Turn on Game status light
          PORTC |= GAME_STATUS_GPIO;
        }
        else
        {
          //Otherwise, just set timer
          ES_Timer_InitTimer(COMPASS_TIMER, START_WAIT_TIME);
        }
      }
    }
    break;

    case CompassGameStarted:
    {
      if (ThisEvent.EventType == ES_TIMEOUT)
      {
        //Write next set of bytes to query game status and unmask interrupt
        NextCommand = COMPASS_COMMANDS[WriteIndex];

        //If the command is COM_POINTS, we need to process 6 bytes of return info. Otherwise
        //We just need to process 3 bytes. FIFO is 8 positions deep, so we're not going to
        //overflow with any of these
        if (NextCommand == COM_POINTS)
        {
          WritePointsBytes(COM_POINTS);
        }
        else
        {
          WriteBytes(NextCommand, 0x00, 0x00);
        }
      }
      else if (ThisEvent.EventType == WRITE_FINISHED)
      {
        //Read in bytes from recieve FIFO
        //If the command is COM_POINTS, we need to process 6 bytes of return info. Otherwise
        //We just need to process 3 bytes. FIFO is 8 positions deep, so we're not going to
        //overflow with any of these
        if (NextCommand == COM_POINTS)
        {
          ProcessPoints();
        }
        //Process the command depending on the type and update module level variables
        else if (NextCommand == COM_COLOR_AND_FREQUENCY)
        {
          ProcessColorAndFrequency();
        }
        else if (NextCommand == COM_STATUS)
        {
          ProcessGameStatus();
        }

        //Update write index. Wrap to zero if needed (so that we cycle through commands)
        WriteIndex += 1;
        if (WriteIndex >= NUM_COMPASS_COMMANDS)
        {
          WriteIndex = 0;
        }

        //If game has ended
        if (GameState == GAME_OVER_COMMAND)
        {
          //Send GAME_ENDED event
          printf("\n\rGame Over");
          ES_Event_t Event;
          Event.EventType = GAME_OVER;
          //Post GAME_OVER to the state machines
          #ifdef USING_COMPASS
          PostPositioningAgainstGW(Event);
          PostRecycling(Event);
          PostMasterSM(Event);
          PostSortingModule(Event);
          #endif

          //Change state to GameEnded
          CurrentState = CompassGameEnded;
          //Turn off game status light
          PORTC &= ~GAME_STATUS_GPIO;
        }
        else
        {
          //Otherwise, just set timer
          ES_Timer_InitTimer(COMPASS_TIMER, CLEANING_UP_WAIT_TIME);
        }
      }
    }
    break;

    case CompassGameEnded:
    {
      //Do nothing
    }
    break;
  }

  return ReturnEvent;
}

/****************************************************************************
Function:     CompassISR
Parameters:   None
Returns:      None
Description:  Interrupt Service routine for when sending FIFO is empty
Author:       S. Diehl
****************************************************************************/
void CompassISR(void)
{
  //Mask interrupt
  HWREG(SSI0_BASE + SSI_O_IM) &= ~SSI_IM_TXIM; //TXIM bit = 0 (masked)

  //Post WRITE_FINISHED to CompassSM
  ES_Event_t Event;
  Event.EventType = WRITE_FINISHED;
  PostCompassSM(Event);
}

/****************************************************************************
Function:     WriteBytes
Parameters:   Three bytes that you want to write to the COMPASS
Returns:      None
Description:  Writes bytes to COMPASS and enables the EOT interrupt
Author:       S. Diehl
****************************************************************************/
static void WriteBytes(uint8_t B1, uint8_t B2, uint8_t B3)
{
  //Write bytes to the FIFO
  HWREG(SSI0_BASE + SSI_O_DR) = B1;
  HWREG(SSI0_BASE + SSI_O_DR) = B2;
  HWREG(SSI0_BASE + SSI_O_DR) = B3;

  //Enable interrupt
  HWREG(SSI0_BASE + SSI_O_IM) |= SSI_IM_TXIM; //TXIM bit = 1 (not masked)
}

/****************************************************************************
Function:     WritePointsBytes
Parameters:   The command byte to query points
Returns:      None
Description:  Writes bytes to COMPASS and enables the EOT interrupt. Must
              write the command byte followed by 5 0x00 bytes for the full transfer
Author:       S. Diehl
****************************************************************************/
void WritePointsBytes(uint8_t B1)
{
  //Write bytes to the FIFO
  HWREG(SSI0_BASE + SSI_O_DR) = B1;
  for (uint8_t i = 0; i < 5; i++)
  {
    HWREG(SSI0_BASE + SSI_O_DR) = 0x00;
  }

  //Enable interrupt
  HWREG(SSI0_BASE + SSI_O_IM) |= SSI_IM_TXIM; //TXIM bit = 1 (not masked)
}

/****************************************************************************
Function:     ProcessColorAndFrequency
Parameters:   The COMPASS return byte to process
Returns:      None
Description:  Processes COMPASS return byte and updates Module level variables
Author:       S. Diehl
****************************************************************************/
void ProcessColorAndFrequency(void)
{
  //Read in bytes from recieve FIFO. We are only concerned about the last byte
  uint8_t CommandByte;
  for (int i = 0; i < 3; i++)
  {
    CommandByte = HWREG(SSI0_BASE + SSI_O_DR);
    //For debugging
    #ifdef COMPASS_DEBUG
    printf("\n\rBYTE: %d", CommandByte);
    #endif
  }

  //Get the frequency to open the door of the assigned recycling center
  //uint8_t DoorPeriodIndex = CommandByte >> 4;
  //RecyclingDoorPeriod = RecylingDoorPeriodList[DoorPeriodIndex];

  //Get the assigned ball color
  AssignedColor = (CommandByte >> 1) & (BIT2HI | BIT1HI | BIT0HI);

  #ifdef COMPASS_DEBUG
  printf( "\n\rDOOR PERIOD: %d",  RecyclingDoorPeriod);
  printf( "\n\rCOLOR: %d",        AssignedColor);
  #endif
}

/****************************************************************************
Function:     ProcessGameStatus
Parameters:   The COMPASS return byte to process
Returns:      None
Description:  Processes COMPASS return byte and updates Module level variables
Author:       S. Diehl
****************************************************************************/
void ProcessGameStatus(void)
{
  //Read in bytes from recieve FIFO. We are only concerned about the last byte
  uint8_t CommandByte;
  for (int i = 0; i < 3; i++)
  {
    CommandByte = HWREG(SSI0_BASE + SSI_O_DR);
    //For debugging
    #ifdef COMPASS_DEBUG
    printf("\n\rBYTE: %d", CommandByte);
    #endif
  }

  //Find the color being accepted by the West recyling center
  WestRecylingColor = (CommandByte >> 5) & (BIT2HI | BIT1HI | BIT0HI);
  //Find the color being accepted by the East recyling center
  EastRecylingColor = (CommandByte >> 2) & (BIT2HI | BIT1HI | BIT0HI);
  //Find the Game status
  GameState = CommandByte & (BIT1HI | BIT0HI);

  //Update our the frequency for our assigned recycling center
  if (WestRecylingColor == AssignedColor)
  {
    RecyclingCenterPeriod = WEST_RECYCLING_CENTER_PERIOD;
    RecyclingDoorPeriod = WEST_RECYCLING_DOOR_PERIOD;
  }
  else
  {
    RecyclingCenterPeriod = EAST_RECYCLING_CENTER_PERIOD;
    RecyclingDoorPeriod = EAST_RECYCLING_DOOR_PERIOD;
  }
  //Update RecylingCenterPeriod value in BeaconService
  //SetRecyclingPeriod(RecyclingCenterPeriod);

  #ifdef COMPASS_DEBUG
  printf( "\n\rRECYCLING PERIOD: %d", RecyclingCenterPeriod);
  printf( "\n\rGAMESTATE: %d",        GameState);
  #endif
}

/****************************************************************************
Function:     ProcessPoints
Parameters:   None
Returns:      None
Description:  Processes COMPASS return bytes to find the current score
Author:       S. Diehl
****************************************************************************/
void ProcessPoints(void)
{
  //Read in bytes from recieve FIFO. We are only concerned about the last byte
  uint8_t   CommandByte;
  uint16_t  Score = 0;

  //Read through first two junk bytes
  CommandByte = HWREG(SSI0_BASE + SSI_O_DR);
  //printf("\n\rBYTE: %d",CommandByte);
  CommandByte = HWREG(SSI0_BASE + SSI_O_DR);
  //printf("\n\rBYTE: %d",CommandByte);

  //North team points
  //Get upper 8 bits
  CommandByte = HWREG(SSI0_BASE + SSI_O_DR);
  //printf("\n\rBYTE: %d",CommandByte);
  Score |= CommandByte;
  Score = Score << 8;
  //Get lower 8 bits
  CommandByte = HWREG(SSI0_BASE + SSI_O_DR);
  //printf("\n\rBYTE: %d",CommandByte);
  Score |= CommandByte;

  //If we are the north team, update our points variable
  if (North == true)
  {
    CurrentPoints = Score;
  }

  Score = 0;

  //South team points
  //Get upper 8 bits
  CommandByte = HWREG(SSI0_BASE + SSI_O_DR);
  //printf("\n\rBYTE: %d",CommandByte);
  Score |= CommandByte;
  Score = Score << 8;
  //Get lower 8 bits
  CommandByte = HWREG(SSI0_BASE + SSI_O_DR);
  //printf("\n\rBYTE: %d",CommandByte);
  Score |= CommandByte;

  //If we are the south team, update our points variable
  if (North == false)
  {
    CurrentPoints = Score;
  }

  #ifdef COMPASS_DEBUG
  printf("\n\rCURRENT POINTS: %d", CurrentPoints);
  #endif
}

/****************************************************************************
Function:     QueryLandfillPeriod
Parameters:   None
Returns:      None
Description:  Returns landfill beacon period for the registered team
Author:       S. Diehl
****************************************************************************/
uint16_t QueryLandfillPeriod(void)
{
  return LandfillPeriod;
}

/****************************************************************************
Function:     QueryRecyclingPeriod
Parameters:   None
Returns:      None
Description:  Returns the beacon period of the currently assigned recycling center
Author:       S. Diehl
****************************************************************************/
uint16_t QueryRecyclingPeriod(void)
{
  return RecyclingCenterPeriod;
}

/****************************************************************************
Function:     GetRecyclingFreq
Parameters:   None
Returns:      None
Description:  Returns the beacon period to open the door of the recycling center
Author:       S. Diehl
****************************************************************************/
uint16_t QueryDoorPeriod(void)
{
  return RecyclingDoorPeriod;
}

/****************************************************************************
Function:     QueryCompassRecyclingColor
Parameters:   None
Returns:      None
Description:  Returns the color of the currently assigned recycling ball type
              AssignedColor is a number from 0-5 (6 colors total)
Author:       S. Diehl
****************************************************************************/
uint8_t QueryCompassRecyclingColor(void)
{
  return AssignedColor;
}

/****************************************************************************
Function:     QueryCurrentPoints
Parameters:   None
Returns:      None
Description:  Returns the points made by our team
Author:       S. Diehl
****************************************************************************/
uint16_t QueryCurrentPoints(void)
{
  return CurrentPoints;
}
