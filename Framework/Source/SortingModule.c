/****************************************************************************
 Module
   SortingModule.c

 Revision
   1.0.1

 Description
   This is a template file for implementing a simple service under the
   Gen2 Events and Services Framework.

 Notes

 History
 When           Who     What/Why
 -------------- ---     --------
 01/16/12 09:58 jec      began conversion from TemplateFSM.c
****************************************************************************/
/*----------------------------- Include Files -----------------------------*/
/* include header files for this state machine as well as any machines at the
   next lower level in the hierarchy that are sub-machines to this machine
*/

#include <math.h>

#include "ES_Configure.h"
#include "ES_Framework.h"
#include "SortingModule.h"

#include "Ports.h"
#include "Parameters.h"
#include "PWM.h"
#include "Move.h"
#include "Timers.h"
#include "SSIModule.h"
#include "HardwareInit.h"
#include "I2CService.h"
#include "CompassSM.h"

#include "MasterSM.h"

/*----------------------------- Module Defines ----------------------------*/

#define CONVEYOR_BELT_DC 20
#define BLUE 0x04
#define RED 0x00
#define YELLOW 0x02
#define GREEN 0x03
#define PINK 0x05
#define ORANGE 0x01
#define SORT_SERVO_TIME 2000

/*---------------------------- Module Functions ---------------------------*/
/* prototypes for private functions for this service.They should be functions
   relevant to the behavior of this service
*/

/*---------------------------- Module Variables ---------------------------*/
// with the introduction of Gen2, we need a module level Priority variable
static uint8_t  MyPriority;

static uint8_t  GarbageCount;
static uint8_t  GeneralCount = 0;
static uint8_t  RecyclingCount;
static uint8_t  LastColor, CurrentColor;
static uint32_t ColorCount, ColorLimit;
static bool     ColorCounted;

/*------------------------------ Module Code ------------------------------*/
/****************************************************************************
 Function
     InitSortingModule

 Parameters
     uint8_t : the priorty of this service

 Returns
     bool, false if error in initialization, true otherwise

 Description
     Saves away the priority, and does any
     other required initialization for this service
 Notes

 Author
     J. Edward Carryer, 01/16/12, 10:00
****************************************************************************/
bool InitSortingModule(uint8_t Priority)
{
  ES_Event_t ThisEvent;

  MyPriority = Priority;
  /********************************************
   in here you write your initialization code
   *******************************************/
  // post the initial transition event
  ThisEvent.EventType = ES_INIT;
  if (ES_PostToService(MyPriority, ThisEvent) == true)
  {
    return true;
  }
  else
  {
    return false;
  }
}

/****************************************************************************
 Function
     PostSortingModule

 Parameters
     EF_Event_t ThisEvent ,the event to post to the queue

 Returns
     bool false if the Enqueue operation failed, true otherwise

 Description
     Posts an event to this state machine's queue
 Notes

 Author
     J. Edward Carryer, 10/23/11, 19:25
****************************************************************************/
bool PostSortingModule(ES_Event_t ThisEvent)
{
  return ES_PostToService(MyPriority, ThisEvent);
}

/****************************************************************************
 Function
    RunSortingModule

 Parameters
   ES_Event_t : the event to process

 Returns
   ES_Event, ES_NO_EVENT if no error ES_ERROR otherwise

 Description
   add your description here
 Notes

 Author
   J. Edward Carryer, 01/15/12, 15:23
****************************************************************************/
ES_Event_t RunSortingModule(ES_Event_t ThisEvent)
{
  ES_Event_t ReturnEvent;
  ReturnEvent.EventType = ES_NO_EVENT; // assume no errors

  switch (ThisEvent.EventType)
  {
    case CLEANING_UP:
    {
      PORTF |= CONVEYOR_MOTOR_GPIO;
      SetDC(CONVEYOR, CONVEYOR_BELT_DC);
      SetServo(TRAPDOOR_SERVO, SORT_SERVO_REC_ANGLE);
    }
    break;

    case GAME_OVER:
    {
      //Turn conveyor off
      SetDC(CONVEYOR, 0);
      PORTF &= ~CONVEYOR_MOTOR_GPIO;
      GarbageCount = 0;
      RecyclingCount = 0;
      GeneralCount = 0;
    }
    break;

    case ES_TIMEOUT:
    {
      SetServo(TRAPDOOR_SERVO, SORT_SERVO_GARB_ANGLE);
    }
    break;
  }

  return ReturnEvent;
}

/***************************************************************************
 private functions
 ***************************************************************************/

bool SortingEventChecker(void)
{
  bool    ReturnVal = false;
  uint8_t color = GetColor();

  if (((color - LastColor) != 0) && (LastColor == 0x06))
  {
    GeneralCount++;
    printf("\n\r General Count: %d\n\r", QueryGeneralCount());
  }

  /*if ((color - LastColor) != 0)
  {
    CurrentColor = color;
    ColorCount = 0;
    ColorCounted = false;
  }
  else if ((color != 0x06) && ((color - CurrentColor) == 0))
  {
    ColorCount++;
  }


  if ((ColorCount > ColorLimit) && !ColorCounted)
  {
    printf("\n\r Color Count: %d\n\r", ColorCount);
    ReturnVal = true;
    uint8_t RecyclingColor2 = QueryCompassRecyclingColor();
    if ((color == BLUE) || (color == RecyclingColor2))
    {
      RecyclingCount++;
      SetServo(TRAPDOOR_SERVO, SORT_SERVO_REC_ANGLE);
      ES_Timer_InitTimer(SORT_SERVO_TIMER, SORT_SERVO_TIME);
      ES_Event_t NewEvent;
      NewEvent.EventType = RECYCLING_BALL;
      PostMasterSM(NewEvent);
    }
    else
    {

      GarbageCount++;
    }
    ColorCounted = true;
    ColorCount = 0;
    //color = 0x06;

    switch(color)
    {
      case RED:
      {
        printf("\n\rColor: RED\r\n");
      }
      break;

      case ORANGE:
      {
        printf("\n\rColor: GREEN\r\n");
      }
      break;

      case YELLOW:
      {
        printf("\n\rColor: YELLOW\r\n");
      }
      break;

      case GREEN:
      {
        printf("\n\rColor: GREEN\r\n");
      }
      break;

      case BLUE:
      {
        printf("\n\rColor: BLUE\r\n");
      }
      break;

      case PINK:
      {
        printf("\n\rColor: PINK\r\n");
      }
      break;
    }
    printf("\n\r Garbage Count: %d\n\r", GarbageCount);
  printf("\n\r Recycling Count: %d\n\r", RecyclingCount);
    printf("\n\r Color Limit: %d\n\r", ColorLimit);
    printf("\n\r Color Count: %d\n\r", ColorCount);
  }
  */
  LastColor = color;

  //printf("\n\r Garbage Count: %d\n\r", GarbageCount);
  //printf("\n\r Recycling Count: %d\n\r", RecyclingCount);
  return ReturnVal;
}

uint8_t GetColor(void)
{
  uint8_t   color = 0x06;

  uint16_t  ClearValue;
  uint16_t  RedValue;
  uint16_t  GreenValue;
  uint16_t  BlueValue;

  ClearValue = I2C_GetClearValue();
  RedValue = I2C_GetRedValue();
  GreenValue = I2C_GetGreenValue();
  BlueValue = I2C_GetBlueValue();

  float RedPer = (float)RedValue * 100 / ClearValue;
  float GreenPer = (float)GreenValue * 100 / ClearValue;
  float BluePer = (float)BlueValue * 100 / ClearValue;

  /*// For debugging
  printf("Clr: %d, Red: %d, Grn: %d, Blu: %d, R%%: %.2f, G%% %.2f, B%% %.2f \r\n",
      ClearValue, RedValue, GreenValue, BlueValue, RedPer, GreenPer, BluePer);//*/

  uint8_t RLimBase = 47;
  uint8_t RLim = 58;
  uint8_t BLim = 18;
  uint8_t GLim = 18;

  if ((BluePer > GreenPer) && (BluePer > RedPer))
  {
    color = BLUE;
    ColorLimit = 1000;
    //printf("\n\rBLUE\r\n");
  }
  else if ((BluePer < GreenPer) && (GreenPer > RedPer))
  {
    color = GREEN;
    ColorLimit = 3000;
    //printf("\n\rGREEN\r\n");
  }
  else if (((uint8_t)RedPer) < RLimBase)
  {
    color = 0x06;
    //printf("\n\rNOTHING\r\n");
  }
  else if ((((uint8_t)RedPer) <= RLim))
  {
    color = YELLOW;
    ColorLimit = 4000;
    //printf("\n\rYELLOW\r\n");
  }
  else
  {
    if ((BluePer > GreenPer) && (BluePer > BLim))
    {
      color = PINK;
      ColorLimit = 1000;
      //printf("\n\rPINK\r\n");
    }
    else if (GreenPer < GLim)
    {
      color = RED;
      ColorLimit = 15000;
      //printf("\n\rRED\r\n");
    }
    else
    {
      color = ORANGE;
      ColorLimit = 1000;
      //printf("\n\rORANGE\r\n");
    }
  }

  //printf("\n\rValue: %d\r\n", ((uint8_t)RedPer));//*/
  return color;
}

uint8_t QueryGarbageCount(void)
{
  return GarbageCount;
}

uint8_t QueryRecyclingCount(void)
{
  return RecyclingCount;
}

void ResetGarbageCount(void)
{
  GarbageCount = 0;
}

void ResetRecyclingCount(void)
{
  RecyclingCount = 0;
}

int QueryGeneralCount(void)
{
  if ((GeneralCount % 2) == 0)
  {
    return (GeneralCount / 2) - 1;
  }
  else
  {
    return GeneralCount / 3;
  }
}

void ResetGeneralCount(void)
{
  GeneralCount = 0;
}

void debugIncreaseGarbage(void)
{
  GarbageCount++;
}

void debugIncreaseGeneralCount(void)
{
  GeneralCount++;
}

/*------------------------------- Footnotes -------------------------------*/
/*------------------------------ End of file ------------------------------*/
