#ifndef Move_H
#define Move_H

//Includes
#include <stdint.h>
#include <stdbool.h>

#include "PWM.h"

typedef enum MoveType
{
  FORWARD,            //Moves both wheel
  BACKWARD,           //Moves both wheel
  COUNTER_CLOCKWISE,  //Moves both wheel
  CLOCKWISE,          //Moves both wheel
  L_WHEEL_FORWARD,
  L_WHEEL_BACKWARD,
  R_WHEEL_FORWARD,
  R_WHEEL_BACKWARD,
  STOP                //Stops both wheels
}MoveType_t;

//Public Function Prototypes
void SetServo(Actuator_t Actuator, uint8_t DesiredAngle);
void SetWheelPWMPolarity(Actuator_t Actuator, bool direction);
void SetLeftWheelDirection(bool forward);
void SetRightWheelDirection(bool forward);
void Move(MoveType_t DesiredMove, uint32_t DutyCycle);

#endif //Move_H
