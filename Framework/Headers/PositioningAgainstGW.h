/****************************************************************************

  Header file for template Flat Sate Machine
  based on the Gen2 Events and Services Framework

 ****************************************************************************/

#ifndef PositioningAgainstGW_H
#define PositioningAgainstGW_H

// Event Definitions
#include "ES_Configure.h" /* gets us event definitions */
#include "ES_Types.h"     /* gets bool type for returns */

// typedefs for the states
// State definitions for use with the query function
typedef enum
{
  GWWaitingForStart, GWBeaconSensing360, DrivingTowardsBeacon, OneLimitSwitchAgainstGW,
  GWBackingAway, GWFlush, GW180_backup, GW180, GW180_GettingFlush, GWGameOver
}PositioningAgainstGWState_t;

//typedef enum for the bumper event checker
typedef enum BumperEvent
{
  FL_HIT = 1, //1
  FL_OFF,     //2
  FR_HIT,     //3
  FR_OFF,     //4
  BL_HIT,     //5
  BL_OFF,     //6
  BR_HIT,     //7
  BR_OFF      //8
}BumperEvent_t;

// Public Function Prototypes
bool InitPositioningAgainstGW(uint8_t Priority);
bool PostPositioningAgainstGW(ES_Event_t ThisEvent);
ES_Event_t RunPositioningAgainstGW(ES_Event_t ThisEvent);
PositioningAgainstGWState_t QueryPositioningAgainstGW(void);
bool BumperEventChecker(void);
void SetRecyclingPeriod(uint16_t RecyclingPeriod);

#endif /* PositioningAgainstGW_H */
