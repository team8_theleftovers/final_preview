#ifndef PWM_UTILS_H
#define PWM_UTILS_H

//Includes
#include <stdint.h>
#include <stdbool.h>

//Public Function Prototypes
void InitPWM(void);
void SetDC(uint32_t DutyCycle);

//Enum for type of move you want to make
typedef enum MoveType
{
  FORWARD,
  BACKWARD,
  CLOCKWISE,
  COUNTER_CLOCKWISE
}MoveType_t;

#endif //PWM_UTILS_H
