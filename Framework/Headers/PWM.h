#ifndef PWM_H
#define PWM_H

//Includes
#include <stdint.h>
#include <stdbool.h>

//Enum to pass in to Set DC and Init functions
typedef enum Actuator
{
  SERVO_1 = 0,
  SERVO_2,
  SERVO_3,
  SERVO_4,
  WHEEL_L,
  WHEEL_R,
  CONVEYOR,
  RECYCLING_BEACON
}Actuator_t;

//Public Function Prototypes
void InitPWM_Modules(void);
void InitPWM_Servos12(void);
void InitPWM_Servos34(void);
void InitPWM_Wheels(void);
void InitPWM_Conveyor(void);
void InitPWM_RecyclingBeacon(void);

void SetDC(Actuator_t Actuator, uint32_t DutyCycle);
void SetRecyclingBeaconPeriod(uint16_t PeriodInMicroSec);

#endif //PWM_H
