/****************************************************************************

  Header file for Bumper service
  based on the Gen 2 Events and Services Framework

 ****************************************************************************/

#ifndef BumperService_H
#define BumperService_H

#include "ES_Types.h"

// Public Function Prototypes

bool InitBumperService(uint8_t Priority);
bool PostBumperService(ES_Event_t ThisEvent);
ES_Event_t RunBumperService(ES_Event_t ThisEvent);

bool BumperEventChecker(void);

#endif /* BumperService_H */
