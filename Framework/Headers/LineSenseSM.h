/****************************************************************************

  Header file for template Flat Sate Machine
  based on the Gen2 Events and Services Framework

 ****************************************************************************/

#ifndef LineSenseSM_H
#define LineSenseSM_H

// Event Definitions
#include "ES_Configure.h" /* gets us event definitions */
#include "ES_Types.h"     /* gets bool type for returns */

// typedefs for the states
// State definitions for use with the query function
typedef enum
{
  LineSenseSMInitPState, LineSensing, LineWaiting, LineDebouncing
}LineSenseSMState_t;

// Public Function Prototypes

bool InitLineSenseSM(uint8_t Priority);
bool PostLineSenseSM(ES_Event_t ThisEvent);
ES_Event_t RunLineSenseSM(ES_Event_t ThisEvent);
LineSenseSMState_t QueryLineSenseSM(void);
bool LineEventChecker(void);

#endif /* LineSenseSM_H */
