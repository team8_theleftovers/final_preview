#ifndef PORTS_H
#define PORTS_H

//Header Files needed to make HWREG work, and BITDEFS
//include header files for hardware access
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_gpio.h"
#include "inc/hw_sysctl.h"

// the common headers for C99 types
#include <stdint.h>
#include <stdbool.h>

// the headers to access the TivaWare Library
#include "driverlib/sysctl.h"
#include "driverlib/pin_map.h"
#include "driverlib/gpio.h"

#include "BITDEFS.H"

/*-----------------Defines---------------------------------------*/
#define ALL_BITS (0xff << 2)

/*Helpful Defines for manipulating GPIO pins
EXAMPLES:
#define PB0 BIT0HI
PORTB |= PB0  --> Turn PB0 HIGH
PORTB &= ~PB0 --> Turn PBO LOW  */

#define PORTA (HWREG(GPIO_PORTA_BASE + (GPIO_O_DATA + ALL_BITS)))
#define PORTB (HWREG(GPIO_PORTB_BASE + (GPIO_O_DATA + ALL_BITS)))
#define PORTC (HWREG(GPIO_PORTC_BASE + (GPIO_O_DATA + ALL_BITS)))
#define PORTD (HWREG(GPIO_PORTD_BASE + (GPIO_O_DATA + ALL_BITS)))
#define PORTE (HWREG(GPIO_PORTE_BASE + (GPIO_O_DATA + ALL_BITS)))
#define PORTF (HWREG(GPIO_PORTF_BASE + (GPIO_O_DATA + ALL_BITS)))

/*-------------Public Function Definitions-------------------------*/

//PA = Port A. Enumeration variable to pass into por#include "Ports.h"t pin manipulation functions
typedef enum PortName
{
  PA = 0,
  PB,
  PC,
  PD,
  PE,
  PF
}PortName_t;

//Functions for Port pins
void EnablePort(PortName_t);
void SetPin2Digital(PortName_t Port, uint8_t PINS);
void SetPin2Output(PortName_t Port, uint8_t PINS);
void SetPin2Input(PortName_t Port, uint8_t PINS);

/*EXAMPLE -  Set several pins to digital outputs:
    SetPin2Digital(PA, PA2 | PA3 | PA4);
    SetPin2Output(PA, PA2 | PA3 | PA4);
OR to set pins to Inputs:
    SetPin2Input(PA, PA2 | PA3 | PA4);   */

#endif //PORTS_H
