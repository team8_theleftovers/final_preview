/****************************************************************************

  Header file for ADPotentiometer service
  based on the Gen 2 Events and Services Framework

 ****************************************************************************/

#ifndef ADPotentiometer_H
#define ADPotentiometer_H

#include "ES_Types.h"

// Public Function Prototypes

bool InitADPotentiometer(uint8_t Priority);
bool PostADPotentiometer(ES_Event_t ThisEvent);
ES_Event_t RunADPotentiometer(ES_Event_t ThisEvent);
uint32_t QueryADPotentiometer(void);
uint32_t QueryRPMRef(void);

#endif /* ADPotentiometer_H */
