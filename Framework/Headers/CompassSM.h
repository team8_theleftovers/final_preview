#ifndef COMPASS_SM_H
#define COMPASS_SM_H

//Includes
#include <stdint.h>
#include <stdbool.h>

// Event Definitions
#include "ES_Configure.h" /* gets us event definitions */
#include "ES_Types.h"     /* gets bool type for returns */
#include "ES_Events.h"

//Public Function Prototypes
bool InitCompassSM(uint8_t Priority);
bool PostCompassSM(ES_Event_t ThisEvent);
ES_Event_t RunCompassSM(ES_Event_t ThisEvent);

//Functions to query parameters set by COMPASS
uint8_t QueryCompassRecyclingColor(void); //Color to be recycled. A number from 0-5
uint16_t QueryLandfillPeriod(void);       //Period of beacon for assigned landfill
uint16_t QueryRecyclingPeriod(void);      //Period of beacon for the assigned recyling center
uint16_t QueryDoorPeriod(void);           //Beacon period needed to open recycling center
uint16_t QueryCurrentPoints(void);        //How many points our team currently has

//Enum for SM states
typedef enum CompassState
{
  CompassInitPState,
  CompassRegistering,
  CompassWaitingToStart,
  CompassGameStarted,
  CompassGameEnded
}CompassState_t;

#endif //COMPASS_SM_H
