/****************************************************************************

  Header file for template Flat Sate Machine
  based on the Gen2 Events and Services Framework

 ****************************************************************************/

#ifndef Recycling_H
#define Recycling_H

// Event Definitions
#include "ES_Configure.h" /* gets us event definitions */
#include "ES_Types.h"     /* gets bool type for returns */

// typedefs for the states
// State definitions for use with the query function
typedef enum
{
  RecyclingWaitingForStart, RecyclingBeaconSensing360, RecyclingDrivingTowardsBeacon,
  RecyclingBackingAway, RecyclingFlush, RecyclingDepositingBackup, RecyclingDepositingWaitingForPulse,
  RecyclingBallsDumpedWaiting, RecyclingInitialMoveRight, RecyclingGameOver
}RecyclingState_t;

// Public Function Prototypes

bool InitRecycling(uint8_t Priority);
bool PostRecycling(ES_Event_t ThisEvent);
ES_Event_t RunRecycling(ES_Event_t ThisEvent);
RecyclingState_t QueryRecycling(void);
void StopPulsingRecycling(void);

#endif /* Recycling_H */
