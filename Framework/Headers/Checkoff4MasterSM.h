/****************************************************************************

  Header file for template Flat Sate Machine
  based on the Gen2 Events and Services Framework

 ****************************************************************************/

#ifndef Checkoff4MasterSM_H
#define Checkoff4MasterSM_H

// Event Definitions
#include "ES_Configure.h" /* gets us event definitions */
#include "ES_Types.h"     /* gets bool type for returns */

// typedefs for the states
// State definitions for use with the query function
typedef enum
{
  Checkoff4MasterWaitingForStart, PositioningAgainstGW, DumpingGarbage,
  Checkoff4MasterWaitingInCollecting, Recycling
}Checkoff4MasterSMState_t;

// Public Function Prototypes

bool InitCheckoff4MasterSM(uint8_t Priority);
bool PostCheckoff4MasterSM(ES_Event_t ThisEvent);
ES_Event_t RunCheckoff4MasterSM(ES_Event_t ThisEvent);
Checkoff4MasterSMState_t QueryCheckoff4MasterSM(void);

#endif /* Checkoff4MasterSM_H */
