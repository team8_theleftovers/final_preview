/****************************************************************************

  Header file for Motor Speed
  based on the Gen 2 Events and Services Framework

 ****************************************************************************/

#ifndef MotorSpeed_H
#define MotorSpeed_H

#include "ES_Types.h"

// Public Function Prototypes

void InitMotorSpeed(void);
void MotorSpeedISR(void);
uint32_t QueryPeriod(void);
uint32_t QueryRPM(void);

#endif /* MotorSpeed_H */
