#ifndef TIMER_UTILS_H
#define TIMER_UTILS_H

//Includes
#include <stdint.h>
#include <stdbool.h>

//Public Function Prototypes
void InitControllerPeriodicInt(void);
void InitEncoderInputCapture(void);
void InitBeaconSensorInputCapture(void);

#endif //TIMER_UTILS_H
