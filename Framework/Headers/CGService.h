/****************************************************************************

  Header file for Command Generator Service
  based on the Gen 2 Events and Services Framework

 ****************************************************************************/

#ifndef CG_SERVICE_H
#define CG_SERVICE_H

#include "ES_Types.h"
#include "ES_Framework.h"

// Public Function Prototypes

bool InitCG(uint8_t Priority);
bool PostCG(ES_Event_t ThisEvent);
ES_Event_t RunCG(ES_Event_t ThisEvent);
bool CommandChecker(void);

#endif /* CG_SERVICE_H */
