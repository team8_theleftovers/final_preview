/****************************************************************************

  Header file for template Flat Sate Machine
  based on the Gen2 Events and Services Framework

 ****************************************************************************/

#ifndef Lab8MotorFunction_H
#define Lab8MotorFunction_H

// Event Definitions
#include "ES_Configure.h" /* gets us event definitions */
#include "ES_Types.h"     /* gets bool type for returns */

// typedefs for the states
// State definitions for use with the query function
typedef enum
{
  Lab8MotorFunctionInitPState, Driving, CheckingSpeed
}Lab8MotorFunctionState_t;

// Public Function Prototypes

bool InitLab8MotorFunction(uint8_t Priority);
bool PostLab8MotorFunction(ES_Event_t ThisEvent);
ES_Event_t RunLab8MotorFunction(ES_Event_t ThisEvent);
Lab8MotorFunctionState_t QueryLab8MotorFunction(void);
bool SpeedEventChecker(void);
//bool LineEventChecker(void);
void Set100DC(void);
void Set0DC(void);
void RestoreDC(void);
void SetA(bool fwd, int DutyCycleA2);
void SetB(bool fwd, int DutyCycleB2);

#endif /* Lab8MotorFunction_H */
