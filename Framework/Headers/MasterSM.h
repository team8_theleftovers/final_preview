/****************************************************************************

  Header file for template Flat Sate Machine
  based on the Gen2 Events and Services Framework

 ****************************************************************************/

#ifndef MasterSM_H
#define MasterSM_H

// Event Definitions
#include "ES_Configure.h" /* gets us event definitions */
#include "ES_Types.h"     /* gets bool type for returns */

// typedefs for the states
// State definitions for use with the query function
typedef enum
{
  MasterWaitingForStart, PositioningAgainstGW, DumpingGarbage,
  MasterWaitingInCollecting, Recycling, CollisionGettingOutOfWay, CollisionTurning,
  CollectingPositionStraight, MasterGameOver
}MasterSMState_t;

typedef enum CollisionParam
{
  COLLISION,
  NO_COLLISION
}CollisionParam_t;

// Public Function Prototypes
bool InitMasterSM(uint8_t Priority);
bool PostMasterSM(ES_Event_t ThisEvent);
ES_Event_t RunMasterSM(ES_Event_t ThisEvent);
MasterSMState_t QueryMasterSM(void);

#endif /* MasterSM_H */
