/*DESCRIPTION:  File to hold all #define constants that are used to define
                the behavior of the robot.

*/

#ifndef PARAMETERS_H
#define PARAMETERS_H

//--------------TIVA Pins------------------------
#define PB0 BIT0HI
#define PB1 BIT1HI
#define PB2 BIT2HI
#define PB3 BIT3HI
#define PB4 BIT4HI
#define PB5 BIT5HI
#define PB6 BIT6HI
#define PB7 BIT7HI
#define PA2 BIT2HI
#define PA3 BIT3HI
#define PA4 BIT4HI
#define PA5 BIT5HI
#define PA6 BIT6HI
#define PA7 BIT7HI
#define PC4 BIT4HI
#define PC5 BIT5HI
#define PC6 BIT6HI
#define PC7 BIT7HI

#define PE0 BIT0HI
#define PE1 BIT1HI
#define PE2 BIT2HI
#define PE3 BIT3HI
#define PE4 BIT4HI
#define PE5 BIT5HI
#define PF4 BIT4HI
#define PF3 BIT3HI
#define PF2 BIT2HI
#define PD0 BIT0HI
#define PD1 BIT1HI
#define PD2 BIT2HI
#define PD3 BIT3HI
#define PD6 BIT6HI
#define PD7 BIT7HI
#define PF0 BIT0HI
#define PF1 BIT1HI

//----------PIN ASSINGMENTS-----------------

//PWM Lines
#define SERVO_1_PWM PB4             //M0PWM2 --> PWM1GENA
#define SERVO_2_PWM PB5             //M0PWM3 --> PWM1GENB
#define SERVO_3_PWM PB6             //M0PWM0 --> PWM0GENA
#define SERVO_4_PWM PB7             //MOPWM1 --> PWM0GENB, Extra Servo
#define WHEEL_L_PWM PA6             //M1PMW2 --> PWM1GENA
#define WHEEL_R_PWM PA7             //M1PWM3 --> PWM1GENB
#define CONVEYOR_MOTOR_PWM PF1      //M1PWM5 --> PWM2GENB
#define RECYCLING_BEACON_PWM PF2    //M1PWM6 --> PWM3GENA

//Input Capture Pins
#define BEACON_SENSOR_R PD0 //WT2CCP0
#define BEACON_SENSOR_L PD1 //WT2CCP1
//#define ENCODER_L PC4       //WT0CCP0
//#define ENCODER_R PC5       //WT0CCP1

//GPIO Pins
#define LIMIT_FL PD6            //Front left
#define LIMIT_FR PD7            //Front right
#define LIMIT_RL PD2            //Rear left
#define LIMIT_RR PD3            //Rear right
#define WHEEL_L_GPIO PC6        //To motor driver
#define WHEEL_R_GPIO PC7        //To motor driver
#define TEAM_STATUS_GPIO PF4    //To switch. Indicates whether North or South team status has been chosen
#define CONVEYOR_MOTOR_GPIO PF0 //To motor driver

#define GAME_STATUS_GPIO PC5

//SPI
#define SCLK PA2 //Fix this
#define MISO PA3
#define MOSI PA4
#define CS PA5

//I2C

//ADC for Opponent sensing
#define RETRO_LEFT PE0
#define RETRO_RIGHT PE1

//-----------Parameters-----------------

//PWM Periods
#define SERVO_PERIOD_IN_MS 20         //50Hz
#define WHEEL_MOTOR_PERIOD_IN_US 1000 //1kHz
#define CONVEYOR_PERIOD_IN_US 2000    //1kHz

//PID Speed Controller
#define PID_CONTROLLER_PERIOD_IN_MS 2 //500Hz

#define FullSpeed_DC 100
#define Slow_DC 30
#define Pivot_DC 75
#define Fast_DC 50 //50

//Give Servos 1-3 a name
#define GARBAGE_SERVO SERVO_2
#define RECYCLING_SERVO SERVO_1
#define TRAPDOOR_SERVO SERVO_3
#define SERVO_CLOSED_ANGLE 45
#define SERVO_OPEN_ANGLE 0
#define SORT_SERVO_REC_ANGLE 55
#define SORT_SERVO_GARB_ANGLE 0

#endif //PARAMETERS_H
