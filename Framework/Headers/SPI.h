#ifndef SSI_MODULE_H
#define SSI_MODULE_H

//Includes
#include <stdint.h>
#include <stdbool.h>

//Public Function Prototypes
void InitSSI(void);

#endif //SSI_MODULE_H
