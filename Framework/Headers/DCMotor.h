/****************************************************************************

  Header file for DC Motor
  based on the Gen 2 Events and Services Framework

 ****************************************************************************/

#ifndef DCMotor_H
#define DCMotor_H

#include "ES_Types.h"

// Public Function Prototypes

bool InitDCMotor(uint8_t Priority);
bool PostDCMotor(ES_Event_t ThisEvent);
ES_Event_t RunDCMotor(ES_Event_t ThisEvent);

#endif /* DCMotor_H */
