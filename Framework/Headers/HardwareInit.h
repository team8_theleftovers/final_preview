#ifndef HARDWARE_INIT_H
#define HARDWARE_INIT_H

//Includes
#include <stdint.h>
#include <stdbool.h>

void InitHW(void);

#endif //HARDWARE_INIT_H
