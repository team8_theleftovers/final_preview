/****************************************************************************

  Header file for PWM Module
  based on the Gen 2 Events and Services Framework

 ****************************************************************************/

#ifndef PWMModule_H
#define PWMModule_H

#include "ES_Framework.h"

// Public Function Prototypes

void InitPWM(void);
void PWMSetDuty(uint32_t DutyCycle);

#endif /* PWMModule_H */
