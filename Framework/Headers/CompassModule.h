/****************************************************************************

  Header file for template service
  based on the Gen 2 Events and Services Framework

 ****************************************************************************/

#ifndef CompassModule_H
#define CompassModule_H

#include "ES_Types.h"

// Public Function Prototypes

bool InitCompassModule(uint8_t Priority);
bool PostCompassModule(ES_Event_t ThisEvent);
ES_Event_t RunCompassModule(ES_Event_t ThisEvent);
uint16_t GetGarbageFreq(void);
uint16_t GetRecyclingFreq(void);

#endif /* CompassModule_H */
