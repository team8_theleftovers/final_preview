/****************************************************************************

  Header file for PI Control
  based on the Gen 2 Events and Services Framework

 ****************************************************************************/

#ifndef PIControl_H
#define PIControl_H

#include "ES_Types.h"

// Public Function Prototypes

void InitPI(void);
void PI_ISR(void);

#endif /* PIControl_H */
