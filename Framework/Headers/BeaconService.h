#ifndef BEACON_SERVICE_H
#define BEACON_SERVICE_H

//Includes
#include <stdint.h>
#include <stdbool.h>

// Event Definitions
#include "ES_Configure.h" /* gets us event definitions */
#include "ES_Types.h"     /* gets bool type for returns */
#include "ES_Events.h"

//Public Function Prototypes
bool InitBeaconService(uint8_t Priority);
bool PostBeaconService(ES_Event_t ThisEvent);
ES_Event_t RunBeaconService(ES_Event_t ThisEvent);

bool BeaconEventChecker(void);

void SetLandfillPeriod(uint16_t NewPeriod);
void SetRecyclingPeriod(uint16_t NewPeriod);
void ClearBeaconDetectors(void);

#endif //BEACON_SERVICE_H
