/****************************************************************************

  Header file for template service
  based on the Gen 2 Events and Services Framework

 ****************************************************************************/

#ifndef CollisionDetector_H
#define CollisionDetector_H

#include "ES_Types.h"

// Public Function Prototypes

bool InitCollisionDetector(uint8_t Priority);
bool PostCollisionDetector(ES_Event_t ThisEvent);
ES_Event_t RunCollisionDetector(ES_Event_t ThisEvent);

bool CollisionEventChecker(void);

#endif /* CollisionDetector_H */
