/****************************************************************************

  Header file for template service
  based on the Gen 2 Events and Services Framework

 ****************************************************************************/

#ifndef SortingModule_H
#define SortingModule_H

#include "ES_Types.h"

// Public Function Prototypes

bool InitSortingModule(uint8_t Priority);
bool PostSortingModule(ES_Event_t ThisEvent);
ES_Event_t RunSortingModule(ES_Event_t ThisEvent);
uint8_t QueryGarbageCount(void);
uint8_t QueryRecyclingCount(void);
bool SortingEventChecker(void);
uint8_t GetColor(void);
void ResetGarbageCount(void);
void ResetRecyclingCount(void);
void debugIncreaseGarbage(void);
void debugIncreaseGeneralCount(void);

int QueryGeneralCount(void);
void ResetGeneralCount(void);

#endif /* SortingModule_H */
